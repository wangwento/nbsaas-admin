-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.12 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  12.5.0.6677
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- 导出 nbsaas-admin 的数据库结构
DROP DATABASE IF EXISTS `nbsaas-admin`;
CREATE DATABASE IF NOT EXISTS `nbsaas-admin` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `nbsaas-admin`;

-- 导出  表 nbsaas-admin.article 结构
DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `comments` int(11) DEFAULT NULL,
  `ext_data` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `introduction` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `likes` int(11) DEFAULT NULL,
  `store_state` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ups` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `catalog_id` bigint(20) DEFAULT NULL,
  `document_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2aptines4lfv2116021bh53gs` (`catalog_id`),
  KEY `FK6xrlkfco3mewsu14n2isbi9oc` (`document_id`),
  KEY `FKgfkys9w7qv3xcubq0drrayuu3` (`user_id`),
  CONSTRAINT `FK2aptines4lfv2116021bh53gs` FOREIGN KEY (`catalog_id`) REFERENCES `article_catalog` (`id`),
  CONSTRAINT `FK6xrlkfco3mewsu14n2isbi9oc` FOREIGN KEY (`document_id`) REFERENCES `article_document` (`id`),
  CONSTRAINT `FKgfkys9w7qv3xcubq0drrayuu3` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.article 的数据：~0 rows (大约)
DELETE FROM `article`;

-- 导出  表 nbsaas-admin.article_catalog 结构
DROP TABLE IF EXISTS `article_catalog`;
CREATE TABLE IF NOT EXISTS `article_catalog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `code` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '编码',
  `depth` int(11) DEFAULT NULL COMMENT '深度',
  `ids` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'ids',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `lft` int(11) DEFAULT NULL COMMENT '左节点',
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `rgt` int(11) DEFAULT NULL COMMENT '右节点',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序号',
  `amount` bigint(20) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb6vm6j4ace2cw6h21vnp5mytb` (`parent_id`),
  CONSTRAINT `FKb6vm6j4ace2cw6h21vnp5mytb` FOREIGN KEY (`parent_id`) REFERENCES `article_catalog` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.article_catalog 的数据：~0 rows (大约)
DELETE FROM `article_catalog`;

-- 导出  表 nbsaas-admin.article_comment 结构
DROP TABLE IF EXISTS `article_comment`;
CREATE TABLE IF NOT EXISTS `article_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `contents` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `article_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKghmocqkgqs5tkmucf5putw64t` (`article_id`),
  KEY `FKq2u0dy36tq655hnhxal16of7k` (`user_id`),
  CONSTRAINT `FKghmocqkgqs5tkmucf5putw64t` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`),
  CONSTRAINT `FKq2u0dy36tq655hnhxal16of7k` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.article_comment 的数据：~0 rows (大约)
DELETE FROM `article_comment`;

-- 导出  表 nbsaas-admin.article_document 结构
DROP TABLE IF EXISTS `article_document`;
CREATE TABLE IF NOT EXISTS `article_document` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `note` varchar(6000) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.article_document 的数据：~0 rows (大约)
DELETE FROM `article_document`;

-- 导出  表 nbsaas-admin.article_like 结构
DROP TABLE IF EXISTS `article_like`;
CREATE TABLE IF NOT EXISTS `article_like` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `article_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKabthli6g1qjriusniw93pbesw` (`article_id`),
  KEY `FK13bn3spxe6xfy318i80669hon` (`user_id`),
  CONSTRAINT `FK13bn3spxe6xfy318i80669hon` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`id`),
  CONSTRAINT `FKabthli6g1qjriusniw93pbesw` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.article_like 的数据：~0 rows (大约)
DELETE FROM `article_like`;

-- 导出  表 nbsaas-admin.article_link_tag 结构
DROP TABLE IF EXISTS `article_link_tag`;
CREATE TABLE IF NOT EXISTS `article_link_tag` (
  `article_id` bigint(20) NOT NULL,
  `tags_id` bigint(20) NOT NULL,
  PRIMARY KEY (`article_id`,`tags_id`),
  KEY `FK8cbs5lfskjdpwpae5p48um1gr` (`tags_id`),
  CONSTRAINT `FK8cbs5lfskjdpwpae5p48um1gr` FOREIGN KEY (`tags_id`) REFERENCES `article_tag` (`id`),
  CONSTRAINT `FKsyqvqf7ceog6lyp1mte4l7prc` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.article_link_tag 的数据：~0 rows (大约)
DELETE FROM `article_link_tag`;

-- 导出  表 nbsaas-admin.article_sensitive_category 结构
DROP TABLE IF EXISTS `article_sensitive_category`;
CREATE TABLE IF NOT EXISTS `article_sensitive_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `code` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '编码',
  `depth` int(11) DEFAULT NULL COMMENT '深度',
  `ids` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'ids',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `lft` int(11) DEFAULT NULL COMMENT '左节点',
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `rgt` int(11) DEFAULT NULL COMMENT '右节点',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序号',
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6yr0lp832885ybn738e80wlp4` (`parent_id`),
  CONSTRAINT `FK6yr0lp832885ybn738e80wlp4` FOREIGN KEY (`parent_id`) REFERENCES `article_sensitive_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.article_sensitive_category 的数据：~0 rows (大约)
DELETE FROM `article_sensitive_category`;

-- 导出  表 nbsaas-admin.article_sensitive_word 结构
DROP TABLE IF EXISTS `article_sensitive_word`;
CREATE TABLE IF NOT EXISTS `article_sensitive_word` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `catalog` int(11) DEFAULT NULL,
  `replace_word` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `word_size` int(11) DEFAULT NULL,
  `word` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4jx243akswqmswta87kwsr4wb` (`category_id`),
  KEY `FKebq1aflt9h1l0hki7qu3292yj` (`user_id`),
  CONSTRAINT `FK4jx243akswqmswta87kwsr4wb` FOREIGN KEY (`category_id`) REFERENCES `article_sensitive_category` (`id`),
  CONSTRAINT `FKebq1aflt9h1l0hki7qu3292yj` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.article_sensitive_word 的数据：~0 rows (大约)
DELETE FROM `article_sensitive_word`;

-- 导出  表 nbsaas-admin.article_tag 结构
DROP TABLE IF EXISTS `article_tag`;
CREATE TABLE IF NOT EXISTS `article_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `amount` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.article_tag 的数据：~0 rows (大约)
DELETE FROM `article_tag`;

-- 导出  表 nbsaas-admin.bs_customer 结构
DROP TABLE IF EXISTS `bs_customer`;
CREATE TABLE IF NOT EXISTS `bs_customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `begin_date` datetime(6) DEFAULT NULL COMMENT '合作日期',
  `introducer` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'introducer',
  `name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '客户名称',
  `note` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '内容',
  `phone` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `score` int(11) DEFAULT NULL COMMENT '关系分数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='客户表';

-- 正在导出表  nbsaas-admin.bs_customer 的数据：~0 rows (大约)
DELETE FROM `bs_customer`;

-- 导出  表 nbsaas-admin.nbsaas_boot_ad 结构
DROP TABLE IF EXISTS `nbsaas_boot_ad`;
CREATE TABLE IF NOT EXISTS `nbsaas_boot_ad` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序号',
  `begin_date` datetime(6) DEFAULT NULL COMMENT '广告开始时间',
  `buss_id` bigint(20) DEFAULT NULL COMMENT '数据id',
  `catalog` int(11) DEFAULT NULL COMMENT '分类',
  `end_date` datetime(6) DEFAULT NULL COMMENT '广告结束时间',
  `note` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '内容',
  `path` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '广告图片',
  `title` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '广告名称',
  `url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '链接地址',
  `ad_position_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKi4ayu50ccdlb54my2ypfvxxxn` (`ad_position_id`),
  CONSTRAINT `FKi4ayu50ccdlb54my2ypfvxxxn` FOREIGN KEY (`ad_position_id`) REFERENCES `nbsaas_boot_ad_position` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='广告';

-- 正在导出表  nbsaas-admin.nbsaas_boot_ad 的数据：~0 rows (大约)
DELETE FROM `nbsaas_boot_ad`;

-- 导出  表 nbsaas-admin.nbsaas_boot_ad_position 结构
DROP TABLE IF EXISTS `nbsaas_boot_ad_position`;
CREATE TABLE IF NOT EXISTS `nbsaas_boot_ad_position` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序号',
  `height` int(11) NOT NULL COMMENT '高度',
  `data_key` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '广告位标识',
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '广告位名称',
  `note` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `template` longtext COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板',
  `width` int(11) NOT NULL COMMENT '宽度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='广告位表';

-- 正在导出表  nbsaas-admin.nbsaas_boot_ad_position 的数据：~0 rows (大约)
DELETE FROM `nbsaas_boot_ad_position`;
INSERT INTO `nbsaas_boot_ad_position` (`id`, `add_date`, `last_date`, `sort_num`, `height`, `data_key`, `name`, `note`, `template`, `width`) VALUES
	(1, '2023-08-13 20:55:30.027000', '2023-08-13 20:55:30.027000', NULL, 0, '21', '312', '', '', 0);

-- 导出  表 nbsaas-admin.nbsaas_common_config 结构
DROP TABLE IF EXISTS `nbsaas_common_config`;
CREATE TABLE IF NOT EXISTS `nbsaas_common_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `class_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '配置类标识',
  `config_data` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '配置json数据',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_gcwxay4sfr5c4x9q23sd6n022` (`class_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='基础配置功能';

-- 正在导出表  nbsaas-admin.nbsaas_common_config 的数据：~0 rows (大约)
DELETE FROM `nbsaas_common_config`;
INSERT INTO `nbsaas_common_config` (`id`, `add_date`, `last_date`, `class_name`, `config_data`) VALUES
	(1, '2023-08-13 17:25:09.713000', '2023-08-13 17:25:09.713000', 'SiteConfig', '{}');

-- 导出  表 nbsaas-admin.scheduling_task 结构
DROP TABLE IF EXISTS `scheduling_task`;
CREATE TABLE IF NOT EXISTS `scheduling_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `alias_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务别名',
  `content` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务内容',
  `cron_expression` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务运行时间表达式',
  `is_sync` int(11) DEFAULT NULL COMMENT '是否异步,1:是;2:否',
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务名称',
  `record_type` int(11) DEFAULT NULL COMMENT '记录类型 1日志记录，2日志不记录',
  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务备注',
  `type` int(11) DEFAULT NULL COMMENT '任务类型,1:url地址请求,2:执行指定命令操作(复制文件,下载文件）,3:打印文本类型',
  `url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务执行url',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='任务计划任务记录表';

-- 正在导出表  nbsaas-admin.scheduling_task 的数据：~2 rows (大约)
DELETE FROM `scheduling_task`;
INSERT INTO `scheduling_task` (`id`, `add_date`, `last_date`, `alias_name`, `content`, `cron_expression`, `is_sync`, `name`, `record_type`, `remark`, `type`, `url`) VALUES
	(1, '2023-08-17 23:16:23.000000', '2023-08-17 23:53:00.490000', '', '', '0 0 * * * ?', NULL, '访问网站', 1, '', NULL, 'http://www.newbyte.ltd/index.htm'),
	(2, '2023-08-17 23:55:33.776000', '2023-08-17 23:55:33.776000', '', '', '0 0 0 * * ?', NULL, '访问nbsaas.com', 1, '', NULL, 'http://www.nbsaas.com/index.htm');

-- 导出  表 nbsaas-admin.scheduling_task_record 结构
DROP TABLE IF EXISTS `scheduling_task_record`;
CREATE TABLE IF NOT EXISTS `scheduling_task_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `note` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务执行完返回的结果',
  `state` int(11) DEFAULT NULL COMMENT '执行状态 1成功，2失败',
  `scheduling_task_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='定时任务记录';

-- 正在导出表  nbsaas-admin.scheduling_task_record 的数据：~0 rows (大约)
DELETE FROM `scheduling_task_record`;

-- 导出  表 nbsaas-admin.sys_app 结构
DROP TABLE IF EXISTS `sys_app`;
CREATE TABLE IF NOT EXISTS `sys_app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `app_key` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '应用key',
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '应用名称',
  `note` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '应用介绍',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.sys_app 的数据：~2 rows (大约)
DELETE FROM `sys_app`;
INSERT INTO `sys_app` (`id`, `add_date`, `last_date`, `app_key`, `name`, `note`) VALUES
	(1, '2023-06-23 13:38:30.789000', '2023-06-23 13:38:30.789000', NULL, NULL, NULL),
	(2, '2023-06-23 13:39:45.117000', '2023-06-23 13:41:50.133000', '123dfgd', '123fg', NULL);

-- 导出  表 nbsaas-admin.sys_app_menu 结构
DROP TABLE IF EXISTS `sys_app_menu`;
CREATE TABLE IF NOT EXISTS `sys_app_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `code` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '编码',
  `depth` int(11) DEFAULT NULL COMMENT '深度',
  `ids` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'ids',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `lft` int(11) DEFAULT NULL COMMENT '左节点',
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `rgt` int(11) DEFAULT NULL COMMENT '右节点',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序号',
  `catalog` int(11) DEFAULT NULL COMMENT '分类',
  `creator` bigint(20) DEFAULT NULL COMMENT '创建人id',
  `icon` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标',
  `menu_type` int(11) DEFAULT NULL COMMENT '菜单类型',
  `num` bigint(20) DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '路径',
  `permission` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '权限',
  `router` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '路由',
  `app_id` bigint(20) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKa21putlh2htcv39jrkgg1bed0` (`app_id`),
  KEY `FK2enr7xrsc790sg3jb4gwlq76q` (`parent_id`),
  CONSTRAINT `FK2enr7xrsc790sg3jb4gwlq76q` FOREIGN KEY (`parent_id`) REFERENCES `sys_app_menu` (`id`),
  CONSTRAINT `FKa21putlh2htcv39jrkgg1bed0` FOREIGN KEY (`app_id`) REFERENCES `sys_app` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.sys_app_menu 的数据：~0 rows (大约)
DELETE FROM `sys_app_menu`;

-- 导出  表 nbsaas-admin.sys_dict 结构
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE IF NOT EXISTS `sys_dict` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `dict_key` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字典key',
  `title` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字典名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='字典表';

-- 正在导出表  nbsaas-admin.sys_dict 的数据：~0 rows (大约)
DELETE FROM `sys_dict`;
INSERT INTO `sys_dict` (`id`, `add_date`, `last_date`, `dict_key`, `title`) VALUES
	(1, '2023-06-24 21:26:49.384000', '2023-07-05 22:33:13.819000', '11', '纷纷');

-- 导出  表 nbsaas-admin.sys_dict_item 结构
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE IF NOT EXISTS `sys_dict_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `data_code` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '编码',
  `data_value` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '键值',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序字段',
  `store_state` int(11) DEFAULT NULL,
  `dict_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdem7dtr28pt4kqu9rk7strqdj` (`dict_id`),
  CONSTRAINT `FKdem7dtr28pt4kqu9rk7strqdj` FOREIGN KEY (`dict_id`) REFERENCES `sys_dict` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='字典子项';

-- 正在导出表  nbsaas-admin.sys_dict_item 的数据：~2 rows (大约)
DELETE FROM `sys_dict_item`;
INSERT INTO `sys_dict_item` (`id`, `add_date`, `last_date`, `data_code`, `data_value`, `sort_num`, `store_state`, `dict_id`) VALUES
	(1, '2023-06-24 21:26:53.700000', '2023-06-24 21:26:53.700000', '123', '123', 0, NULL, 1),
	(2, '2023-06-24 21:26:57.093000', '2023-06-24 21:26:57.093000', '123', '123', 0, NULL, 1);

-- 导出  表 nbsaas-admin.sys_error_log 结构
DROP TABLE IF EXISTS `sys_error_log`;
CREATE TABLE IF NOT EXISTS `sys_error_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `app` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `param` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `server_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.sys_error_log 的数据：~0 rows (大约)
DELETE FROM `sys_error_log`;

-- 导出  表 nbsaas-admin.sys_menu 结构
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE IF NOT EXISTS `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `code` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '编码',
  `depth` int(11) DEFAULT NULL COMMENT '深度',
  `ids` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'ids',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `lft` int(11) DEFAULT NULL COMMENT '左节点',
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `rgt` int(11) DEFAULT NULL COMMENT '右节点',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序号',
  `catalog` int(11) DEFAULT NULL COMMENT '菜单类型',
  `icon` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `menu_type` int(11) DEFAULT NULL COMMENT '是否租户使用',
  `nums` bigint(20) DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `router` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2jrf4gb0gjqi8882gxytpxnhe` (`parent_id`),
  CONSTRAINT `FK2jrf4gb0gjqi8882gxytpxnhe` FOREIGN KEY (`parent_id`) REFERENCES `sys_menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.sys_menu 的数据：~14 rows (大约)
DELETE FROM `sys_menu`;
INSERT INTO `sys_menu` (`id`, `add_date`, `code`, `depth`, `ids`, `last_date`, `lft`, `name`, `rgt`, `sort_num`, `catalog`, `icon`, `menu_type`, `nums`, `path`, `permission`, `router`, `parent_id`) VALUES
	(1, NULL, '', 1, '', NULL, NULL, '首页', NULL, 0, NULL, 'fa fa-home', NULL, NULL, '/home', '', '/home', NULL),
	(2, NULL, '', 1, '', NULL, NULL, '系统设置', NULL, 100, NULL, 'fa  fa-gear', NULL, NULL, '', '', '/setting', NULL),
	(3, NULL, NULL, NULL, NULL, NULL, NULL, '菜单管理', NULL, 1, NULL, 'fa fa-solid fa-bars', NULL, NULL, '', 'menu', '/menu/index', 2),
	(5, NULL, '', NULL, '', NULL, NULL, '广告管理', NULL, 0, NULL, 'fa fa-light fa-life-ring', NULL, NULL, '', '', '/ad_home', NULL),
	(6, NULL, NULL, NULL, NULL, NULL, NULL, '角色管理', NULL, 1, NULL, 'fa fa-users', NULL, NULL, '', 'role', '/role/index', 2),
	(16, NULL, '', NULL, '', NULL, NULL, '个人信息', NULL, 0, NULL, 'fa fa-user', NULL, NULL, '', 'userInfo', '/usercenter/index', 2),
	(17, NULL, NULL, NULL, NULL, NULL, NULL, '数据字典', NULL, 2, NULL, 'fa fa-solid fa-book-journal-whills', NULL, NULL, '', 'dict', '/dict/index', 2),
	(18, NULL, NULL, NULL, NULL, NULL, NULL, '系统配置', NULL, 111, NULL, 'fa  fa-gear', NULL, NULL, '', '', '/system/index', 2),
	(38, NULL, '', NULL, '', NULL, NULL, '广告管理', NULL, 0, NULL, 'fa fa-thin fa-compass', NULL, NULL, '', 'ad', '/ad/index', 5),
	(39, NULL, '', NULL, '', NULL, NULL, '广告位管理', NULL, 0, NULL, 'fa-regular fa-circle', NULL, NULL, '', 'adPosition', '/adPosition/index', 5),
	(67, NULL, NULL, 1, NULL, NULL, NULL, '办公系统', NULL, 0, NULL, 'fa fa-solid fa-bars', NULL, NULL, '', '', '', NULL),
	(68, NULL, NULL, 2, NULL, NULL, NULL, '部门管理', NULL, 0, NULL, 'fa fa-solid fa-bars', NULL, NULL, '', 'structure', '/structure/index', 67),
	(76, NULL, NULL, 2, NULL, NULL, NULL, '用户管理', NULL, 1, NULL, 'fa fa-user', NULL, NULL, '', 'user', '/user/index', 2),
	(77, NULL, NULL, 2, '77', NULL, NULL, '定时任务', NULL, 0, NULL, 'fa fa-thin fa-compass', NULL, NULL, '', 'schedulingTask,schedulingTaskRecord', '/schedulingTask/index', 2);

-- 导出  表 nbsaas-admin.sys_mock 结构
DROP TABLE IF EXISTS `sys_mock`;
CREATE TABLE IF NOT EXISTS `sys_mock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `content` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.sys_mock 的数据：~0 rows (大约)
DELETE FROM `sys_mock`;

-- 导出  表 nbsaas-admin.sys_record_log 结构
DROP TABLE IF EXISTS `sys_record_log`;
CREATE TABLE IF NOT EXISTS `sys_record_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `app` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_date` datetime(6) DEFAULT NULL,
  `create_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `data` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ip` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.sys_record_log 的数据：~0 rows (大约)
DELETE FROM `sys_record_log`;

-- 导出  表 nbsaas-admin.sys_role 结构
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE IF NOT EXISTS `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='角色表 ';

-- 正在导出表  nbsaas-admin.sys_role 的数据：~0 rows (大约)
DELETE FROM `sys_role`;
INSERT INTO `sys_role` (`id`, `add_date`, `last_date`, `name`, `remark`) VALUES
	(1, '2023-06-23 11:43:12.000000', '2023-06-23 12:00:58.681000', '管理员', 'fdsd');

-- 导出  表 nbsaas-admin.sys_role_menu 结构
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE IF NOT EXISTS `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `menu_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKf3mud4qoc7ayew8nml4plkevo` (`menu_id`),
  KEY `FKkeitxsgxwayackgqllio4ohn5` (`role_id`),
  CONSTRAINT `FKf3mud4qoc7ayew8nml4plkevo` FOREIGN KEY (`menu_id`) REFERENCES `sys_menu` (`id`),
  CONSTRAINT `FKkeitxsgxwayackgqllio4ohn5` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='角色功能表';

-- 正在导出表  nbsaas-admin.sys_role_menu 的数据：~13 rows (大约)
DELETE FROM `sys_role_menu`;
INSERT INTO `sys_role_menu` (`id`, `add_date`, `last_date`, `menu_id`, `role_id`) VALUES
	(337, '2023-08-17 23:03:39.899000', '2023-08-17 23:03:39.899000', 1, 1),
	(338, '2023-08-17 23:03:39.901000', '2023-08-17 23:03:39.901000', 5, 1),
	(339, '2023-08-17 23:03:39.902000', '2023-08-17 23:03:39.902000', 38, 1),
	(340, '2023-08-17 23:03:39.902000', '2023-08-17 23:03:39.902000', 39, 1),
	(341, '2023-08-17 23:03:39.903000', '2023-08-17 23:03:39.903000', 67, 1),
	(342, '2023-08-17 23:03:39.904000', '2023-08-17 23:03:39.904000', 68, 1),
	(343, '2023-08-17 23:03:39.905000', '2023-08-17 23:03:39.905000', 2, 1),
	(344, '2023-08-17 23:03:39.905000', '2023-08-17 23:03:39.905000', 16, 1),
	(345, '2023-08-17 23:03:39.906000', '2023-08-17 23:03:39.906000', 77, 1),
	(346, '2023-08-17 23:03:39.906000', '2023-08-17 23:03:39.906000', 3, 1),
	(347, '2023-08-17 23:03:39.907000', '2023-08-17 23:03:39.907000', 6, 1),
	(348, '2023-08-17 23:03:39.907000', '2023-08-17 23:03:39.907000', 76, 1),
	(349, '2023-08-17 23:03:39.907000', '2023-08-17 23:03:39.907000', 17, 1),
	(350, '2023-08-17 23:03:39.908000', '2023-08-17 23:03:39.908000', 18, 1);

-- 导出  表 nbsaas-admin.sys_sequence 结构
DROP TABLE IF EXISTS `sys_sequence`;
CREATE TABLE IF NOT EXISTS `sys_sequence` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `create_date` datetime(6) DEFAULT NULL,
  `current_num` bigint(20) DEFAULT NULL,
  `increment` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `update_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.sys_sequence 的数据：~0 rows (大约)
DELETE FROM `sys_sequence`;

-- 导出  表 nbsaas-admin.sys_structure 结构
DROP TABLE IF EXISTS `sys_structure`;
CREATE TABLE IF NOT EXISTS `sys_structure` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `code` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '编码',
  `depth` int(11) DEFAULT NULL COMMENT '深度',
  `ids` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'ids',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `lft` int(11) DEFAULT NULL COMMENT '左节点',
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `rgt` int(11) DEFAULT NULL COMMENT '右节点',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序号',
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKt36aqf18w4kny9gd6xnxkb9o0` (`parent_id`),
  CONSTRAINT `FKt36aqf18w4kny9gd6xnxkb9o0` FOREIGN KEY (`parent_id`) REFERENCES `sys_structure` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='组织架构';

-- 正在导出表  nbsaas-admin.sys_structure 的数据：~7 rows (大约)
DELETE FROM `sys_structure`;
INSERT INTO `sys_structure` (`id`, `add_date`, `code`, `depth`, `ids`, `last_date`, `lft`, `name`, `rgt`, `sort_num`, `parent_id`) VALUES
	(1, '2023-07-03 22:52:48.000000', NULL, 1, '1', NULL, NULL, '测试1', NULL, 11, NULL),
	(3, '2023-07-03 22:52:47.000000', NULL, 2, '1-3', NULL, NULL, 'gfdfgdfg', NULL, 12312, 1),
	(4, '2023-07-03 22:52:47.000000', NULL, 2, '1-4', NULL, NULL, '321', NULL, 31, 1),
	(11, NULL, '123', 2, '1-11', NULL, NULL, '123', NULL, NULL, 1),
	(12, NULL, '123', 3, '1-11-12', NULL, NULL, '123', NULL, NULL, 11),
	(13, NULL, '123123', 3, '1-11-13', NULL, NULL, '123', NULL, NULL, 11);

-- 导出  表 nbsaas-admin.todo 结构
DROP TABLE IF EXISTS `todo`;
CREATE TABLE IF NOT EXISTS `todo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `data_id` bigint(20) DEFAULT NULL,
  `data_key` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `handler` bigint(20) DEFAULT NULL,
  `handler_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `navigate_url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `owner_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKry89lgog184maw56mvs4a6i2g` (`owner_id`),
  CONSTRAINT `FKry89lgog184maw56mvs4a6i2g` FOREIGN KEY (`owner_id`) REFERENCES `user_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.todo 的数据：~0 rows (大约)
DELETE FROM `todo`;

-- 导出  表 nbsaas-admin.todo_template 结构
DROP TABLE IF EXISTS `todo_template`;
CREATE TABLE IF NOT EXISTS `todo_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `data_key` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ext_data` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `navigate_url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.todo_template 的数据：~0 rows (大约)
DELETE FROM `todo_template`;

-- 导出  表 nbsaas-admin.user_access_log 结构
DROP TABLE IF EXISTS `user_access_log`;
CREATE TABLE IF NOT EXISTS `user_access_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `consume_time` bigint(20) DEFAULT NULL COMMENT '消耗时间',
  `ip` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'ip地址',
  `store_state` int(11) DEFAULT NULL COMMENT '存储地址',
  `url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'url地址',
  `creator_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKi0347jxfpv2i5b9crf8ckbg5w` (`creator_id`),
  CONSTRAINT `FKi0347jxfpv2i5b9crf8ckbg5w` FOREIGN KEY (`creator_id`) REFERENCES `user_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='访问日志';

-- 正在导出表  nbsaas-admin.user_access_log 的数据：~386 rows (大约)
DELETE FROM `user_access_log`;
INSERT INTO `user_access_log` (`id`, `add_date`, `last_date`, `consume_time`, `ip`, `store_state`, `url`, `creator_id`) VALUES
	(1, '2023-08-12 17:08:12.237000', '2023-08-12 17:08:12.237000', 172, '127.0.0.1', 1, '/user/login', 3),
	(2, '2023-08-12 17:08:12.350000', '2023-08-12 17:08:12.350000', 5, '127.0.0.1', 1, '/user/current', 3),
	(3, '2023-08-12 17:08:12.443000', '2023-08-12 17:08:12.443000', 70, '127.0.0.1', 1, '/menu/tree', 3),
	(4, '2023-08-12 17:08:15.716000', '2023-08-12 17:08:15.716000', 18, '127.0.0.1', 1, '/role/search', 3),
	(5, '2023-08-12 17:08:16.254000', '2023-08-12 17:08:16.254000', 19, '127.0.0.1', 1, '/staff/search', 3),
	(6, '2023-08-12 17:08:16.258000', '2023-08-12 17:08:16.258000', 23, '127.0.0.1', 1, '/structure/list', 3),
	(7, '2023-08-12 17:49:14.454000', '2023-08-12 17:49:14.454000', 19, '127.0.0.1', 1, '/user/login', 3),
	(8, '2023-08-12 17:49:14.529000', '2023-08-12 17:49:14.529000', 4, '127.0.0.1', 1, '/user/current', 3),
	(9, '2023-08-12 17:49:14.563000', '2023-08-12 17:49:14.563000', 10, '127.0.0.1', 1, '/menu/tree', 3),
	(10, '2023-08-12 17:49:20.893000', '2023-08-12 17:49:20.893000', 46, '127.0.0.1', 1, '/menu/root', 3),
	(11, '2023-08-12 17:49:21.632000', '2023-08-12 17:49:21.632000', 3, '127.0.0.1', 1, '/role/search', 3),
	(12, '2023-08-12 17:49:24.031000', '2023-08-12 17:49:24.031000', 39, '127.0.0.1', 1, '/menu/root', 3),
	(13, '2023-08-12 17:49:24.055000', '2023-08-12 17:49:24.055000', 10, '127.0.0.1', 1, '/menu/selectForPermission', 3),
	(14, '2023-08-13 17:17:47.471000', '2023-08-13 17:17:47.471000', 40, '127.0.0.1', 1, '/user/login', 1),
	(15, '2023-08-13 17:17:47.502000', '2023-08-13 17:17:47.502000', 4, '127.0.0.1', 1, '/user/current', 1),
	(16, '2023-08-13 17:17:47.566000', '2023-08-13 17:17:47.566000', 52, '127.0.0.1', 1, '/menu/tree', 1),
	(17, '2023-08-13 17:18:03.949000', '2023-08-13 17:18:03.949000', 7, '127.0.0.1', 1, '/user/login', 3),
	(18, '2023-08-13 17:18:03.980000', '2023-08-13 17:18:03.980000', 3, '127.0.0.1', 1, '/user/current', 3),
	(19, '2023-08-13 17:18:04.022000', '2023-08-13 17:18:04.022000', 14, '127.0.0.1', 1, '/menu/tree', 3),
	(20, '2023-08-13 17:18:12.489000', '2023-08-13 17:18:12.489000', 15, '127.0.0.1', 1, '/menu/root', 3),
	(21, '2023-08-13 17:18:13.557000', '2023-08-13 17:18:13.557000', 18, '127.0.0.1', 1, '/role/search', 3),
	(22, '2023-08-13 17:18:15.489000', '2023-08-13 17:18:15.489000', 11, '127.0.0.1', 1, '/menu/root', 3),
	(23, '2023-08-13 17:18:15.529000', '2023-08-13 17:18:15.529000', 12, '127.0.0.1', 1, '/menu/selectForPermission', 3),
	(24, '2023-08-13 17:18:19.466000', '2023-08-13 17:18:19.466000', 69, '127.0.0.1', 1, '/menu/updateRoleMenus', 3),
	(25, '2023-08-13 17:18:26.764000', '2023-08-13 17:18:26.764000', 38, '127.0.0.1', 1, '/role/delete', 3),
	(26, '2023-08-13 17:18:26.793000', '2023-08-13 17:18:26.793000', 13, '127.0.0.1', 1, '/role/delete', 3),
	(27, '2023-08-13 17:18:26.827000', '2023-08-13 17:18:26.827000', 16, '127.0.0.1', 1, '/role/delete', 3),
	(28, '2023-08-13 17:18:26.854000', '2023-08-13 17:18:26.854000', 11, '127.0.0.1', 1, '/role/delete', 3),
	(29, '2023-08-13 17:18:26.886000', '2023-08-13 17:18:26.886000', 14, '127.0.0.1', 1, '/role/delete', 3),
	(30, '2023-08-13 17:18:26.918000', '2023-08-13 17:18:26.918000', 14, '127.0.0.1', 1, '/role/delete', 3),
	(31, '2023-08-13 17:18:26.948000', '2023-08-13 17:18:26.948000', 12, '127.0.0.1', 1, '/role/delete', 3),
	(32, '2023-08-13 17:18:26.980000', '2023-08-13 17:18:26.980000', 14, '127.0.0.1', 1, '/role/delete', 3),
	(33, '2023-08-13 17:18:27.013000', '2023-08-13 17:18:27.013000', 16, '127.0.0.1', 1, '/role/delete', 3),
	(34, '2023-08-13 17:18:27.034000', '2023-08-13 17:18:27.034000', 3, '127.0.0.1', 1, '/role/search', 3),
	(35, '2023-08-13 17:18:30.898000', '2023-08-13 17:18:30.898000', 8, '127.0.0.1', 1, '/role/delete', 3),
	(36, '2023-08-13 17:18:30.927000', '2023-08-13 17:18:30.927000', 8, '127.0.0.1', 1, '/role/delete', 3),
	(37, '2023-08-13 17:18:30.949000', '2023-08-13 17:18:30.949000', 2, '127.0.0.1', 1, '/role/search', 3),
	(38, '2023-08-13 17:18:34.165000', '2023-08-13 17:18:34.165000', 10, '127.0.0.1', 1, '/menu/root', 3),
	(39, '2023-08-13 17:18:34.489000', '2023-08-13 17:18:34.489000', 3, '127.0.0.1', 1, '/user/current', 3),
	(40, '2023-08-13 17:18:34.489000', '2023-08-13 17:18:34.489000', 2, '127.0.0.1', 1, '/user/current', 3),
	(41, '2023-08-13 17:18:35.685000', '2023-08-13 17:18:35.685000', 16, '127.0.0.1', 1, '/structure/list', 3),
	(42, '2023-08-13 17:18:50.542000', '2023-08-13 17:18:50.542000', 6, '127.0.0.1', 1, '/user/login', 3),
	(43, '2023-08-13 17:18:50.579000', '2023-08-13 17:18:50.579000', 1, '127.0.0.1', 1, '/user/current', 3),
	(44, '2023-08-13 17:18:50.590000', '2023-08-13 17:18:50.590000', 4, '127.0.0.1', 1, '/menu/tree', 3),
	(45, '2023-08-13 17:18:53.314000', '2023-08-13 17:18:53.314000', 3, '127.0.0.1', 1, '/user/current', 3),
	(46, '2023-08-13 17:18:53.314000', '2023-08-13 17:18:53.314000', 3, '127.0.0.1', 1, '/user/current', 3),
	(47, '2023-08-13 17:18:53.814000', '2023-08-13 17:18:53.814000', 9, '127.0.0.1', 1, '/menu/root', 3),
	(48, '2023-08-13 17:18:54.161000', '2023-08-13 17:18:54.161000', 3, '127.0.0.1', 1, '/role/search', 3),
	(49, '2023-08-13 17:18:57.746000', '2023-08-13 17:18:57.746000', 6, '127.0.0.1', 1, '/structure/list', 3),
	(50, '2023-08-13 17:18:57.754000', '2023-08-13 17:18:57.754000', 12, '127.0.0.1', 1, '/userInfo/search', 3),
	(51, '2023-08-13 17:19:03.647000', '2023-08-13 17:19:03.647000', 1, '127.0.0.1', 1, '/userInfo/resetPassword', 3),
	(52, '2023-08-13 17:19:04.952000', '2023-08-13 17:19:04.952000', 1, '127.0.0.1', 1, '/userInfo/resetPassword', 3),
	(53, '2023-08-13 17:19:05.152000', '2023-08-13 17:19:05.152000', 1, '127.0.0.1', 1, '/userInfo/resetPassword', 3),
	(54, '2023-08-13 17:19:05.355000', '2023-08-13 17:19:05.355000', 0, '127.0.0.1', 1, '/userInfo/resetPassword', 3),
	(55, '2023-08-13 17:19:05.552000', '2023-08-13 17:19:05.552000', 1, '127.0.0.1', 1, '/userInfo/resetPassword', 3),
	(56, '2023-08-13 17:21:13.048000', '2023-08-13 17:21:13.048000', 21, '127.0.0.1', 1, '/user/login', 3),
	(57, '2023-08-13 17:21:13.098000', '2023-08-13 17:21:13.098000', 5, '127.0.0.1', 1, '/user/current', 3),
	(58, '2023-08-13 17:21:13.168000', '2023-08-13 17:21:13.168000', 55, '127.0.0.1', 1, '/menu/tree', 3),
	(59, '2023-08-13 17:21:19.230000', '2023-08-13 17:21:19.230000', 1, '127.0.0.1', 1, '/ad/search', 3),
	(60, '2023-08-13 17:21:19.229000', '2023-08-13 17:21:19.229000', 1, '127.0.0.1', 1, '/adPosition/list', 3),
	(61, '2023-08-13 17:21:19.813000', '2023-08-13 17:21:19.813000', 1, '127.0.0.1', 1, '/adPosition/search', 3),
	(62, '2023-08-13 17:21:21.416000', '2023-08-13 17:21:21.416000', 4, '127.0.0.1', 1, '/user/current', 3),
	(63, '2023-08-13 17:21:21.416000', '2023-08-13 17:21:21.416000', 4, '127.0.0.1', 1, '/user/current', 3),
	(64, '2023-08-13 17:21:21.802000', '2023-08-13 17:21:21.802000', 15, '127.0.0.1', 1, '/menu/root', 3),
	(65, '2023-08-13 17:21:25.158000', '2023-08-13 17:21:25.158000', 21, '127.0.0.1', 1, '/role/search', 3),
	(66, '2023-08-13 17:21:25.499000', '2023-08-13 17:21:25.499000', 5, '127.0.0.1', 1, '/userInfo/search', 3),
	(67, '2023-08-13 17:21:25.508000', '2023-08-13 17:21:25.508000', 14, '127.0.0.1', 1, '/structure/list', 3),
	(68, '2023-08-13 17:21:33.859000', '2023-08-13 17:21:33.859000', 23, '127.0.0.1', 1, '/userInfo/resetPassword', 3),
	(69, '2023-08-13 17:21:38.132000', '2023-08-13 17:21:38.132000', 6, '127.0.0.1', 1, '/user/login', 1),
	(70, '2023-08-13 17:21:38.146000', '2023-08-13 17:21:38.146000', 2, '127.0.0.1', 1, '/user/current', 1),
	(71, '2023-08-13 17:21:38.161000', '2023-08-13 17:21:38.161000', 2, '127.0.0.1', 1, '/menu/tree', 1),
	(72, '2023-08-13 17:21:46.253000', '2023-08-13 17:21:46.253000', 6, '127.0.0.1', 1, '/user/login', 3),
	(73, '2023-08-13 17:21:46.294000', '2023-08-13 17:21:46.294000', 3, '127.0.0.1', 1, '/user/current', 3),
	(74, '2023-08-13 17:21:46.310000', '2023-08-13 17:21:46.310000', 4, '127.0.0.1', 1, '/menu/tree', 3),
	(75, '2023-08-13 17:21:48.641000', '2023-08-13 17:21:48.641000', 9, '127.0.0.1', 1, '/menu/root', 3),
	(76, '2023-08-13 17:21:49.084000', '2023-08-13 17:21:49.084000', 2, '127.0.0.1', 1, '/role/search', 3),
	(77, '2023-08-13 17:21:50.257000', '2023-08-13 17:21:50.257000', 5, '127.0.0.1', 1, '/userInfo/search', 3),
	(78, '2023-08-13 17:21:50.258000', '2023-08-13 17:21:50.258000', 5, '127.0.0.1', 1, '/structure/list', 3),
	(79, '2023-08-13 17:21:51.729000', '2023-08-13 17:21:51.729000', 2, '127.0.0.1', 1, '/role/list', 3),
	(80, '2023-08-13 17:21:51.730000', '2023-08-13 17:21:51.730000', 5, '127.0.0.1', 1, '/structure/list', 3),
	(81, '2023-08-13 17:21:51.778000', '2023-08-13 17:21:51.778000', 13, '127.0.0.1', 1, '/userRole/list', 3),
	(82, '2023-08-13 17:21:58.235000', '2023-08-13 17:21:58.235000', 2, '127.0.0.1', 1, '/role/list', 3),
	(83, '2023-08-13 17:21:58.237000', '2023-08-13 17:21:58.237000', 4, '127.0.0.1', 1, '/structure/list', 3),
	(84, '2023-08-13 17:21:58.268000', '2023-08-13 17:21:58.268000', 3, '127.0.0.1', 1, '/userRole/list', 3),
	(85, '2023-08-13 17:22:03.390000', '2023-08-13 17:22:03.390000', 28, '127.0.0.1', 1, '/userInfo/update', 3),
	(86, '2023-08-13 17:22:03.434000', '2023-08-13 17:22:03.434000', 25, '127.0.0.1', 1, '/userRole/updateRole', 3),
	(87, '2023-08-13 17:22:03.465000', '2023-08-13 17:22:03.465000', 5, '127.0.0.1', 1, '/userInfo/search', 3),
	(88, '2023-08-13 17:22:08.503000', '2023-08-13 17:22:08.503000', 3, '127.0.0.1', 1, '/role/list', 3),
	(89, '2023-08-13 17:22:08.505000', '2023-08-13 17:22:08.505000', 5, '127.0.0.1', 1, '/structure/list', 3),
	(90, '2023-08-13 17:22:08.552000', '2023-08-13 17:22:08.552000', 2, '127.0.0.1', 1, '/userRole/list', 3),
	(91, '2023-08-13 17:22:12.334000', '2023-08-13 17:22:12.334000', 19, '127.0.0.1', 1, '/userInfo/update', 3),
	(92, '2023-08-13 17:22:12.364000', '2023-08-13 17:22:12.364000', 13, '127.0.0.1', 1, '/userRole/updateRole', 3),
	(93, '2023-08-13 17:22:12.394000', '2023-08-13 17:22:12.394000', 4, '127.0.0.1', 1, '/userInfo/search', 3),
	(94, '2023-08-13 17:22:18.335000', '2023-08-13 17:22:18.335000', 6, '127.0.0.1', 1, '/user/login', 1),
	(95, '2023-08-13 17:22:18.347000', '2023-08-13 17:22:18.347000', 3, '127.0.0.1', 1, '/user/current', 1),
	(96, '2023-08-13 17:22:18.362000', '2023-08-13 17:22:18.362000', 4, '127.0.0.1', 1, '/menu/tree', 1),
	(97, '2023-08-13 17:22:20.356000', '2023-08-13 17:22:20.356000', 3, '127.0.0.1', 1, '/user/current', 1),
	(98, '2023-08-13 17:22:20.356000', '2023-08-13 17:22:20.356000', 3, '127.0.0.1', 1, '/user/current', 1),
	(99, '2023-08-13 17:22:20.933000', '2023-08-13 17:22:20.933000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(100, '2023-08-13 17:22:21.471000', '2023-08-13 17:22:21.471000', 2, '127.0.0.1', 1, '/role/search', 1),
	(101, '2023-08-13 17:22:22.335000', '2023-08-13 17:22:22.335000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(102, '2023-08-13 17:22:22.831000', '2023-08-13 17:22:22.831000', 3, '127.0.0.1', 1, '/user/current', 1),
	(103, '2023-08-13 17:22:22.831000', '2023-08-13 17:22:22.831000', 3, '127.0.0.1', 1, '/user/current', 1),
	(104, '2023-08-13 17:22:23.788000', '2023-08-13 17:22:23.788000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(105, '2023-08-13 17:22:24.921000', '2023-08-13 17:22:24.921000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(106, '2023-08-13 17:22:25.614000', '2023-08-13 17:22:25.614000', 3, '127.0.0.1', 1, '/user/current', 1),
	(107, '2023-08-13 17:22:25.614000', '2023-08-13 17:22:25.615000', 4, '127.0.0.1', 1, '/user/current', 1),
	(108, '2023-08-13 17:22:26.085000', '2023-08-13 17:22:26.085000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(109, '2023-08-13 17:22:26.389000', '2023-08-13 17:22:26.389000', 2, '127.0.0.1', 1, '/role/search', 1),
	(110, '2023-08-13 17:22:26.714000', '2023-08-13 17:22:26.714000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(111, '2023-08-13 17:22:26.715000', '2023-08-13 17:22:26.715000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(112, '2023-08-13 17:22:27.137000', '2023-08-13 17:22:27.137000', 13, '127.0.0.1', 1, '/dict/list', 1),
	(113, '2023-08-13 17:22:27.625000', '2023-08-13 17:22:27.625000', 1, '127.0.0.1', 1, '/config/info', 1),
	(114, '2023-08-13 17:22:28.069000', '2023-08-13 17:22:28.069000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(115, '2023-08-13 17:22:28.385000', '2023-08-13 17:22:28.385000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(116, '2023-08-13 17:22:28.385000', '2023-08-13 17:22:28.385000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(117, '2023-08-13 17:22:28.703000', '2023-08-13 17:22:28.703000', 2, '127.0.0.1', 1, '/role/search', 1),
	(118, '2023-08-13 17:22:29.107000', '2023-08-13 17:22:29.107000', 3, '127.0.0.1', 1, '/dict/list', 1),
	(119, '2023-08-13 17:22:29.526000', '2023-08-13 17:22:29.526000', 1, '127.0.0.1', 1, '/config/info', 1),
	(120, '2023-08-13 17:22:30.498000', '2023-08-13 17:22:30.498000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(121, '2023-08-13 17:22:31.022000', '2023-08-13 17:22:31.022000', 3, '127.0.0.1', 1, '/role/search', 1),
	(122, '2023-08-13 17:22:31.354000', '2023-08-13 17:22:31.354000', 13, '127.0.0.1', 1, '/menu/root', 1),
	(123, '2023-08-13 17:22:31.687000', '2023-08-13 17:22:31.687000', 3, '127.0.0.1', 1, '/user/current', 1),
	(124, '2023-08-13 17:22:31.687000', '2023-08-13 17:22:31.687000', 2, '127.0.0.1', 1, '/user/current', 1),
	(125, '2023-08-13 17:24:57.316000', '2023-08-13 17:24:57.316000', 181, '127.0.0.1', 1, '/user/login', 1),
	(126, '2023-08-13 17:24:57.372000', '2023-08-13 17:24:57.372000', 4, '127.0.0.1', 1, '/user/current', 1),
	(127, '2023-08-13 17:24:57.434000', '2023-08-13 17:24:57.434000', 54, '127.0.0.1', 1, '/menu/tree', 1),
	(128, '2023-08-13 17:24:59.035000', '2023-08-13 17:24:59.035000', 17, '127.0.0.1', 1, '/adPosition/list', 1),
	(129, '2023-08-13 17:24:59.036000', '2023-08-13 17:24:59.036000', 18, '127.0.0.1', 1, '/ad/search', 1),
	(130, '2023-08-13 17:24:59.723000', '2023-08-13 17:24:59.723000', 5, '127.0.0.1', 1, '/adPosition/search', 1),
	(131, '2023-08-13 17:25:00.517000', '2023-08-13 17:25:00.517000', 3, '127.0.0.1', 1, '/ad/search', 1),
	(132, '2023-08-13 17:25:00.517000', '2023-08-13 17:25:00.517000', 3, '127.0.0.1', 1, '/adPosition/list', 1),
	(133, '2023-08-13 17:25:02.167000', '2023-08-13 17:25:02.167000', 21, '127.0.0.1', 1, '/structure/list', 1),
	(134, '2023-08-13 17:25:03.460000', '2023-08-13 17:25:03.460000', 4, '127.0.0.1', 1, '/user/current', 1),
	(135, '2023-08-13 17:25:03.460000', '2023-08-13 17:25:03.460000', 4, '127.0.0.1', 1, '/user/current', 1),
	(136, '2023-08-13 17:25:04.027000', '2023-08-13 17:25:04.027000', 12, '127.0.0.1', 1, '/menu/root', 1),
	(137, '2023-08-13 17:25:04.817000', '2023-08-13 17:25:04.817000', 13, '127.0.0.1', 1, '/role/search', 1),
	(138, '2023-08-13 17:25:05.436000', '2023-08-13 17:25:05.436000', 7, '127.0.0.1', 1, '/structure/list', 1),
	(139, '2023-08-13 17:25:05.441000', '2023-08-13 17:25:05.441000', 12, '127.0.0.1', 1, '/userInfo/search', 1),
	(140, '2023-08-13 17:25:08.969000', '2023-08-13 17:25:08.969000', 14, '127.0.0.1', 1, '/dict/list', 1),
	(141, '2023-08-13 17:25:09.727000', '2023-08-13 17:25:09.727000', 27, '127.0.0.1', 1, '/config/info', 1),
	(142, '2023-08-13 17:25:10.420000', '2023-08-13 17:25:10.420000', 3, '127.0.0.1', 1, '/dict/list', 1),
	(143, '2023-08-13 17:25:11.149000', '2023-08-13 17:25:11.149000', 4, '127.0.0.1', 1, '/config/info', 1),
	(144, '2023-08-13 17:25:11.505000', '2023-08-13 17:25:11.505000', 3, '127.0.0.1', 1, '/dict/list', 1),
	(145, '2023-08-13 17:25:11.855000', '2023-08-13 17:25:11.855000', 5, '127.0.0.1', 1, '/userInfo/search', 1),
	(146, '2023-08-13 17:25:11.855000', '2023-08-13 17:25:11.855000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(147, '2023-08-13 17:25:12.121000', '2023-08-13 17:25:12.121000', 2, '127.0.0.1', 1, '/role/search', 1),
	(148, '2023-08-13 17:25:12.464000', '2023-08-13 17:25:12.464000', 10, '127.0.0.1', 1, '/menu/root', 1),
	(149, '2023-08-13 17:25:12.696000', '2023-08-13 17:25:12.696000', 3, '127.0.0.1', 1, '/user/current', 1),
	(150, '2023-08-13 17:25:12.696000', '2023-08-13 17:25:12.696000', 3, '127.0.0.1', 1, '/user/current', 1),
	(151, '2023-08-13 17:25:13.525000', '2023-08-13 17:25:13.525000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(152, '2023-08-13 17:25:15.102000', '2023-08-13 17:25:15.102000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(153, '2023-08-13 17:25:15.102000', '2023-08-13 17:25:15.102000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(154, '2023-08-13 17:25:15.434000', '2023-08-13 17:25:15.434000', 3, '127.0.0.1', 1, '/adPosition/search', 1),
	(155, '2023-08-13 17:25:15.907000', '2023-08-13 17:25:15.907000', 3, '127.0.0.1', 1, '/ad/search', 1),
	(156, '2023-08-13 17:25:15.910000', '2023-08-13 17:25:15.910000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(157, '2023-08-13 17:25:17.948000', '2023-08-13 17:25:17.948000', 3, '127.0.0.1', 1, '/ad/search', 1),
	(158, '2023-08-13 17:25:17.948000', '2023-08-13 17:25:17.948000', 3, '127.0.0.1', 1, '/adPosition/list', 1),
	(159, '2023-08-13 17:25:18.268000', '2023-08-13 17:25:18.268000', 3, '127.0.0.1', 1, '/adPosition/search', 1),
	(160, '2023-08-13 17:25:21.034000', '2023-08-13 17:25:21.034000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(161, '2023-08-13 17:25:21.943000', '2023-08-13 17:25:21.943000', 4, '127.0.0.1', 1, '/user/current', 1),
	(162, '2023-08-13 17:25:21.943000', '2023-08-13 17:25:21.943000', 4, '127.0.0.1', 1, '/user/current', 1),
	(163, '2023-08-13 17:25:22.414000', '2023-08-13 17:25:22.414000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(164, '2023-08-13 17:25:22.730000', '2023-08-13 17:25:22.730000', 3, '127.0.0.1', 1, '/role/search', 1),
	(165, '2023-08-13 17:25:23.187000', '2023-08-13 17:25:23.187000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(166, '2023-08-13 17:25:23.187000', '2023-08-13 17:25:23.187000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(167, '2023-08-13 17:25:23.517000', '2023-08-13 17:25:23.517000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(168, '2023-08-13 17:25:24.286000', '2023-08-13 17:25:24.286000', 3, '127.0.0.1', 1, '/config/info', 1),
	(169, '2023-08-13 17:25:24.636000', '2023-08-13 17:25:24.636000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(170, '2023-08-13 17:25:24.942000', '2023-08-13 17:25:24.942000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(171, '2023-08-13 17:25:24.942000', '2023-08-13 17:25:24.942000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(172, '2023-08-13 17:25:25.214000', '2023-08-13 17:25:25.214000', 3, '127.0.0.1', 1, '/role/search', 1),
	(173, '2023-08-13 17:25:25.784000', '2023-08-13 17:25:25.784000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(174, '2023-08-13 17:25:26.100000', '2023-08-13 17:25:26.100000', 2, '127.0.0.1', 1, '/user/current', 1),
	(175, '2023-08-13 17:25:26.100000', '2023-08-13 17:25:26.100000', 2, '127.0.0.1', 1, '/user/current', 1),
	(176, '2023-08-13 17:25:26.519000', '2023-08-13 17:25:26.519000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(177, '2023-08-13 17:25:26.765000', '2023-08-13 17:25:26.765000', 3, '127.0.0.1', 1, '/role/search', 1),
	(178, '2023-08-13 17:25:27.050000', '2023-08-13 17:25:27.050000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(179, '2023-08-13 17:25:27.050000', '2023-08-13 17:25:27.050000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(180, '2023-08-13 17:25:27.335000', '2023-08-13 17:25:27.335000', 3, '127.0.0.1', 1, '/dict/list', 1),
	(181, '2023-08-13 17:25:27.549000', '2023-08-13 17:25:27.549000', 2, '127.0.0.1', 1, '/config/info', 1),
	(182, '2023-08-13 17:25:27.897000', '2023-08-13 17:25:27.897000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(183, '2023-08-13 17:25:28.166000', '2023-08-13 17:25:28.166000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(184, '2023-08-13 17:25:28.166000', '2023-08-13 17:25:28.166000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(185, '2023-08-13 17:25:28.509000', '2023-08-13 17:25:28.509000', 3, '127.0.0.1', 1, '/user/current', 1),
	(186, '2023-08-13 17:25:28.509000', '2023-08-13 17:25:28.509000', 3, '127.0.0.1', 1, '/user/current', 1),
	(187, '2023-08-13 17:25:30.878000', '2023-08-13 17:25:30.878000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(188, '2023-08-13 17:38:09.123000', '2023-08-13 17:38:09.123000', 191, '127.0.0.1', 1, '/user/login', 1),
	(189, '2023-08-13 17:38:09.150000', '2023-08-13 17:38:09.150000', 5, '127.0.0.1', 1, '/user/current', 1),
	(190, '2023-08-13 17:38:09.216000', '2023-08-13 17:38:09.216000', 57, '127.0.0.1', 1, '/menu/tree', 1),
	(191, '2023-08-13 17:38:11.204000', '2023-08-13 17:38:11.204000', 4, '127.0.0.1', 1, '/user/current', 1),
	(192, '2023-08-13 17:38:11.204000', '2023-08-13 17:38:11.204000', 4, '127.0.0.1', 1, '/user/current', 1),
	(193, '2023-08-13 17:38:11.644000', '2023-08-13 17:38:11.644000', 15, '127.0.0.1', 1, '/menu/root', 1),
	(194, '2023-08-13 17:38:12.070000', '2023-08-13 17:38:12.070000', 23, '127.0.0.1', 1, '/role/search', 1),
	(195, '2023-08-13 17:38:12.511000', '2023-08-13 17:38:12.511000', 13, '127.0.0.1', 1, '/dict/list', 1),
	(196, '2023-08-13 17:38:12.968000', '2023-08-13 17:38:12.968000', 14, '127.0.0.1', 1, '/config/info', 1),
	(197, '2023-08-13 17:38:13.536000', '2023-08-13 17:38:13.536000', 3, '127.0.0.1', 1, '/dict/list', 1),
	(198, '2023-08-13 17:38:13.885000', '2023-08-13 17:38:13.885000', 14, '127.0.0.1', 1, '/userInfo/search', 1),
	(199, '2023-08-13 17:38:13.889000', '2023-08-13 17:38:13.889000', 19, '127.0.0.1', 1, '/structure/list', 1),
	(200, '2023-08-13 17:38:14.379000', '2023-08-13 17:38:14.379000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(201, '2023-08-13 17:38:14.654000', '2023-08-13 17:38:14.654000', 3, '127.0.0.1', 1, '/user/current', 1),
	(202, '2023-08-13 17:38:14.654000', '2023-08-13 17:38:14.654000', 3, '127.0.0.1', 1, '/user/current', 1),
	(203, '2023-08-13 17:40:52.468000', '2023-08-13 17:40:52.468000', 15, '127.0.0.1', 1, '/menu/root', 1),
	(204, '2023-08-13 17:40:53.493000', '2023-08-13 17:40:53.493000', 3, '127.0.0.1', 1, '/role/search', 1),
	(205, '2023-08-13 17:40:54.042000', '2023-08-13 17:40:54.042000', 3, '127.0.0.1', 1, '/userInfo/search', 1),
	(206, '2023-08-13 17:40:54.043000', '2023-08-13 17:40:54.043000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(207, '2023-08-13 17:40:54.790000', '2023-08-13 17:40:54.790000', 3, '127.0.0.1', 1, '/dict/list', 1),
	(208, '2023-08-13 17:40:55.298000', '2023-08-13 17:40:55.298000', 2, '127.0.0.1', 1, '/config/info', 1),
	(209, '2023-08-13 17:40:55.822000', '2023-08-13 17:40:55.822000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(210, '2023-08-13 17:40:56.281000', '2023-08-13 17:40:56.281000', 2, '127.0.0.1', 1, '/config/info', 1),
	(211, '2023-08-13 17:40:56.790000', '2023-08-13 17:40:56.790000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(212, '2023-08-13 17:40:57.109000', '2023-08-13 17:40:57.109000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(213, '2023-08-13 17:40:57.109000', '2023-08-13 17:40:57.109000', 3, '127.0.0.1', 1, '/userInfo/search', 1),
	(214, '2023-08-13 17:40:57.442000', '2023-08-13 17:40:57.442000', 2, '127.0.0.1', 1, '/role/search', 1),
	(215, '2023-08-13 17:40:57.774000', '2023-08-13 17:40:57.774000', 11, '127.0.0.1', 1, '/menu/root', 1),
	(216, '2023-08-13 17:40:58.108000', '2023-08-13 17:40:58.108000', 3, '127.0.0.1', 1, '/user/current', 1),
	(217, '2023-08-13 17:40:58.108000', '2023-08-13 17:40:58.108000', 3, '127.0.0.1', 1, '/user/current', 1),
	(218, '2023-08-13 17:41:01.436000', '2023-08-13 17:41:01.436000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(219, '2023-08-13 17:41:03.991000', '2023-08-13 17:41:03.991000', 14, '127.0.0.1', 1, '/adPosition/list', 1),
	(220, '2023-08-13 17:41:03.991000', '2023-08-13 17:41:03.991000', 14, '127.0.0.1', 1, '/ad/search', 1),
	(221, '2023-08-13 17:41:04.551000', '2023-08-13 17:41:04.551000', 4, '127.0.0.1', 1, '/adPosition/search', 1),
	(222, '2023-08-13 17:41:05.212000', '2023-08-13 17:41:05.212000', 3, '127.0.0.1', 1, '/ad/search', 1),
	(223, '2023-08-13 17:41:05.212000', '2023-08-13 17:41:05.212000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(224, '2023-08-13 17:41:08.553000', '2023-08-13 17:41:08.553000', 3, '127.0.0.1', 1, '/user/current', 1),
	(225, '2023-08-13 17:41:08.553000', '2023-08-13 17:41:08.553000', 3, '127.0.0.1', 1, '/user/current', 1),
	(226, '2023-08-13 17:41:09.015000', '2023-08-13 17:41:09.015000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(227, '2023-08-13 17:41:09.406000', '2023-08-13 17:41:09.406000', 2, '127.0.0.1', 1, '/role/search', 1),
	(228, '2023-08-13 17:41:09.730000', '2023-08-13 17:41:09.730000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(229, '2023-08-13 17:41:09.730000', '2023-08-13 17:41:09.730000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(230, '2023-08-13 17:41:10.035000', '2023-08-13 17:41:10.035000', 3, '127.0.0.1', 1, '/dict/list', 1),
	(231, '2023-08-13 17:41:10.327000', '2023-08-13 17:41:10.327000', 2, '127.0.0.1', 1, '/config/info', 1),
	(232, '2023-08-13 17:44:22.989000', '2023-08-13 17:44:22.989000', 187, '127.0.0.1', 1, '/user/login', 1),
	(233, '2023-08-13 17:44:23.010000', '2023-08-13 17:44:23.010000', 5, '127.0.0.1', 1, '/user/current', 1),
	(234, '2023-08-13 17:44:23.072000', '2023-08-13 17:44:23.072000', 55, '127.0.0.1', 1, '/menu/tree', 1),
	(235, '2023-08-13 17:44:25.154000', '2023-08-13 17:44:25.154000', 5, '127.0.0.1', 1, '/user/current', 1),
	(236, '2023-08-13 17:44:25.154000', '2023-08-13 17:44:25.154000', 4, '127.0.0.1', 1, '/user/current', 1),
	(237, '2023-08-13 17:44:25.948000', '2023-08-13 17:44:25.948000', 16, '127.0.0.1', 1, '/menu/root', 1),
	(238, '2023-08-13 17:44:38.507000', '2023-08-13 17:44:38.507000', 22, '127.0.0.1', 1, '/structure/list', 1),
	(239, '2023-08-13 17:44:49.996000', '2023-08-13 17:44:49.996000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(240, '2023-08-13 17:45:18.881000', '2023-08-13 17:45:18.881000', 17, '127.0.0.1', 1, '/adPosition/search', 1),
	(241, '2023-08-13 17:45:19.836000', '2023-08-13 17:45:19.836000', 4, '127.0.0.1', 1, '/adPosition/list', 1),
	(242, '2023-08-13 17:45:19.844000', '2023-08-13 17:45:19.844000', 12, '127.0.0.1', 1, '/ad/search', 1),
	(243, '2023-08-13 17:45:20.899000', '2023-08-13 17:45:20.899000', 2, '127.0.0.1', 1, '/adPosition/search', 1),
	(244, '2023-08-13 17:45:22.527000', '2023-08-13 17:45:22.527000', 3, '127.0.0.1', 1, '/user/current', 1),
	(245, '2023-08-13 17:45:22.527000', '2023-08-13 17:45:22.527000', 3, '127.0.0.1', 1, '/user/current', 1),
	(246, '2023-08-13 17:45:23.034000', '2023-08-13 17:45:23.034000', 11, '127.0.0.1', 1, '/menu/root', 1),
	(247, '2023-08-13 17:45:23.528000', '2023-08-13 17:45:23.528000', 23, '127.0.0.1', 1, '/role/search', 1),
	(248, '2023-08-13 17:45:24.086000', '2023-08-13 17:45:24.086000', 13, '127.0.0.1', 1, '/structure/list', 1),
	(249, '2023-08-13 17:45:24.092000', '2023-08-13 17:45:24.092000', 19, '127.0.0.1', 1, '/userInfo/search', 1),
	(250, '2023-08-13 17:45:24.511000', '2023-08-13 17:45:24.511000', 13, '127.0.0.1', 1, '/dict/list', 1),
	(251, '2023-08-13 17:45:24.960000', '2023-08-13 17:45:24.960000', 13, '127.0.0.1', 1, '/config/info', 1),
	(252, '2023-08-13 17:45:25.319000', '2023-08-13 17:45:25.319000', 3, '127.0.0.1', 1, '/dict/list', 1),
	(253, '2023-08-13 17:45:25.639000', '2023-08-13 17:45:25.639000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(254, '2023-08-13 17:45:25.640000', '2023-08-13 17:45:25.640000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(255, '2023-08-13 17:45:25.988000', '2023-08-13 17:45:25.988000', 3, '127.0.0.1', 1, '/role/search', 1),
	(256, '2023-08-13 17:45:26.319000', '2023-08-13 17:45:26.319000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(257, '2023-08-13 17:45:26.640000', '2023-08-13 17:45:26.640000', 3, '127.0.0.1', 1, '/user/current', 1),
	(258, '2023-08-13 17:45:26.640000', '2023-08-13 17:45:26.640000', 3, '127.0.0.1', 1, '/user/current', 1),
	(259, '2023-08-13 17:45:26.992000', '2023-08-13 17:45:26.992000', 11, '127.0.0.1', 1, '/menu/root', 1),
	(260, '2023-08-13 17:45:27.287000', '2023-08-13 17:45:27.287000', 3, '127.0.0.1', 1, '/role/search', 1),
	(261, '2023-08-13 17:45:27.582000', '2023-08-13 17:45:27.582000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(262, '2023-08-13 17:45:27.583000', '2023-08-13 17:45:27.583000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(263, '2023-08-13 17:45:27.923000', '2023-08-13 17:45:27.923000', 3, '127.0.0.1', 1, '/dict/list', 1),
	(264, '2023-08-13 17:45:28.249000', '2023-08-13 17:45:28.249000', 3, '127.0.0.1', 1, '/config/info', 1),
	(265, '2023-08-13 17:45:28.627000', '2023-08-13 17:45:28.627000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(266, '2023-08-13 17:45:28.885000', '2023-08-13 17:45:28.885000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(267, '2023-08-13 17:45:28.886000', '2023-08-13 17:45:28.886000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(268, '2023-08-13 17:45:29.163000', '2023-08-13 17:45:29.163000', 2, '127.0.0.1', 1, '/role/search', 1),
	(269, '2023-08-13 17:45:29.482000', '2023-08-13 17:45:29.482000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(270, '2023-08-13 17:45:29.741000', '2023-08-13 17:45:29.741000', 2, '127.0.0.1', 1, '/user/current', 1),
	(271, '2023-08-13 17:45:29.741000', '2023-08-13 17:45:29.741000', 2, '127.0.0.1', 1, '/user/current', 1),
	(272, '2023-08-13 17:45:30.696000', '2023-08-13 17:45:30.696000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(273, '2023-08-13 17:45:31.915000', '2023-08-13 17:45:31.915000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(274, '2023-08-13 17:45:31.915000', '2023-08-13 17:45:31.915000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(275, '2023-08-13 17:45:32.387000', '2023-08-13 17:45:32.387000', 3, '127.0.0.1', 1, '/adPosition/search', 1),
	(276, '2023-08-13 17:45:32.981000', '2023-08-13 17:45:32.981000', 3, '127.0.0.1', 1, '/adPosition/list', 1),
	(277, '2023-08-13 17:45:32.981000', '2023-08-13 17:45:32.981000', 3, '127.0.0.1', 1, '/ad/search', 1),
	(278, '2023-08-13 18:27:33.034000', '2023-08-13 18:27:33.034000', 188, '127.0.0.1', 1, '/user/login', 1),
	(279, '2023-08-13 18:27:33.058000', '2023-08-13 18:27:33.058000', 6, '127.0.0.1', 1, '/user/current', 1),
	(280, '2023-08-13 18:27:33.134000', '2023-08-13 18:27:33.134000', 55, '127.0.0.1', 1, '/menu/tree', 1),
	(281, '2023-08-13 18:27:36.689000', '2023-08-13 18:27:36.689000', 24, '127.0.0.1', 1, '/structure/list', 1),
	(282, '2023-08-13 18:27:42.081000', '2023-08-13 18:27:42.081000', 36, '127.0.0.1', 1, '/structure/delete', 1),
	(283, '2023-08-13 18:27:42.105000', '2023-08-13 18:27:42.105000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(284, '2023-08-13 18:27:44.796000', '2023-08-13 18:27:44.796000', 28, '127.0.0.1', 1, '/structure/delete', 1),
	(285, '2023-08-13 18:27:44.810000', '2023-08-13 18:27:44.810000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(286, '2023-08-13 18:27:59.591000', '2023-08-13 18:27:59.591000', 39, '127.0.0.1', 1, '/structure/create', 1),
	(287, '2023-08-13 18:27:59.612000', '2023-08-13 18:27:59.612000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(288, '2023-08-13 18:28:35.562000', '2023-08-13 18:28:35.562000', 27, '127.0.0.1', 1, '/structure/delete', 1),
	(289, '2023-08-13 18:28:35.575000', '2023-08-13 18:28:35.575000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(290, '2023-08-13 18:28:41.344000', '2023-08-13 18:28:41.344000', 37, '127.0.0.1', 1, '/structure/create', 1),
	(291, '2023-08-13 18:28:41.370000', '2023-08-13 18:28:41.370000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(292, '2023-08-13 18:28:56.861000', '2023-08-13 18:28:56.861000', 14, '127.0.0.1', 1, '/structure/create', 1),
	(293, '2023-08-13 18:28:56.886000', '2023-08-13 18:28:56.886000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(294, '2023-08-13 18:29:00.293000', '2023-08-13 18:29:00.293000', 10, '127.0.0.1', 1, '/structure/create', 1),
	(295, '2023-08-13 18:29:00.323000', '2023-08-13 18:29:00.323000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(296, '2023-08-13 18:29:39.377000', '2023-08-13 18:29:39.377000', 98, '127.0.0.1', 1, '/structure/create', 1),
	(297, '2023-08-13 18:29:39.400000', '2023-08-13 18:29:39.400000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(298, '2023-08-13 18:29:42.443000', '2023-08-13 18:29:42.443000', 7, '127.0.0.1', 1, '/structure/create', 1),
	(299, '2023-08-13 18:29:42.472000', '2023-08-13 18:29:42.472000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(300, '2023-08-13 18:29:46.338000', '2023-08-13 18:29:46.338000', 11, '127.0.0.1', 1, '/structure/create', 1),
	(301, '2023-08-13 18:29:46.362000', '2023-08-13 18:29:46.362000', 7, '127.0.0.1', 1, '/structure/list', 1),
	(302, '2023-08-13 18:30:05.352000', '2023-08-13 18:30:05.352000', 12, '127.0.0.1', 1, '/structure/delete', 1),
	(303, '2023-08-13 18:30:05.382000', '2023-08-13 18:30:05.382000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(304, '2023-08-13 18:30:06.653000', '2023-08-13 18:30:06.653000', 8, '127.0.0.1', 1, '/structure/delete', 1),
	(305, '2023-08-13 18:30:06.682000', '2023-08-13 18:30:06.682000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(306, '2023-08-13 18:30:08.410000', '2023-08-13 18:30:08.410000', 7, '127.0.0.1', 1, '/structure/delete', 1),
	(307, '2023-08-13 18:30:08.435000', '2023-08-13 18:30:08.435000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(308, '2023-08-13 20:55:23.289000', '2023-08-13 20:55:23.289000', 195, '127.0.0.1', 1, '/user/login', 1),
	(309, '2023-08-13 20:55:23.403000', '2023-08-13 20:55:23.403000', 5, '127.0.0.1', 1, '/user/current', 1),
	(310, '2023-08-13 20:55:23.475000', '2023-08-13 20:55:23.475000', 54, '127.0.0.1', 1, '/menu/tree', 1),
	(311, '2023-08-13 20:55:26.289000', '2023-08-13 20:55:26.289000', 16, '127.0.0.1', 1, '/adPosition/list', 1),
	(312, '2023-08-13 20:55:26.293000', '2023-08-13 20:55:26.293000', 19, '127.0.0.1', 1, '/ad/search', 1),
	(313, '2023-08-13 20:55:27.194000', '2023-08-13 20:55:27.194000', 5, '127.0.0.1', 1, '/adPosition/search', 1),
	(314, '2023-08-13 20:55:30.049000', '2023-08-13 20:55:30.049000', 32, '127.0.0.1', 1, '/adPosition/create', 1),
	(315, '2023-08-13 20:55:30.104000', '2023-08-13 20:55:30.104000', 5, '127.0.0.1', 1, '/adPosition/search', 1),
	(316, '2023-08-13 20:55:31.721000', '2023-08-13 20:55:31.721000', 3, '127.0.0.1', 1, '/adPosition/list', 1),
	(317, '2023-08-13 20:55:31.721000', '2023-08-13 20:55:31.721000', 3, '127.0.0.1', 1, '/ad/search', 1),
	(318, '2023-08-13 20:55:32.146000', '2023-08-13 20:55:32.146000', 4, '127.0.0.1', 1, '/adPosition/search', 1),
	(319, '2023-08-13 20:55:32.670000', '2023-08-13 20:55:32.670000', 3, '127.0.0.1', 1, '/adPosition/list', 1),
	(320, '2023-08-13 20:55:32.669000', '2023-08-13 20:55:32.669000', 3, '127.0.0.1', 1, '/ad/search', 1),
	(321, '2023-08-13 20:55:33.036000', '2023-08-13 20:55:33.036000', 3, '127.0.0.1', 1, '/adPosition/search', 1),
	(322, '2023-08-13 20:56:43.581000', '2023-08-13 20:56:43.581000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(323, '2023-08-13 20:56:43.581000', '2023-08-13 20:56:43.581000', 3, '127.0.0.1', 1, '/ad/search', 1),
	(324, '2023-08-13 20:56:44.002000', '2023-08-13 20:56:44.002000', 3, '127.0.0.1', 1, '/adPosition/search', 1),
	(325, '2023-08-13 20:56:45.220000', '2023-08-13 20:56:45.220000', 20, '127.0.0.1', 1, '/structure/list', 1),
	(326, '2023-08-13 20:56:46.413000', '2023-08-13 20:56:46.413000', 2, '127.0.0.1', 1, '/user/current', 1),
	(327, '2023-08-13 20:56:46.413000', '2023-08-13 20:56:46.413000', 2, '127.0.0.1', 1, '/user/current', 1),
	(328, '2023-08-13 20:56:46.708000', '2023-08-13 20:56:46.708000', 12, '127.0.0.1', 1, '/menu/root', 1),
	(329, '2023-08-13 20:56:47.101000', '2023-08-13 20:56:47.101000', 13, '127.0.0.1', 1, '/role/search', 1),
	(330, '2023-08-13 20:56:47.575000', '2023-08-13 20:56:47.575000', 7, '127.0.0.1', 1, '/structure/list', 1),
	(331, '2023-08-13 20:56:47.587000', '2023-08-13 20:56:47.587000', 16, '127.0.0.1', 1, '/userInfo/search', 1),
	(332, '2023-08-13 20:56:47.943000', '2023-08-13 20:56:47.943000', 13, '127.0.0.1', 1, '/dict/list', 1),
	(333, '2023-08-13 20:56:48.302000', '2023-08-13 20:56:48.302000', 13, '127.0.0.1', 1, '/config/info', 1),
	(334, '2023-08-13 20:56:48.779000', '2023-08-13 20:56:48.779000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(335, '2023-08-13 20:56:49.197000', '2023-08-13 20:56:49.197000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(336, '2023-08-13 20:56:49.198000', '2023-08-13 20:56:49.198000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(337, '2023-08-13 20:56:49.652000', '2023-08-13 20:56:49.652000', 3, '127.0.0.1', 1, '/role/search', 1),
	(338, '2023-08-13 20:56:49.973000', '2023-08-13 20:56:49.973000', 10, '127.0.0.1', 1, '/menu/root', 1),
	(339, '2023-08-13 20:56:50.507000', '2023-08-13 20:56:50.507000', 3, '127.0.0.1', 1, '/user/current', 1),
	(340, '2023-08-13 20:56:50.507000', '2023-08-13 20:56:50.507000', 3, '127.0.0.1', 1, '/user/current', 1),
	(341, '2023-08-13 20:56:51.751000', '2023-08-13 20:56:51.751000', 7, '127.0.0.1', 1, '/structure/list', 1),
	(342, '2023-08-13 20:56:53.100000', '2023-08-13 20:56:53.100000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(343, '2023-08-13 20:56:53.100000', '2023-08-13 20:56:53.100000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(344, '2023-08-13 20:56:53.517000', '2023-08-13 20:56:53.517000', 2, '127.0.0.1', 1, '/adPosition/search', 1),
	(345, '2023-08-13 20:56:53.857000', '2023-08-13 20:56:53.857000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(346, '2023-08-13 20:56:53.858000', '2023-08-13 20:56:53.858000', 3, '127.0.0.1', 1, '/adPosition/list', 1),
	(347, '2023-08-13 20:56:54.193000', '2023-08-13 20:56:54.193000', 3, '127.0.0.1', 1, '/adPosition/search', 1),
	(348, '2023-08-13 20:56:54.455000', '2023-08-13 20:56:54.455000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(349, '2023-08-13 20:56:54.455000', '2023-08-13 20:56:54.455000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(350, '2023-08-13 20:56:57.224000', '2023-08-13 20:56:57.224000', 3, '127.0.0.1', 1, '/user/current', 1),
	(351, '2023-08-13 20:56:57.224000', '2023-08-13 20:56:57.224000', 3, '127.0.0.1', 1, '/user/current', 1),
	(352, '2023-08-13 20:56:57.555000', '2023-08-13 20:56:57.555000', 2, '127.0.0.1', 1, '/role/search', 1),
	(353, '2023-08-13 20:56:57.845000', '2023-08-13 20:56:57.845000', 3, '127.0.0.1', 1, '/userInfo/search', 1),
	(354, '2023-08-13 20:56:57.846000', '2023-08-13 20:56:57.846000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(355, '2023-08-13 20:56:58.090000', '2023-08-13 20:56:58.090000', 2, '127.0.0.1', 1, '/config/info', 1),
	(356, '2023-08-13 20:56:58.577000', '2023-08-13 20:56:58.577000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(357, '2023-08-13 20:56:58.846000', '2023-08-13 20:56:58.846000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(358, '2023-08-13 20:56:58.847000', '2023-08-13 20:56:58.847000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(359, '2023-08-13 20:56:59.114000', '2023-08-13 20:56:59.114000', 2, '127.0.0.1', 1, '/role/search', 1),
	(360, '2023-08-13 20:56:59.393000', '2023-08-13 20:56:59.393000', 8, '127.0.0.1', 1, '/menu/root', 1),
	(361, '2023-08-13 20:56:59.652000', '2023-08-13 20:56:59.652000', 3, '127.0.0.1', 1, '/user/current', 1),
	(362, '2023-08-13 20:56:59.652000', '2023-08-13 20:56:59.652000', 3, '127.0.0.1', 1, '/user/current', 1),
	(363, '2023-08-13 20:57:00.717000', '2023-08-13 20:57:00.717000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(364, '2023-08-13 20:57:05.815000', '2023-08-13 20:57:05.815000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(365, '2023-08-13 20:57:05.815000', '2023-08-13 20:57:05.815000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(366, '2023-08-13 20:57:06.183000', '2023-08-13 20:57:06.183000', 3, '127.0.0.1', 1, '/adPosition/search', 1),
	(367, '2023-08-13 20:57:06.524000', '2023-08-13 20:57:06.524000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(368, '2023-08-13 20:57:06.524000', '2023-08-13 20:57:06.524000', 3, '127.0.0.1', 1, '/adPosition/list', 1),
	(369, '2023-08-13 20:57:06.988000', '2023-08-13 20:57:06.988000', 3, '127.0.0.1', 1, '/adPosition/search', 1),
	(370, '2023-08-13 20:57:07.320000', '2023-08-13 20:57:07.320000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(371, '2023-08-13 20:57:07.320000', '2023-08-13 20:57:07.320000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(372, '2023-08-13 20:57:07.627000', '2023-08-13 20:57:07.627000', 3, '127.0.0.1', 1, '/adPosition/search', 1),
	(373, '2023-08-13 20:57:08.121000', '2023-08-13 20:57:08.121000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(374, '2023-08-13 20:57:08.121000', '2023-08-13 20:57:08.121000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(375, '2023-08-13 20:57:09.386000', '2023-08-13 20:57:09.386000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(376, '2023-08-13 20:57:10.388000', '2023-08-13 20:57:10.388000', 3, '127.0.0.1', 1, '/user/current', 1),
	(377, '2023-08-13 20:57:10.388000', '2023-08-13 20:57:10.388000', 3, '127.0.0.1', 1, '/user/current', 1),
	(378, '2023-08-13 20:57:10.730000', '2023-08-13 20:57:10.730000', 8, '127.0.0.1', 1, '/menu/root', 1),
	(379, '2023-08-13 20:57:11.087000', '2023-08-13 20:57:11.087000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(380, '2023-08-13 20:57:11.088000', '2023-08-13 20:57:11.088000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(381, '2023-08-13 20:57:11.392000', '2023-08-13 20:57:11.392000', 2, '127.0.0.1', 1, '/config/info', 1),
	(382, '2023-08-13 20:57:11.891000', '2023-08-13 20:57:11.891000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(383, '2023-08-13 20:57:12.232000', '2023-08-13 20:57:12.232000', 4, '127.0.0.1', 1, '/userInfo/search', 1),
	(384, '2023-08-13 20:57:12.233000', '2023-08-13 20:57:12.233000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(385, '2023-08-13 20:57:12.481000', '2023-08-13 20:57:12.481000', 2, '127.0.0.1', 1, '/role/search', 1),
	(386, '2023-08-13 20:57:12.738000', '2023-08-13 20:57:12.738000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(387, '2023-08-13 20:57:13.055000', '2023-08-13 20:57:13.055000', 2, '127.0.0.1', 1, '/user/current', 1),
	(388, '2023-08-13 20:57:13.055000', '2023-08-13 20:57:13.055000', 2, '127.0.0.1', 1, '/user/current', 1),
	(389, '2023-08-13 20:57:14.464000', '2023-08-13 20:57:14.464000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(390, '2023-08-13 20:57:15.367000', '2023-08-13 20:57:15.367000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(391, '2023-08-13 20:57:15.368000', '2023-08-13 20:57:15.368000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(392, '2023-08-13 20:57:15.684000', '2023-08-13 20:57:15.684000', 4, '127.0.0.1', 1, '/adPosition/search', 1),
	(393, '2023-08-13 20:57:16.089000', '2023-08-13 20:57:16.089000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(394, '2023-08-13 20:57:16.089000', '2023-08-13 20:57:16.089000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(395, '2023-08-13 20:57:17.787000', '2023-08-13 20:57:17.787000', 3, '127.0.0.1', 1, '/adPosition/search', 1),
	(396, '2023-08-13 20:57:18.250000', '2023-08-13 20:57:18.250000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(397, '2023-08-13 20:57:18.250000', '2023-08-13 20:57:18.250000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(398, '2023-08-13 20:57:20.012000', '2023-08-13 20:57:20.012000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(399, '2023-08-13 20:57:21.236000', '2023-08-13 20:57:21.236000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(400, '2023-08-13 20:57:21.236000', '2023-08-13 20:57:21.236000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(401, '2023-08-13 20:57:21.524000', '2023-08-13 20:57:21.524000', 2, '127.0.0.1', 1, '/adPosition/search', 1),
	(402, '2023-08-13 20:57:21.780000', '2023-08-13 20:57:21.780000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(403, '2023-08-13 20:57:21.780000', '2023-08-13 20:57:21.780000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(404, '2023-08-13 20:57:22.505000', '2023-08-13 20:57:22.505000', 2, '127.0.0.1', 1, '/adPosition/search', 1),
	(405, '2023-08-13 20:57:22.784000', '2023-08-13 20:57:22.784000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(406, '2023-08-13 20:57:22.784000', '2023-08-13 20:57:22.784000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(407, '2023-08-17 21:51:46.469000', '2023-08-17 21:51:46.469000', 221, '127.0.0.1', 1, '/user/login', 1),
	(408, '2023-08-17 21:51:46.504000', '2023-08-17 21:51:46.504000', 7, '127.0.0.1', 1, '/user/current', 1),
	(409, '2023-08-17 21:51:46.591000', '2023-08-17 21:51:46.591000', 66, '127.0.0.1', 1, '/menu/tree', 1),
	(410, '2023-08-17 21:51:49.015000', '2023-08-17 21:51:49.015000', 24, '127.0.0.1', 1, '/structure/list', 1),
	(411, '2023-08-17 21:51:50.500000', '2023-08-17 21:51:50.500000', 3, '127.0.0.1', 1, '/user/current', 1),
	(412, '2023-08-17 21:51:50.500000', '2023-08-17 21:51:50.500000', 3, '127.0.0.1', 1, '/user/current', 1),
	(413, '2023-08-17 21:51:51.125000', '2023-08-17 21:51:51.125000', 13, '127.0.0.1', 1, '/menu/root', 1),
	(414, '2023-08-17 21:52:14.409000', '2023-08-17 21:52:14.409000', 85, '127.0.0.1', 1, '/menu/update', 1),
	(415, '2023-08-17 21:52:14.438000', '2023-08-17 21:52:14.438000', 11, '127.0.0.1', 1, '/menu/root', 1),
	(416, '2023-08-17 21:53:04.816000', '2023-08-17 21:53:04.816000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(417, '2023-08-17 21:53:10.931000', '2023-08-17 21:53:10.931000', 18, '127.0.0.1', 1, '/adPosition/list', 1),
	(418, '2023-08-17 21:53:10.932000', '2023-08-17 21:53:10.932000', 18, '127.0.0.1', 1, '/ad/search', 1),
	(419, '2023-08-17 21:53:11.244000', '2023-08-17 21:53:11.244000', 4, '127.0.0.1', 1, '/adPosition/search', 1),
	(420, '2023-08-17 21:53:13.222000', '2023-08-17 21:53:13.222000', 4, '127.0.0.1', 1, '/user/current', 1),
	(421, '2023-08-17 21:53:13.222000', '2023-08-17 21:53:13.222000', 4, '127.0.0.1', 1, '/user/current', 1),
	(422, '2023-08-17 21:53:13.577000', '2023-08-17 21:53:13.577000', 11, '127.0.0.1', 1, '/menu/root', 1),
	(423, '2023-08-17 21:53:14.023000', '2023-08-17 21:53:14.023000', 13, '127.0.0.1', 1, '/role/search', 1),
	(424, '2023-08-17 21:53:14.719000', '2023-08-17 21:53:14.719000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(425, '2023-08-17 21:53:15.139000', '2023-08-17 21:53:15.139000', 2, '127.0.0.1', 1, '/user/current', 1),
	(426, '2023-08-17 21:53:15.139000', '2023-08-17 21:53:15.139000', 2, '127.0.0.1', 1, '/user/current', 1),
	(427, '2023-08-17 21:53:16.871000', '2023-08-17 21:53:16.871000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(428, '2023-08-17 23:02:59.126000', '2023-08-17 23:02:59.126000', 215, '127.0.0.1', 1, '/user/login', 1),
	(429, '2023-08-17 23:02:59.234000', '2023-08-17 23:02:59.234000', 7, '127.0.0.1', 1, '/user/current', 1),
	(430, '2023-08-17 23:02:59.311000', '2023-08-17 23:02:59.311000', 55, '127.0.0.1', 1, '/menu/tree', 1),
	(431, '2023-08-17 23:03:01.764000', '2023-08-17 23:03:01.764000', 17, '127.0.0.1', 1, '/role/search', 1),
	(432, '2023-08-17 23:03:02.396000', '2023-08-17 23:03:02.396000', 17, '127.0.0.1', 1, '/menu/root', 1),
	(433, '2023-08-17 23:03:04.673000', '2023-08-17 23:03:04.673000', 48, '127.0.0.1', 1, '/menu/create', 1),
	(434, '2023-08-17 23:03:13.436000', '2023-08-17 23:03:13.436000', 13, '127.0.0.1', 1, '/menu/update', 1),
	(435, '2023-08-17 23:03:13.468000', '2023-08-17 23:03:13.468000', 11, '127.0.0.1', 1, '/menu/root', 1),
	(436, '2023-08-17 23:03:31.532000', '2023-08-17 23:03:31.532000', 101, '127.0.0.1', 1, '/menu/update', 1),
	(437, '2023-08-17 23:03:31.562000', '2023-08-17 23:03:31.562000', 11, '127.0.0.1', 1, '/menu/root', 1),
	(438, '2023-08-17 23:03:34.196000', '2023-08-17 23:03:34.196000', 3, '127.0.0.1', 1, '/role/search', 1),
	(439, '2023-08-17 23:03:36.695000', '2023-08-17 23:03:36.695000', 11, '127.0.0.1', 1, '/menu/root', 1),
	(440, '2023-08-17 23:03:36.736000', '2023-08-17 23:03:36.736000', 10, '127.0.0.1', 1, '/menu/selectForPermission', 1),
	(441, '2023-08-17 23:03:39.920000', '2023-08-17 23:03:39.920000', 54, '127.0.0.1', 1, '/menu/updateRoleMenus', 1),
	(442, '2023-08-17 23:03:45.638000', '2023-08-17 23:03:45.638000', 6, '127.0.0.1', 1, '/user/login', 1),
	(443, '2023-08-17 23:03:45.652000', '2023-08-17 23:03:45.652000', 3, '127.0.0.1', 1, '/user/current', 1),
	(444, '2023-08-17 23:03:45.667000', '2023-08-17 23:03:45.667000', 4, '127.0.0.1', 1, '/menu/tree', 1),
	(445, '2023-08-17 23:03:49.426000', '2023-08-17 23:03:49.426000', 3, '127.0.0.1', 1, '/user/current', 1),
	(446, '2023-08-17 23:03:49.426000', '2023-08-17 23:03:49.426000', 3, '127.0.0.1', 1, '/user/current', 1),
	(447, '2023-08-17 23:03:49.623000', '2023-08-17 23:03:49.623000', 53, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(448, '2023-08-17 23:07:27.025000', '2023-08-17 23:07:27.025000', 18, '127.0.0.1', 1, '/ad/search', 1),
	(449, '2023-08-17 23:07:27.030000', '2023-08-17 23:07:27.030000', 22, '127.0.0.1', 1, '/adPosition/list', 1),
	(450, '2023-08-17 23:07:28.019000', '2023-08-17 23:07:28.019000', 5, '127.0.0.1', 1, '/adPosition/search', 1),
	(451, '2023-08-17 23:07:28.804000', '2023-08-17 23:07:28.804000', 3, '127.0.0.1', 1, '/ad/search', 1),
	(452, '2023-08-17 23:07:28.804000', '2023-08-17 23:07:28.804000', 3, '127.0.0.1', 1, '/adPosition/list', 1),
	(453, '2023-08-17 23:07:30.896000', '2023-08-17 23:07:30.896000', 3, '127.0.0.1', 1, '/adPosition/search', 1),
	(454, '2023-08-17 23:07:31.829000', '2023-08-17 23:07:31.829000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(455, '2023-08-17 23:07:31.829000', '2023-08-17 23:07:31.829000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(456, '2023-08-17 23:12:39.824000', '2023-08-17 23:12:39.824000', 27, '0:0:0:0:0:0:0:1', 1, '/user/login', 1),
	(457, '2023-08-17 23:12:39.899000', '2023-08-17 23:12:39.899000', 4, '0:0:0:0:0:0:0:1', 1, '/user/current', 1),
	(458, '2023-08-17 23:12:39.923000', '2023-08-17 23:12:39.923000', 9, '0:0:0:0:0:0:0:1', 1, '/menu/tree', 1),
	(459, '2023-08-17 23:12:44.248000', '2023-08-17 23:12:44.248000', 42, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/search', 1),
	(460, '2023-08-17 23:16:07.090000', '2023-08-17 23:16:07.090000', 31, '127.0.0.1', 1, '/user/login', 1),
	(461, '2023-08-17 23:16:07.177000', '2023-08-17 23:16:07.177000', 3, '127.0.0.1', 1, '/user/current', 1),
	(462, '2023-08-17 23:16:07.198000', '2023-08-17 23:16:07.198000', 9, '127.0.0.1', 1, '/menu/tree', 1),
	(463, '2023-08-17 23:16:10.538000', '2023-08-17 23:16:10.538000', 7, '127.0.0.1', 1, '/role/search', 1),
	(464, '2023-08-17 23:16:11.278000', '2023-08-17 23:16:11.278000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(465, '2023-08-17 23:16:11.672000', '2023-08-17 23:16:11.672000', 40, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(466, '2023-08-17 23:16:12.925000', '2023-08-17 23:16:12.925000', 12, '127.0.0.1', 1, '/menu/root', 1),
	(467, '2023-08-17 23:16:14.175000', '2023-08-17 23:16:14.175000', 16, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(468, '2023-08-17 23:16:23.904000', '2023-08-17 23:16:23.904000', 60, '127.0.0.1', 1, '/schedulingTask/create', 1),
	(469, '2023-08-17 23:16:23.975000', '2023-08-17 23:16:23.975000', 16, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(470, '2023-08-17 23:18:04.474000', '2023-08-17 23:18:04.474000', 26, '127.0.0.1', 1, '/user/login', 1),
	(471, '2023-08-17 23:18:04.540000', '2023-08-17 23:18:04.540000', 3, '127.0.0.1', 1, '/user/current', 1),
	(472, '2023-08-17 23:18:04.566000', '2023-08-17 23:18:04.566000', 8, '127.0.0.1', 1, '/menu/tree', 1),
	(473, '2023-08-17 23:18:09.864000', '2023-08-17 23:18:09.864000', 12, '127.0.0.1', 1, '/userInfo/search', 1),
	(474, '2023-08-17 23:18:09.869000', '2023-08-17 23:18:09.869000', 18, '127.0.0.1', 1, '/structure/list', 1),
	(475, '2023-08-17 23:18:10.525000', '2023-08-17 23:18:10.525000', 34, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(476, '2023-08-17 23:18:12.320000', '2023-08-17 23:18:12.320000', 27, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(477, '2023-08-17 23:18:13.385000', '2023-08-17 23:18:13.385000', 21, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(478, '2023-08-17 23:18:14.756000', '2023-08-17 23:18:14.756000', 18, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(479, '2023-08-17 23:19:05.363000', '2023-08-17 23:19:05.363000', 21, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(480, '2023-08-17 23:21:31.516000', '2023-08-17 23:21:31.516000', 11, '127.0.0.1', 1, '/ad/search', 1),
	(481, '2023-08-17 23:21:31.516000', '2023-08-17 23:21:31.516000', 11, '127.0.0.1', 1, '/adPosition/list', 1),
	(482, '2023-08-17 23:21:34.700000', '2023-08-17 23:21:34.700000', 6, '127.0.0.1', 1, '/role/search', 1),
	(483, '2023-08-17 23:21:35.490000', '2023-08-17 23:21:35.490000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(484, '2023-08-17 23:21:35.736000', '2023-08-17 23:21:35.736000', 16, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(485, '2023-08-17 23:21:43.880000', '2023-08-17 23:21:43.880000', 19, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(486, '2023-08-17 23:21:46.181000', '2023-08-17 23:21:46.181000', 2, '127.0.0.1', 1, '/adPosition/list', 1),
	(487, '2023-08-17 23:21:46.181000', '2023-08-17 23:21:46.181000', 2, '127.0.0.1', 1, '/ad/search', 1),
	(488, '2023-08-17 23:21:49.088000', '2023-08-17 23:21:49.088000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(489, '2023-08-17 23:21:49.720000', '2023-08-17 23:21:49.720000', 17, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(490, '2023-08-17 23:21:50.444000', '2023-08-17 23:21:50.444000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(491, '2023-08-17 23:22:53.979000', '2023-08-17 23:22:53.979000', 15, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(492, '2023-08-17 23:27:20.227000', '2023-08-17 23:27:20.227000', 31, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(493, '2023-08-17 23:27:20.484000', '2023-08-17 23:27:20.484000', 16, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(494, '2023-08-17 23:27:22.197000', '2023-08-17 23:27:22.197000', 15, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(495, '2023-08-17 23:27:24.366000', '2023-08-17 23:27:24.366000', 19, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(496, '2023-08-17 23:35:40.973000', '2023-08-17 23:35:40.973000', 116, '127.0.0.1', 1, '/user/login', 1),
	(497, '2023-08-17 23:35:41.096000', '2023-08-17 23:35:41.096000', 8, '127.0.0.1', 1, '/user/current', 1),
	(498, '2023-08-17 23:35:41.176000', '2023-08-17 23:35:41.176000', 60, '127.0.0.1', 1, '/menu/tree', 1),
	(499, '2023-08-17 23:35:43.038000', '2023-08-17 23:35:43.038000', 66, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(500, '2023-08-17 23:35:43.610000', '2023-08-17 23:35:43.610000', 16, '127.0.0.1', 1, '/menu/root', 1),
	(501, '2023-08-17 23:35:44.924000', '2023-08-17 23:35:44.924000', 29, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(502, '2023-08-17 23:35:46.388000', '2023-08-17 23:35:46.388000', 37, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(503, '2023-08-17 23:36:07.581000', '2023-08-17 23:36:07.581000', 98, '127.0.0.1', 1, '/schedulingTask/update', 1),
	(504, '2023-08-17 23:36:07.658000', '2023-08-17 23:36:07.658000', 23, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(505, '2023-08-17 23:36:09.524000', '2023-08-17 23:36:09.524000', 23, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(506, '2023-08-17 23:36:26.524000', '2023-08-17 23:36:26.524000', 60, '127.0.0.1', 1, '/schedulingTask/update', 1),
	(507, '2023-08-17 23:36:26.596000', '2023-08-17 23:36:26.596000', 21, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(508, '2023-08-17 23:37:04.538000', '2023-08-17 23:37:04.538000', 22, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(509, '2023-08-17 23:37:08.952000', '2023-08-17 23:37:08.952000', 22, '127.0.0.1', 1, '/schedulingTask/update', 1),
	(510, '2023-08-17 23:37:09.028000', '2023-08-17 23:37:09.028000', 20, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(511, '2023-08-17 23:37:28.983000', '2023-08-17 23:37:28.983000', 19, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(512, '2023-08-17 23:39:21.009000', '2023-08-17 23:39:21.009000', 94, '127.0.0.1', 1, '/user/login', 1),
	(513, '2023-08-17 23:39:21.035000', '2023-08-17 23:39:21.035000', 7, '127.0.0.1', 1, '/user/current', 1),
	(514, '2023-08-17 23:39:21.111000', '2023-08-17 23:39:21.111000', 56, '127.0.0.1', 1, '/menu/tree', 1),
	(515, '2023-08-17 23:39:23.395000', '2023-08-17 23:39:23.395000', 15, '127.0.0.1', 1, '/menu/root', 1),
	(516, '2023-08-17 23:39:24.014000', '2023-08-17 23:39:24.014000', 15, '127.0.0.1', 1, '/role/search', 1),
	(517, '2023-08-17 23:39:25.384000', '2023-08-17 23:39:25.384000', 10, '127.0.0.1', 1, '/menu/root', 1),
	(518, '2023-08-17 23:39:26.215000', '2023-08-17 23:39:26.215000', 2, '127.0.0.1', 1, '/role/search', 1),
	(519, '2023-08-17 23:39:26.664000', '2023-08-17 23:39:26.664000', 12, '127.0.0.1', 1, '/userInfo/search', 1),
	(520, '2023-08-17 23:39:26.670000', '2023-08-17 23:39:26.670000', 18, '127.0.0.1', 1, '/structure/list', 1),
	(521, '2023-08-17 23:39:27.123000', '2023-08-17 23:39:27.123000', 13, '127.0.0.1', 1, '/dict/list', 1),
	(522, '2023-08-17 23:39:27.411000', '2023-08-17 23:39:27.411000', 5, '127.0.0.1', 1, '/userInfo/search', 1),
	(523, '2023-08-17 23:39:27.411000', '2023-08-17 23:39:27.411000', 5, '127.0.0.1', 1, '/structure/list', 1),
	(524, '2023-08-17 23:39:27.692000', '2023-08-17 23:39:27.692000', 2, '127.0.0.1', 1, '/role/search', 1),
	(525, '2023-08-17 23:39:28.020000', '2023-08-17 23:39:28.020000', 11, '127.0.0.1', 1, '/menu/root', 1),
	(526, '2023-08-17 23:39:28.356000', '2023-08-17 23:39:28.356000', 2, '127.0.0.1', 1, '/role/search', 1),
	(527, '2023-08-17 23:39:28.656000', '2023-08-17 23:39:28.656000', 5, '127.0.0.1', 1, '/userInfo/search', 1),
	(528, '2023-08-17 23:39:28.658000', '2023-08-17 23:39:28.658000', 6, '127.0.0.1', 1, '/structure/list', 1),
	(529, '2023-08-17 23:39:29.062000', '2023-08-17 23:39:29.062000', 2, '127.0.0.1', 1, '/role/search', 1),
	(530, '2023-08-17 23:39:29.486000', '2023-08-17 23:39:29.486000', 49, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(531, '2023-08-17 23:39:31.129000', '2023-08-17 23:39:31.129000', 28, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(532, '2023-08-17 23:39:32.610000', '2023-08-17 23:39:32.610000', 26, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(533, '2023-08-17 23:39:33.675000', '2023-08-17 23:39:33.675000', 20, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(534, '2023-08-17 23:39:34.852000', '2023-08-17 23:39:34.852000', 29, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(535, '2023-08-17 23:39:38.282000', '2023-08-17 23:39:38.282000', 19, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(536, '2023-08-17 23:40:11.086000', '2023-08-17 23:40:11.086000', 38, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(537, '2023-08-17 23:40:36.213000', '2023-08-17 23:40:36.213000', 22, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(538, '2023-08-17 23:40:37.058000', '2023-08-17 23:40:37.058000', 18, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(539, '2023-08-17 23:41:00.575000', '2023-08-17 23:41:00.575000', 19, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(540, '2023-08-17 23:41:03.082000', '2023-08-17 23:41:03.082000', 17, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(541, '2023-08-17 23:41:16.569000', '2023-08-17 23:41:16.569000', 16, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(542, '2023-08-17 23:41:18.309000', '2023-08-17 23:41:18.309000', 18, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(543, '2023-08-17 23:41:19.415000', '2023-08-17 23:41:19.415000', 19, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(544, '2023-08-17 23:41:20.946000', '2023-08-17 23:41:20.946000', 14, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(545, '2023-08-17 23:41:55.096000', '2023-08-17 23:41:55.096000', 18, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(546, '2023-08-17 23:43:45.385000', '2023-08-17 23:43:45.385000', 17, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(547, '2023-08-17 23:43:55.351000', '2023-08-17 23:43:55.351000', 15, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(548, '2023-08-17 23:44:04.294000', '2023-08-17 23:44:04.294000', 17, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(549, '2023-08-17 23:44:24.475000', '2023-08-17 23:44:24.475000', 16, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(550, '2023-08-17 23:44:51.861000', '2023-08-17 23:44:51.861000', 16, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(551, '2023-08-17 23:44:53.740000', '2023-08-17 23:44:53.740000', 16, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(552, '2023-08-17 23:45:58.007000', '2023-08-17 23:45:58.007000', 14, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(553, '2023-08-17 23:47:33.976000', '2023-08-17 23:47:33.976000', 17, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(554, '2023-08-17 23:47:45.216000', '2023-08-17 23:47:45.216000', 16, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(555, '2023-08-17 23:48:11.424000', '2023-08-17 23:48:11.424000', 14, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(556, '2023-08-17 23:49:14.006000', '2023-08-17 23:49:14.006000', 8, '127.0.0.1', 1, '/menu/root', 1),
	(557, '2023-08-17 23:49:22.195000', '2023-08-17 23:49:22.195000', 41, '127.0.0.1', 1, '/menu/update', 1),
	(558, '2023-08-17 23:49:22.223000', '2023-08-17 23:49:22.223000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(559, '2023-08-17 23:49:29.149000', '2023-08-17 23:49:29.149000', 6, '127.0.0.1', 1, '/user/login', 1),
	(560, '2023-08-17 23:49:29.170000', '2023-08-17 23:49:29.170000', 2, '127.0.0.1', 1, '/user/current', 1),
	(561, '2023-08-17 23:49:29.186000', '2023-08-17 23:49:29.186000', 4, '127.0.0.1', 1, '/menu/tree', 1),
	(562, '2023-08-17 23:49:31.883000', '2023-08-17 23:49:31.883000', 9, '127.0.0.1', 1, '/menu/root', 1),
	(563, '2023-08-17 23:49:32.445000', '2023-08-17 23:49:32.445000', 15, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(564, '2023-08-17 23:49:33.640000', '2023-08-17 23:49:33.640000', 16, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(565, '2023-08-17 23:49:34.549000', '2023-08-17 23:49:34.549000', 20, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(566, '2023-08-17 23:49:38.170000', '2023-08-17 23:49:38.170000', 15, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(567, '2023-08-17 23:50:26.219000', '2023-08-17 23:50:26.219000', 16, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(568, '2023-08-17 23:50:26.509000', '2023-08-17 23:50:26.509000', 15, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(569, '2023-08-17 23:51:22.881000', '2023-08-17 23:51:22.881000', 15, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(570, '2023-08-17 23:51:47.951000', '2023-08-17 23:51:47.951000', 16, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(571, '2023-08-17 23:51:50.619000', '2023-08-17 23:51:50.619000', 15, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(572, '2023-08-17 23:51:51.581000', '2023-08-17 23:51:51.581000', 16, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(573, '2023-08-17 23:51:52.192000', '2023-08-17 23:51:52.192000', 14, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(574, '2023-08-17 23:51:53.245000', '2023-08-17 23:51:53.245000', 16, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(575, '2023-08-17 23:51:53.817000', '2023-08-17 23:51:53.817000', 17, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(576, '2023-08-17 23:51:55.431000', '2023-08-17 23:51:55.431000', 14, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(577, '2023-08-17 23:51:57.258000', '2023-08-17 23:51:57.258000', 16, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(578, '2023-08-17 23:51:58.648000', '2023-08-17 23:51:58.648000', 16, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(579, '2023-08-17 23:51:59.579000', '2023-08-17 23:51:59.579000', 20, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(580, '2023-08-17 23:52:00.722000', '2023-08-17 23:52:00.722000', 14, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(581, '2023-08-17 23:52:01.593000', '2023-08-17 23:52:01.593000', 15, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(582, '2023-08-17 23:52:03.001000', '2023-08-17 23:52:03.001000', 15, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(583, '2023-08-17 23:52:04.176000', '2023-08-17 23:52:04.176000', 16, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(584, '2023-08-17 23:52:04.978000', '2023-08-17 23:52:04.978000', 14, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(585, '2023-08-17 23:52:05.386000', '2023-08-17 23:52:05.386000', 12, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(586, '2023-08-17 23:52:06.108000', '2023-08-17 23:52:06.108000', 14, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(587, '2023-08-17 23:52:07.100000', '2023-08-17 23:52:07.100000', 16, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(588, '2023-08-17 23:52:08.165000', '2023-08-17 23:52:08.165000', 14, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(589, '2023-08-17 23:52:15.595000', '2023-08-17 23:52:15.595000', 13, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(590, '2023-08-17 23:52:16.561000', '2023-08-17 23:52:16.561000', 14, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(591, '2023-08-17 23:52:17.256000', '2023-08-17 23:52:17.256000', 14, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(592, '2023-08-17 23:52:17.712000', '2023-08-17 23:52:17.712000', 15, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(593, '2023-08-17 23:52:18.178000', '2023-08-17 23:52:18.178000', 13, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(594, '2023-08-17 23:52:18.667000', '2023-08-17 23:52:18.667000', 16, '127.0.0.1', 1, '/schedulingTaskRecord/search', 1),
	(595, '2023-08-17 23:52:43.616000', '2023-08-17 23:52:43.616000', 5, '0:0:0:0:0:0:0:1', 1, '/user/login', 1),
	(596, '2023-08-17 23:52:43.638000', '2023-08-17 23:52:43.638000', 2, '0:0:0:0:0:0:0:1', 1, '/user/current', 1),
	(597, '2023-08-17 23:52:43.652000', '2023-08-17 23:52:43.652000', 4, '0:0:0:0:0:0:0:1', 1, '/menu/tree', 1),
	(598, '2023-08-17 23:52:45.529000', '2023-08-17 23:52:45.529000', 2, '0:0:0:0:0:0:0:1', 1, '/user/current', 1),
	(599, '2023-08-17 23:52:45.529000', '2023-08-17 23:52:45.529000', 2, '0:0:0:0:0:0:0:1', 1, '/user/current', 1),
	(600, '2023-08-17 23:52:46.151000', '2023-08-17 23:52:46.151000', 15, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/search', 1),
	(601, '2023-08-17 23:52:47.604000', '2023-08-17 23:52:47.604000', 14, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/view', 1),
	(602, '2023-08-17 23:52:48.564000', '2023-08-17 23:52:48.564000', 16, '0:0:0:0:0:0:0:1', 1, '/schedulingTaskRecord/search', 1),
	(603, '2023-08-17 23:52:49.916000', '2023-08-17 23:52:49.916000', 15, '0:0:0:0:0:0:0:1', 1, '/schedulingTaskRecord/search', 1),
	(604, '2023-08-17 23:52:50.379000', '2023-08-17 23:52:50.379000', 15, '0:0:0:0:0:0:0:1', 1, '/schedulingTaskRecord/search', 1),
	(605, '2023-08-17 23:52:50.815000', '2023-08-17 23:52:50.815000', 13, '0:0:0:0:0:0:0:1', 1, '/schedulingTaskRecord/search', 1),
	(606, '2023-08-17 23:52:51.896000', '2023-08-17 23:52:51.896000', 15, '0:0:0:0:0:0:0:1', 1, '/schedulingTaskRecord/search', 1),
	(607, '2023-08-17 23:52:52.798000', '2023-08-17 23:52:52.798000', 13, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/view', 1),
	(608, '2023-08-17 23:52:53.492000', '2023-08-17 23:52:53.492000', 18, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/search', 1),
	(609, '2023-08-17 23:52:55.788000', '2023-08-17 23:52:55.788000', 14, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/view', 1),
	(610, '2023-08-17 23:53:00.498000', '2023-08-17 23:53:00.498000', 21, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/update', 1),
	(611, '2023-08-17 23:53:00.556000', '2023-08-17 23:53:00.556000', 17, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/search', 1),
	(612, '2023-08-17 23:54:39.304000', '2023-08-17 23:54:39.304000', 14, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(613, '2023-08-17 23:54:40.804000', '2023-08-17 23:54:40.804000', 2, '127.0.0.1', 1, '/user/current', 1),
	(614, '2023-08-17 23:54:40.805000', '2023-08-17 23:54:40.805000', 2, '127.0.0.1', 1, '/user/current', 1),
	(615, '2023-08-17 23:54:41.786000', '2023-08-17 23:54:41.786000', 12, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(616, '2023-08-17 23:54:42.299000', '2023-08-17 23:54:42.299000', 12, '127.0.0.1', 1, '/menu/root', 1),
	(617, '2023-08-17 23:54:42.659000', '2023-08-17 23:54:42.659000', 1, '127.0.0.1', 1, '/role/search', 1),
	(618, '2023-08-17 23:54:43.014000', '2023-08-17 23:54:43.014000', 3, '127.0.0.1', 1, '/userInfo/search', 1),
	(619, '2023-08-17 23:54:43.015000', '2023-08-17 23:54:43.015000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(620, '2023-08-17 23:54:43.447000', '2023-08-17 23:54:43.447000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(621, '2023-08-17 23:54:43.858000', '2023-08-17 23:54:43.858000', 12, '127.0.0.1', 1, '/config/info', 1),
	(622, '2023-08-17 23:54:44.164000', '2023-08-17 23:54:44.164000', 1, '127.0.0.1', 1, '/dict/list', 1),
	(623, '2023-08-17 23:54:44.436000', '2023-08-17 23:54:44.436000', 3, '127.0.0.1', 1, '/userInfo/search', 1),
	(624, '2023-08-17 23:54:44.437000', '2023-08-17 23:54:44.437000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(625, '2023-08-17 23:54:44.708000', '2023-08-17 23:54:44.708000', 1, '127.0.0.1', 1, '/role/search', 1),
	(626, '2023-08-17 23:54:45.043000', '2023-08-17 23:54:45.043000', 8, '127.0.0.1', 1, '/menu/root', 1),
	(627, '2023-08-17 23:54:45.344000', '2023-08-17 23:54:45.344000', 16, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(628, '2023-08-17 23:54:45.657000', '2023-08-17 23:54:45.657000', 2, '127.0.0.1', 1, '/user/current', 1),
	(629, '2023-08-17 23:54:45.658000', '2023-08-17 23:54:45.658000', 3, '127.0.0.1', 1, '/user/current', 1),
	(630, '2023-08-17 23:54:46.050000', '2023-08-17 23:54:46.050000', 14, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(631, '2023-08-17 23:54:46.358000', '2023-08-17 23:54:46.358000', 7, '127.0.0.1', 1, '/menu/root', 1),
	(632, '2023-08-17 23:54:46.672000', '2023-08-17 23:54:46.672000', 2, '127.0.0.1', 1, '/role/search', 1),
	(633, '2023-08-17 23:54:46.989000', '2023-08-17 23:54:46.989000', 3, '127.0.0.1', 1, '/userInfo/search', 1),
	(634, '2023-08-17 23:54:46.990000', '2023-08-17 23:54:46.990000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(635, '2023-08-17 23:54:47.331000', '2023-08-17 23:54:47.331000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(636, '2023-08-17 23:54:47.724000', '2023-08-17 23:54:47.724000', 2, '127.0.0.1', 1, '/config/info', 1),
	(637, '2023-08-17 23:54:48.195000', '2023-08-17 23:54:48.195000', 1, '127.0.0.1', 1, '/dict/list', 1),
	(638, '2023-08-17 23:54:48.503000', '2023-08-17 23:54:48.503000', 2, '127.0.0.1', 1, '/userInfo/search', 1),
	(639, '2023-08-17 23:54:48.505000', '2023-08-17 23:54:48.505000', 4, '127.0.0.1', 1, '/structure/list', 1),
	(640, '2023-08-17 23:54:48.755000', '2023-08-17 23:54:48.755000', 2, '127.0.0.1', 1, '/role/search', 1),
	(641, '2023-08-17 23:54:49.047000', '2023-08-17 23:54:49.047000', 7, '127.0.0.1', 1, '/menu/root', 1),
	(642, '2023-08-17 23:54:49.317000', '2023-08-17 23:54:49.317000', 12, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(643, '2023-08-17 23:54:49.561000', '2023-08-17 23:54:49.561000', 2, '127.0.0.1', 1, '/user/current', 1),
	(644, '2023-08-17 23:54:49.561000', '2023-08-17 23:54:49.561000', 2, '127.0.0.1', 1, '/user/current', 1),
	(645, '2023-08-17 23:54:49.920000', '2023-08-17 23:54:49.920000', 15, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(646, '2023-08-17 23:54:50.206000', '2023-08-17 23:54:50.206000', 7, '127.0.0.1', 1, '/menu/root', 1),
	(647, '2023-08-17 23:54:50.550000', '2023-08-17 23:54:50.550000', 2, '127.0.0.1', 1, '/role/search', 1),
	(648, '2023-08-17 23:54:50.898000', '2023-08-17 23:54:50.898000', 3, '127.0.0.1', 1, '/userInfo/search', 1),
	(649, '2023-08-17 23:54:50.898000', '2023-08-17 23:54:50.898000', 3, '127.0.0.1', 1, '/structure/list', 1),
	(650, '2023-08-17 23:54:51.176000', '2023-08-17 23:54:51.176000', 2, '127.0.0.1', 1, '/dict/list', 1),
	(651, '2023-08-17 23:54:51.728000', '2023-08-17 23:54:51.728000', 2, '127.0.0.1', 1, '/config/info', 1),
	(652, '2023-08-17 23:54:52.015000', '2023-08-17 23:54:52.015000', 1, '127.0.0.1', 1, '/dict/list', 1),
	(653, '2023-08-17 23:54:52.269000', '2023-08-17 23:54:52.269000', 1, '127.0.0.1', 1, '/role/search', 1),
	(654, '2023-08-17 23:54:52.539000', '2023-08-17 23:54:52.539000', 6, '127.0.0.1', 1, '/menu/root', 1),
	(655, '2023-08-17 23:54:52.809000', '2023-08-17 23:54:52.809000', 15, '127.0.0.1', 1, '/schedulingTask/search', 1),
	(656, '2023-08-17 23:54:56.993000', '2023-08-17 23:54:56.993000', 15, '127.0.0.1', 1, '/schedulingTask/view', 1),
	(657, '2023-08-17 23:54:59.613000', '2023-08-17 23:54:59.613000', 15, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/view', 1),
	(658, '2023-08-17 23:55:00.730000', '2023-08-17 23:55:00.730000', 15, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/search', 1),
	(659, '2023-08-17 23:55:01.527000', '2023-08-17 23:55:01.527000', 17, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/view', 1),
	(660, '2023-08-17 23:55:02.376000', '2023-08-17 23:55:02.376000', 15, '0:0:0:0:0:0:0:1', 1, '/schedulingTaskRecord/search', 1),
	(661, '2023-08-17 23:55:02.914000', '2023-08-17 23:55:02.914000', 12, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/view', 1),
	(662, '2023-08-17 23:55:03.730000', '2023-08-17 23:55:03.730000', 16, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/search', 1),
	(663, '2023-08-17 23:55:04.973000', '2023-08-17 23:55:04.973000', 15, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/view', 1),
	(664, '2023-08-17 23:55:06.331000', '2023-08-17 23:55:06.331000', 14, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/search', 1),
	(665, '2023-08-17 23:55:33.785000', '2023-08-17 23:55:33.785000', 38, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/create', 1),
	(666, '2023-08-17 23:55:33.845000', '2023-08-17 23:55:33.845000', 15, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/search', 1),
	(667, '2023-08-17 23:57:17.005000', '2023-08-17 23:57:17.005000', 2, '0:0:0:0:0:0:0:1', 1, '/user/current', 1),
	(668, '2023-08-17 23:57:17.005000', '2023-08-17 23:57:17.005000', 2, '0:0:0:0:0:0:0:1', 1, '/user/current', 1),
	(669, '2023-08-17 23:57:24.408000', '2023-08-17 23:57:24.408000', 3, '0:0:0:0:0:0:0:1', 1, '/user/current', 1),
	(670, '2023-08-17 23:57:26.103000', '2023-08-17 23:57:26.103000', 2, '0:0:0:0:0:0:0:1', 1, '/user/current', 1),
	(671, '2023-08-17 23:57:28.036000', '2023-08-17 23:57:28.036000', 2, '0:0:0:0:0:0:0:1', 1, '/user/current', 1),
	(672, '2023-08-17 23:57:29.005000', '2023-08-17 23:57:29.005000', 2, '0:0:0:0:0:0:0:1', 1, '/user/current', 1),
	(673, '2023-08-17 23:57:30.089000', '2023-08-17 23:57:30.089000', 2, '0:0:0:0:0:0:0:1', 1, '/user/current', 1),
	(674, '2023-08-17 23:57:32.779000', '2023-08-17 23:57:32.779000', 14, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/search', 1),
	(675, '2023-08-17 23:57:33.841000', '2023-08-17 23:57:33.841000', 10, '0:0:0:0:0:0:0:1', 1, '/menu/root', 1),
	(676, '2023-08-17 23:57:34.396000', '2023-08-17 23:57:34.396000', 2, '0:0:0:0:0:0:0:1', 1, '/role/search', 1),
	(677, '2023-08-17 23:57:34.809000', '2023-08-17 23:57:34.809000', 2, '0:0:0:0:0:0:0:1', 1, '/userInfo/search', 1),
	(678, '2023-08-17 23:57:34.810000', '2023-08-17 23:57:34.810000', 3, '0:0:0:0:0:0:0:1', 1, '/structure/list', 1),
	(679, '2023-08-17 23:57:35.126000', '2023-08-17 23:57:35.126000', 2, '0:0:0:0:0:0:0:1', 1, '/dict/list', 1),
	(680, '2023-08-17 23:57:35.511000', '2023-08-17 23:57:35.511000', 2, '0:0:0:0:0:0:0:1', 1, '/config/info', 1),
	(681, '2023-08-17 23:57:36.469000', '2023-08-17 23:57:36.469000', 1, '0:0:0:0:0:0:0:1', 1, '/dict/list', 1),
	(682, '2023-08-17 23:57:36.848000', '2023-08-17 23:57:36.848000', 3, '0:0:0:0:0:0:0:1', 1, '/userInfo/search', 1),
	(683, '2023-08-17 23:57:36.849000', '2023-08-17 23:57:36.849000', 4, '0:0:0:0:0:0:0:1', 1, '/structure/list', 1),
	(684, '2023-08-17 23:57:37.628000', '2023-08-17 23:57:37.628000', 1, '0:0:0:0:0:0:0:1', 1, '/role/search', 1),
	(685, '2023-08-17 23:57:38.438000', '2023-08-17 23:57:38.438000', 13, '0:0:0:0:0:0:0:1', 1, '/schedulingTask/search', 1),
	(686, '2023-08-17 23:57:38.721000', '2023-08-17 23:57:38.721000', 2, '0:0:0:0:0:0:0:1', 1, '/user/current', 1),
	(687, '2023-08-17 23:57:38.721000', '2023-08-17 23:57:38.721000', 2, '0:0:0:0:0:0:0:1', 1, '/user/current', 1);

-- 导出  表 nbsaas-admin.user_account 结构
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE IF NOT EXISTS `user_account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `account_type` int(11) DEFAULT NULL COMMENT '账号类型',
  `login_size` int(11) DEFAULT NULL COMMENT '登陆次数',
  `username` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户名',
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKloyhlvrn82g8811wyjaa8ehm0` (`user_id`),
  CONSTRAINT `FKloyhlvrn82g8811wyjaa8ehm0` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户账号';

-- 正在导出表  nbsaas-admin.user_account 的数据：~3 rows (大约)
DELETE FROM `user_account`;
INSERT INTO `user_account` (`id`, `add_date`, `last_date`, `account_type`, `login_size`, `username`, `user_id`) VALUES
	(1, '2023-06-23 10:57:32.870000', '2023-06-23 10:57:32.870000', 0, 0, 'ada', 1),
	(2, '2023-06-23 10:57:41.108000', '2023-06-23 10:57:41.108000', 0, 0, '1', 2),
	(3, '2023-06-23 10:58:14.048000', '2023-06-23 10:58:14.048000', 0, 0, 'admin', 3);

-- 导出  表 nbsaas-admin.user_info 结构
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE IF NOT EXISTS `user_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `avatar` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户头像',
  `catalog` int(11) DEFAULT NULL COMMENT '用户类型',
  `data_scope` int(11) DEFAULT NULL COMMENT '数据范围',
  `login_size` int(11) DEFAULT NULL COMMENT '用户登录次数',
  `name` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户真实姓名',
  `note` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户介绍',
  `phone` varchar(15) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手机号码',
  `state` int(11) DEFAULT NULL COMMENT '用户状态',
  `account_no` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号',
  `store_state` int(11) DEFAULT NULL COMMENT '用户状态',
  `sex` varchar(2) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '性别',
  `structure_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKhrwdxo6u0y0yxjvkm6sq8aea3` (`structure_id`),
  CONSTRAINT `FKhrwdxo6u0y0yxjvkm6sq8aea3` FOREIGN KEY (`structure_id`) REFERENCES `sys_structure` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户信息表';

-- 正在导出表  nbsaas-admin.user_info 的数据：~3 rows (大约)
DELETE FROM `user_info`;
INSERT INTO `user_info` (`id`, `add_date`, `last_date`, `avatar`, `catalog`, `data_scope`, `login_size`, `name`, `note`, `phone`, `state`, `account_no`, `store_state`, `sex`, `structure_id`) VALUES
	(1, '2023-06-23 10:57:32.846000', '2023-08-13 17:22:12.317000', '', NULL, 3, 0, 'ada', NULL, 'ada', NULL, NULL, NULL, NULL, 1),
	(2, '2023-06-23 10:57:41.107000', '2023-07-03 23:50:41.877000', '', NULL, NULL, 0, '1', NULL, '1', NULL, NULL, NULL, NULL, 3),
	(3, '2023-06-23 10:58:14.048000', '2023-07-03 23:49:38.910000', '', NULL, NULL, 0, 'admin', NULL, 'admin', NULL, NULL, NULL, NULL, 4);

-- 导出  表 nbsaas-admin.user_info_attribute 结构
DROP TABLE IF EXISTS `user_info_attribute`;
CREATE TABLE IF NOT EXISTS `user_info_attribute` (
  `user_id` bigint(20) NOT NULL,
  `attr` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(36) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`user_id`,`name`),
  CONSTRAINT `FKjf8g1vng9lpmniy8u187j7t0` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  nbsaas-admin.user_info_attribute 的数据：~0 rows (大约)
DELETE FROM `user_info_attribute`;

-- 导出  表 nbsaas-admin.user_login_log 结构
DROP TABLE IF EXISTS `user_login_log`;
CREATE TABLE IF NOT EXISTS `user_login_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `account` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '登陆账号',
  `client` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '登陆客户端',
  `ip` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '登录IP',
  `note` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `password` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '登录密码',
  `state` int(11) DEFAULT NULL COMMENT '登录状态，0为失败1为成功',
  `store_state` int(11) DEFAULT NULL COMMENT '存储状态',
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKj91w0nnfocpdp796lr3ot4lxs` (`user_id`),
  CONSTRAINT `FKj91w0nnfocpdp796lr3ot4lxs` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户登录记录';

-- 正在导出表  nbsaas-admin.user_login_log 的数据：~30 rows (大约)
DELETE FROM `user_login_log`;
INSERT INTO `user_login_log` (`id`, `add_date`, `last_date`, `account`, `client`, `ip`, `note`, `password`, `state`, `store_state`, `user_id`) VALUES
	(1, '2023-07-09 10:37:19.473000', '2023-07-09 10:37:19.473000', 'admin', 'web front', '127.0.0.1', NULL, '', 1, 1, 3),
	(2, '2023-07-09 10:41:39.355000', '2023-07-09 10:41:39.355000', 'admin', 'web front', '127.0.0.1', NULL, '', 1, 1, 3),
	(3, '2023-07-09 10:59:27.318000', '2023-07-09 10:59:27.318000', 'admin', 'web front', '127.0.0.1', NULL, '', 1, 1, 3),
	(4, '2023-07-09 11:00:09.401000', '2023-07-09 11:00:09.401000', 'admin', 'web front', '127.0.0.1', NULL, '', 1, 1, 3),
	(5, '2023-08-12 17:08:12.237000', '2023-08-12 17:08:12.237000', 'admin', 'web front', '127.0.0.1', NULL, '', 1, 1, 3),
	(6, '2023-08-12 17:49:14.454000', '2023-08-12 17:49:14.454000', 'admin', 'web front', '127.0.0.1', NULL, '', 1, 1, 3),
	(7, '2023-08-13 17:16:39.555000', '2023-08-13 17:16:39.555000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(8, '2023-08-13 17:16:41.400000', '2023-08-13 17:16:41.400000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(9, '2023-08-13 17:16:56.974000', '2023-08-13 17:16:56.974000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(10, '2023-08-13 17:16:58.095000', '2023-08-13 17:16:58.095000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(11, '2023-08-13 17:17:08.685000', '2023-08-13 17:17:08.685000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(12, '2023-08-13 17:17:09.761000', '2023-08-13 17:17:09.761000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(13, '2023-08-13 17:17:09.970000', '2023-08-13 17:17:09.970000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(14, '2023-08-13 17:17:10.164000', '2023-08-13 17:17:10.164000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(15, '2023-08-13 17:17:10.339000', '2023-08-13 17:17:10.339000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(16, '2023-08-13 17:17:10.508000', '2023-08-13 17:17:10.508000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(17, '2023-08-13 17:17:44.259000', '2023-08-13 17:17:44.259000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(18, '2023-08-13 17:17:47.463000', '2023-08-13 17:17:47.463000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(19, '2023-08-13 17:17:58.106000', '2023-08-13 17:17:58.106000', 'admin', 'web front', '127.0.0.1', NULL, 'wtjt1234', 0, 1, NULL),
	(20, '2023-08-13 17:18:00.846000', '2023-08-13 17:18:00.846000', 'admin', 'web front', '127.0.0.1', NULL, '1', 0, 1, NULL),
	(21, '2023-08-13 17:18:03.949000', '2023-08-13 17:18:03.949000', 'admin', 'web front', '127.0.0.1', NULL, '', 1, 1, 3),
	(22, '2023-08-13 17:18:40.587000', '2023-08-13 17:18:40.587000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(23, '2023-08-13 17:18:50.542000', '2023-08-13 17:18:50.542000', 'admin', 'web front', '127.0.0.1', NULL, '', 1, 1, 3),
	(24, '2023-08-13 17:21:05.077000', '2023-08-13 17:21:05.077000', 'ada', 'web front', '127.0.0.1', NULL, '123456', 0, 1, NULL),
	(25, '2023-08-13 17:21:13.042000', '2023-08-13 17:21:13.042000', 'admin', 'web front', '127.0.0.1', NULL, '', 1, 1, 3),
	(26, '2023-08-13 17:21:38.132000', '2023-08-13 17:21:38.132000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(27, '2023-08-13 17:21:46.253000', '2023-08-13 17:21:46.253000', 'admin', 'web front', '127.0.0.1', NULL, '', 1, 1, 3),
	(28, '2023-08-13 17:22:18.335000', '2023-08-13 17:22:18.335000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(29, '2023-08-13 17:24:57.316000', '2023-08-13 17:24:57.316000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(30, '2023-08-13 17:38:09.123000', '2023-08-13 17:38:09.123000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(31, '2023-08-13 17:44:22.981000', '2023-08-13 17:44:22.981000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(32, '2023-08-13 18:27:33.026000', '2023-08-13 18:27:33.026000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(33, '2023-08-13 20:55:23.279000', '2023-08-13 20:55:23.279000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(34, '2023-08-17 21:51:46.458000', '2023-08-17 21:51:46.458000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(35, '2023-08-17 23:02:59.116000', '2023-08-17 23:02:59.116000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(36, '2023-08-17 23:03:45.637000', '2023-08-17 23:03:45.637000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(37, '2023-08-17 23:12:39.822000', '2023-08-17 23:12:39.822000', 'ada', 'web front', '0:0:0:0:0:0:0:1', NULL, '', 1, 1, 1),
	(38, '2023-08-17 23:16:07.090000', '2023-08-17 23:16:07.090000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(39, '2023-08-17 23:18:04.473000', '2023-08-17 23:18:04.473000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(40, '2023-08-17 23:35:40.973000', '2023-08-17 23:35:40.973000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(41, '2023-08-17 23:39:21.006000', '2023-08-17 23:39:21.006000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(42, '2023-08-17 23:49:29.149000', '2023-08-17 23:49:29.149000', 'ada', 'web front', '127.0.0.1', NULL, '', 1, 1, 1),
	(43, '2023-08-17 23:52:43.616000', '2023-08-17 23:52:43.616000', 'ada', 'web front', '0:0:0:0:0:0:0:1', NULL, '', 1, 1, 1);

-- 导出  表 nbsaas-admin.user_oauth_config 结构
DROP TABLE IF EXISTS `user_oauth_config`;
CREATE TABLE IF NOT EXISTS `user_oauth_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `app_key` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '程序key',
  `app_secret` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '程序密钥',
  `class_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'java实现类',
  `model` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '第三方登陆标识',
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '第三方登陆名称',
  `state` int(11) DEFAULT NULL COMMENT '启用状态  1启用 2未启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='第三方登录配置表';

-- 正在导出表  nbsaas-admin.user_oauth_config 的数据：~0 rows (大约)
DELETE FROM `user_oauth_config`;

-- 导出  表 nbsaas-admin.user_oauth_token 结构
DROP TABLE IF EXISTS `user_oauth_token`;
CREATE TABLE IF NOT EXISTS `user_oauth_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `access_token` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '访问token',
  `expires_time` bigint(20) DEFAULT NULL COMMENT '过期时间',
  `login_size` int(11) DEFAULT NULL COMMENT '登录次数',
  `open_id` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户openId',
  `refresh_token` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '刷新token',
  `token_type` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'token类型',
  `union_id` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户unionId',
  `user_id` bigint(20) DEFAULT NULL,
  `user_oauth_config_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKl7l4hbuly2wmp7tul1ldjeh85` (`user_id`),
  KEY `FKihm5qlb22d3ofuohg6f8l7w81` (`user_oauth_config_id`),
  CONSTRAINT `FKihm5qlb22d3ofuohg6f8l7w81` FOREIGN KEY (`user_oauth_config_id`) REFERENCES `user_oauth_config` (`id`),
  CONSTRAINT `FKl7l4hbuly2wmp7tul1ldjeh85` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户oauth登陆信息';

-- 正在导出表  nbsaas-admin.user_oauth_token 的数据：~0 rows (大约)
DELETE FROM `user_oauth_token`;

-- 导出  表 nbsaas-admin.user_password 结构
DROP TABLE IF EXISTS `user_password`;
CREATE TABLE IF NOT EXISTS `user_password` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `check_size` int(11) DEFAULT NULL COMMENT '校验次数',
  `password` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
  `salt` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '加密盐',
  `security_type` int(11) DEFAULT NULL COMMENT '安全类型',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`),
  KEY `FKbnfetqc91yx2vu6qwk04ceci7` (`user_id`),
  CONSTRAINT `FKbnfetqc91yx2vu6qwk04ceci7` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户密码';

-- 正在导出表  nbsaas-admin.user_password 的数据：~3 rows (大约)
DELETE FROM `user_password`;
INSERT INTO `user_password` (`id`, `add_date`, `last_date`, `check_size`, `password`, `salt`, `security_type`, `user_id`) VALUES
	(1, '2023-06-23 10:57:32.882000', '2023-08-13 17:21:33.843000', 0, 'EHED1Y5VOXyg3k2fQe9bUYv9IiSOJDnuVGhmlICaVkc=', 'LFGJPaC5p2Zz4SYzo+836QXR8Qnl/MoFUqtBuBFU0Ro=', 0, 1),
	(2, '2023-06-23 10:57:41.112000', '2023-06-23 10:57:41.112000', 0, 'kudVeSUFr8DMYSPiDvtgPFp3EQ/a1+KPtTobZZ6PAhY=', 'HrV7bdZN2O6TDmfV4RuQ9Onk6iqfLs5CvYTwHRLA+wc=', 0, 2),
	(3, '2023-06-23 10:58:14.051000', '2023-08-13 17:20:56.954000', 0, 'pMv2KyiL03UtIazuzjZt2HNm1qcxW3E8EsVNsKILQ24=', 'fq1pZdTDSmxifdaioio1WLHDVMT4LyVVrQlq8oRZtU0=', 0, 3);

-- 导出  表 nbsaas-admin.user_role 结构
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE IF NOT EXISTS `user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `add_date` datetime(6) DEFAULT NULL COMMENT '添加时间',
  `last_date` datetime(6) DEFAULT NULL COMMENT '最新修改时间',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色id',
  `user_info_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbk0u1la56lxsmwbi40xd2wopr` (`user_info_id`),
  CONSTRAINT `FKbk0u1la56lxsmwbi40xd2wopr` FOREIGN KEY (`user_info_id`) REFERENCES `user_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户角色表';

-- 正在导出表  nbsaas-admin.user_role 的数据：~2 rows (大约)
DELETE FROM `user_role`;
INSERT INTO `user_role` (`id`, `add_date`, `last_date`, `role_id`, `user_info_id`) VALUES
	(1, NULL, NULL, 1, 3),
	(3, '2023-08-13 17:22:12.354000', '2023-08-13 17:22:12.354000', 1, 1);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
