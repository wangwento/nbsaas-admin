# nbsaas-admin

[![maven](https://img.shields.io/maven-central/v/com.nbsaas.boot/nbsaas-boot.svg)](http://mvnrepository.com/artifact/com.nbsaas.boot/nbsaas-boot)
[![QQ](https://img.shields.io/badge/chat-on%20QQ-ff69b4.svg?style=flat-square)](//shang.qq.com/wpa/qunwpa?idkey=d1a308945e4b2ff8aeb1711c2c7914342dae15e9ce7041e94756ab355430dc78)
[![Apache-2.0](https://img.shields.io/hexpm/l/plug.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)
[![使用IntelliJ IDEA开发维护](https://img.shields.io/badge/IntelliJ%20IDEA-提供支持-blue.svg)](https://www.jetbrains.com/idea/)
[![GitHub forks](https://img.shields.io/github/stars/nbsaas/nbsaas-boot.svg?style=social&logo=github&label=Stars)](https://github.com/nbsaas/nbsaas-boot)

#### 介绍
nbsaas-admin后台项目模块工程

#### 后台首页

![后台首页](http://file.nbsaas.com/newbyte/upload/image/202308/90294ef3-6296-44ab-817c-b27eab3ff5dd.png)

### 技术选型：

* **服务端**
* Spring、SpringMVC、spring data jpa
* Spring boot,Spring cloud,Spring alibaba
* 安全权限 Shiro
* 缓存 Ehcache
* 视图模板 freemarker
* 其它 Jsoup、gson
* 核心采用Request-Response模式，Chain模型。

## 编码规范

### 1.项目结构规范

```
{主工程}
{主工程}.adapters
{主工程}.admins
{主工程}.api
{主工程}.apps
{主工程}.code-generator
{主工程}.commons
{主工程}.gates
{主工程}.gateway
{主工程}.models
{主工程}.resources

```

### 2.Api模块结构规范

```
com.{公司域名}.{主工程}.{子工程}
com.{公司域名}.{主工程}.{子工程}.api.apis
com.{公司域名}.{主工程}.{子工程}.api.domain.enums
com.{公司域名}.{主工程}.{子工程}.api.domain.request
com.{公司域名}.{主工程}.{子工程}.api.domain.response
com.{公司域名}.{主工程}.{子工程}.api.domain.simple
com.{公司域名}.{主工程}.{子工程}.ext.apis
com.{公司域名}.{主工程}.{子工程}.ext.domain.enums
com.{公司域名}.{主工程}.{子工程}.ext.domain.request
com.{公司域名}.{主工程}.{子工程}.ext.domain.response
com.{公司域名}.{主工程}.{子工程}.ext.domain.simple
```

### 3.Resource模块结构规范

```
com.{公司域名}.{主工程}.{子工程}
com.{公司域名}.{主工程}.{子工程}.data.entity
com.{公司域名}.{主工程}.{子工程}.data.repository
com.{公司域名}.{主工程}.{子工程}.rest.conver
com.{公司域名}.{主工程}.{子工程}.rest.resource
com.{公司域名}.{主工程}.{子工程}.ext.conver
com.{公司域名}.{主工程}.{子工程}.ext.resource
```

### 4.api接口

```
/**
 * 响应接口
 *
 * @param <Response> 详情对象
 * @param <Simple>   列表对象
 * @param <Request>     表单对象
 */
public interface ResponseApi<Response, Simple, Request extends RequestId> {

    /**
     * 分页查询
     *
     * @param request
     * @return 分页数据信息
     */
    PageResponse<Simple> search(PageRequest request);

    /**
     * 根据条件查询集合，不分页
     *
     * @param request
     * @return 数据集合数据
     */
    ListResponse<Simple> list(PageRequest request);

    /**
     * 创建
     *
     * @param request
     * @return 数据详情
     */
    ResponseObject<Response> create(Request request);

    /**
     * 更新
     *
     * @param request
     * @return 数据详情
     */
    ResponseObject<Response> update(RequestId request);

    /**
     * 删除
     *
     * @param request
     * @return 删除状态
     */
    ResponseObject<?> delete(RequestId request);

    /**
     * 根据ID查询详情
     *
     * @param request
     * @return 数据详情
     */
    ResponseObject<Response> view(RequestId request);

}
```

### 5.搜索对象

```
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UserInfoSearchRequest   extends PageRequest implements Serializable {


      
      @Search(name = "phone",operator = Operator.like)
      private String phone;

     @Search(name = "catalog",operator = Operator.eq)
      private Integer catalog;

    
     @Search(name = "note",operator = Operator.like)
      private String note;
     
     @Search(name = "loginSize",operator = Operator.eq)
      private Integer loginSize;
     
     @Search(name = "name",operator = Operator.like)
      private String name;


}
```



## 搭建步骤

1. 创建数据库。如使用MySQL，字符集选择为`utf8`或者`utf8mb4`（支持更多特殊字符，推荐）。
2. 在idea中导入maven项目。点击idea菜单`File` - `open`，选择 `项目所在磁盘位置`。创建好maven项目后，会开始从maven服务器下载第三方jar包（如spring等），需要一定时间，请耐心等待。
3. 创建mysql数据库，导入`/documents/nbsaas_admin.sql`
4. 修改数据库连接。打开`/gates/admin/main/resources/application.yml`文件，根据实际情况修改`jdbc.url`、`jdbc.username`、`jdbc.password`的值。
5. 运行程序AdminApplication(后台)，FrontApplication(前台)
6. 访问系统。前台接口地址：[http://localhost:8001](http://localhost:8081/)，后台接口地址：[http://127.0.0.1:8002/](http://127.0.0.1:8082/)；用户名：ada，密码：123456。
7. 下载前端代码 https://gitee.com/cng1985/nbsaas-admin-vue
8. 安装前端依赖 npm i
9. 启动前端  npm run dev

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

