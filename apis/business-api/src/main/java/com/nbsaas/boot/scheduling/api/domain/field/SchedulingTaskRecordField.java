package com.nbsaas.boot.scheduling.api.domain.field;


/**
*   字段映射类
*/
public class SchedulingTaskRecordField  {



    public static final String  note = "note";


    public static final String  schedulingTask = "schedulingTask";


    public static final String  schedulingTaskName = "schedulingTaskName";


    public static final String  state = "state";


    public static final String  id = "id";


    public static final String  addDate = "addDate";


    public static final String  lastDate = "lastDate";

}