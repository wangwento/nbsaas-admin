package com.nbsaas.boot.todo.api.domain.field;


/**
*   字段映射类
*/
public class TodoTemplateField  {



    public static final String  dataKey = "dataKey";


    public static final String  extData = "extData";


    public static final String  navigateUrl = "navigateUrl";


    public static final String  name = "name";


    public static final String  id = "id";


    public static final String  addDate = "addDate";


    public static final String  lastDate = "lastDate";

}