package com.nbsaas.boot.article.api.domain.request;

import com.nbsaas.boot.rest.filter.Operator;
import com.nbsaas.boot.rest.filter.Search;
import com.nbsaas.boot.rest.request.PageRequest;
import lombok.*;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
* 搜索bean
*/
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ArticleSearchRequest   extends PageRequest implements Serializable {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;


    @Search(name = "catalog.id",operator = Operator.eq)
    private Long catalog;

    @Search(name = "user.name",operator = Operator.like)
    private String userName;

    @Search(name = "catalog.name",operator = Operator.like)
    private String catalogName;


            /**
            * 
            **/
            @Search(name = "images",operator = Operator.like)
            private String images;

            /**
            * 
            **/
            @Search(name = "img",operator = Operator.like)
            private String img;

            /**
            * 
            **/
            @Search(name = "comments",operator = Operator.eq)
            private Integer comments;

            /**
            * 
            **/
            @Search(name = "title",operator = Operator.like)
            private String title;

            /**
            * 
            **/
            @Search(name = "extData",operator = Operator.like)
            private String extData;

            /**
            * 
            **/
            @Search(name = "ups",operator = Operator.eq)
            private Integer ups;

            /**
            * 主键id
            **/
            @Search(name = "id",operator = Operator.eq)
            private Long id;

            /**
            * 
            **/
            @Search(name = "introduction",operator = Operator.like)
            private String introduction;

            /**
            * 
            **/
            @Search(name = "views",operator = Operator.eq)
            private Integer views;

            /**
            * 
            **/
            @Search(name = "likes",operator = Operator.eq)
            private Integer likes;



}