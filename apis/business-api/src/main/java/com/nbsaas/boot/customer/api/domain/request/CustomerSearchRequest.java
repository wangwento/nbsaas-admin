package com.nbsaas.boot.customer.api.domain.request;

import com.nbsaas.boot.rest.filter.Operator;
import com.nbsaas.boot.rest.filter.Search;
import com.nbsaas.boot.rest.request.PageRequest;
import lombok.*;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
* 搜索bean
*/
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CustomerSearchRequest   extends PageRequest implements Serializable {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;


    @Search(name = "name",operator = Operator.like)
    private String name;

    @Search(name = "introducer",operator = Operator.like)
    private String introducer;


            /**
            * 关系分数
            **/
            @Search(name = "score",operator = Operator.eq)
            private Integer score;

            /**
            * 内容
            **/
            @Search(name = "note",operator = Operator.like)
            private String note;

            /**
            * 联系电话
            **/
            @Search(name = "phone",operator = Operator.like)
            private String phone;

            /**
            * 主键id
            **/
            @Search(name = "id",operator = Operator.eq)
            private Long id;



}