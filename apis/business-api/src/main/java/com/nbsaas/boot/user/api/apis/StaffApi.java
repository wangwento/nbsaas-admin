package com.nbsaas.boot.user.api.apis;

import com.nbsaas.boot.user.api.domain.request.StaffDataRequest;
import com.nbsaas.boot.user.api.domain.simple.StaffSimple;
import com.nbsaas.boot.user.api.domain.response.StaffResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface StaffApi extends BaseApi<StaffResponse, StaffSimple, StaffDataRequest> {


}
