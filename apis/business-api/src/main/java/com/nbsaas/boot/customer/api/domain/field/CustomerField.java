package com.nbsaas.boot.customer.api.domain.field;


/**
*   字段映射类
*/
public class CustomerField  {



    public static final String  beginDate = "beginDate";


    public static final String  score = "score";


    public static final String  note = "note";


    public static final String  introducer = "introducer";


    public static final String  phone = "phone";


    public static final String  name = "name";


    public static final String  id = "id";


    public static final String  addDate = "addDate";


    public static final String  lastDate = "lastDate";

}