package com.nbsaas.boot.scheduling.api.domain.field;


/**
*   字段映射类
*/
public class SchedulingTaskField  {



    public static final String  cronExpression = "cronExpression";


    public static final String  aliasName = "aliasName";


    public static final String  recordType = "recordType";


    public static final String  name = "name";


    public static final String  remark = "remark";


    public static final String  id = "id";


    public static final String  type = "type";


    public static final String  isSync = "isSync";


    public static final String  addDate = "addDate";


    public static final String  url = "url";


    public static final String  content = "content";


    public static final String  lastDate = "lastDate";

}