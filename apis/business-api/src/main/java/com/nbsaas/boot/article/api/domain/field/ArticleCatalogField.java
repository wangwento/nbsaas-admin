package com.nbsaas.boot.article.api.domain.field;


/**
*   字段映射类
*/
public class ArticleCatalogField  {



    public static final String  amount = "amount";


    public static final String  code = "code";


    public static final String  depth = "depth";


    public static final String  name = "name";


    public static final String  ids = "ids";


    public static final String  sortNum = "sortNum";


    public static final String  id = "id";


    public static final String  lft = "lft";


    public static final String  addDate = "addDate";


    public static final String  rgt = "rgt";


    public static final String  lastDate = "lastDate";

}