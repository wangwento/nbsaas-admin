package com.nbsaas.boot.article.api.apis;

import com.nbsaas.boot.article.api.domain.request.ArticleDocumentDataRequest;
import com.nbsaas.boot.article.api.domain.simple.ArticleDocumentSimple;
import com.nbsaas.boot.article.api.domain.response.ArticleDocumentResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface ArticleDocumentApi extends BaseApi<ArticleDocumentResponse, ArticleDocumentSimple, ArticleDocumentDataRequest> {


}
