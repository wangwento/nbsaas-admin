package com.nbsaas.boot.todo.api.apis;

import com.nbsaas.boot.todo.api.domain.request.TodoTemplateDataRequest;
import com.nbsaas.boot.todo.api.domain.simple.TodoTemplateSimple;
import com.nbsaas.boot.todo.api.domain.response.TodoTemplateResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface TodoTemplateApi extends BaseApi<TodoTemplateResponse, TodoTemplateSimple, TodoTemplateDataRequest> {


}
