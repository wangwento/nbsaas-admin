package com.nbsaas.boot.article.api.domain.response;

import lombok.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
            import com.nbsaas.boot.rest.enums.StoreState;
/**
* 响应对象
*/
@Getter
@Setter
@ToString(callSuper = true)
public class ArticleResponse  implements Serializable {
/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;


        /**
        * 
        **/
            private String note;

        /**
        * 
        **/
            private String images;

        /**
        * 
        **/
            private String img;

        /**
        * 
        **/
            private Integer comments;

        /**
        * 
        **/
            private String userAvatar;

        /**
        * 
        **/
            private String title;

        /**
        * 
        **/
            private String userName;

        /**
        * 添加时间
        **/
            private Date addDate;

        /**
        * 
        **/
            private String extData;

        /**
        * 
        **/
            private Integer ups;

        /**
        * 
        **/
            //枚举
            private StoreState storeState;

            private String storeStateName;

        /**
        * 主键id
        **/
            private Long id;

        /**
        * 
        **/
            private String introduction;

        /**
        * 
        **/
            private Integer views;

        /**
        * 
        **/
            private Integer likes;

        /**
        * 最新修改时间
        **/
            private Date lastDate;

}