package com.nbsaas.boot.article.api.apis;

import com.nbsaas.boot.article.api.domain.request.ArticleLikeDataRequest;
import com.nbsaas.boot.article.api.domain.simple.ArticleLikeSimple;
import com.nbsaas.boot.article.api.domain.response.ArticleLikeResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface ArticleLikeApi extends BaseApi<ArticleLikeResponse, ArticleLikeSimple, ArticleLikeDataRequest> {


}
