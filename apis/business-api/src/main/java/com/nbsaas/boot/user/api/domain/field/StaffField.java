package com.nbsaas.boot.user.api.domain.field;


/**
*   字段映射类
*/
public class StaffField  {



    public static final String  phone = "phone";


    public static final String  catalog = "catalog";


    public static final String  structureName = "structureName";


    public static final String  name = "name";


    public static final String  storeState = "storeState";


    public static final String  avatar = "avatar";


    public static final String  id = "id";


    public static final String  addDate = "addDate";


    public static final String  structure = "structure";


    public static final String  loginSize = "loginSize";


    public static final String  lastDate = "lastDate";

}