package com.nbsaas.boot.scheduling.api.domain.request;

import com.nbsaas.boot.rest.filter.Operator;
import com.nbsaas.boot.rest.filter.Search;
import com.nbsaas.boot.rest.request.PageRequest;
import lombok.*;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
* 搜索bean
*/
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SchedulingTaskSearchRequest   extends PageRequest implements Serializable {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;


    @Search(name = "name",operator = Operator.like)
    private String name;


            /**
            * 任务运行时间表达式
            **/
            @Search(name = "cronExpression",operator = Operator.like)
            private String cronExpression;

            /**
            * 任务别名
            **/
            @Search(name = "aliasName",operator = Operator.like)
            private String aliasName;

            /**
            * 记录类型 1日志记录，2日志不记录
            **/
            @Search(name = "recordType",operator = Operator.eq)
            private Integer recordType;

            /**
            * 任务备注
            **/
            @Search(name = "remark",operator = Operator.like)
            private String remark;

            /**
            * 主键id
            **/
            @Search(name = "id",operator = Operator.eq)
            private Long id;

            /**
            * 任务类型,1:url地址请求,2:执行指定命令操作(复制文件,下载文件）,3:打印文本类型
            **/
            @Search(name = "type",operator = Operator.eq)
            private Integer type;

            /**
            * 是否异步,1:是;2:否
            **/
            @Search(name = "isSync",operator = Operator.eq)
            private Integer isSync;

            /**
            * 任务执行url
            **/
            @Search(name = "url",operator = Operator.like)
            private String url;

            /**
            * 任务内容
            **/
            @Search(name = "content",operator = Operator.like)
            private String content;



}