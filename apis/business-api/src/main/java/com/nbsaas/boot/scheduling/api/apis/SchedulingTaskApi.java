package com.nbsaas.boot.scheduling.api.apis;

import com.nbsaas.boot.scheduling.api.domain.request.SchedulingTaskDataRequest;
import com.nbsaas.boot.scheduling.api.domain.simple.SchedulingTaskSimple;
import com.nbsaas.boot.scheduling.api.domain.response.SchedulingTaskResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface SchedulingTaskApi extends BaseApi<SchedulingTaskResponse, SchedulingTaskSimple, SchedulingTaskDataRequest> {


}
