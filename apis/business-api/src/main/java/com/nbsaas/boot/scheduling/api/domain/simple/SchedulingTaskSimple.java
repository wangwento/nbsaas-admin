package com.nbsaas.boot.scheduling.api.domain.simple;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import lombok.Data;

/**
* 列表对象
*/
@Data
public class SchedulingTaskSimple implements Serializable {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;




            /**
            * 任务运行时间表达式
            **/
                private String cronExpression;

            /**
            * 任务别名
            **/
                private String aliasName;

            /**
            * 记录类型 1日志记录，2日志不记录
            **/
                private Integer recordType;

            /**
            * 任务名称
            **/
                private String name;

            /**
            * 任务备注
            **/
                private String remark;

            /**
            * 主键id
            **/
                private Long id;

            /**
            * 任务类型,1:url地址请求,2:执行指定命令操作(复制文件,下载文件）,3:打印文本类型
            **/
                private Integer type;

            /**
            * 是否异步,1:是;2:否
            **/
                private Integer isSync;

            /**
            * 添加时间
            **/
                private Date addDate;

            /**
            * 任务执行url
            **/
                private String url;

            /**
            * 任务内容
            **/
                private String content;

            /**
            * 最新修改时间
            **/
                private Date lastDate;


}