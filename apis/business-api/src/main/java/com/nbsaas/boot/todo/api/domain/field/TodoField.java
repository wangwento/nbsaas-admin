package com.nbsaas.boot.todo.api.domain.field;


/**
*   字段映射类
*/
public class TodoField  {



    public static final String  dataKey = "dataKey";


    public static final String  note = "note";


    public static final String  handler = "handler";


    public static final String  ownerName = "ownerName";


    public static final String  dataId = "dataId";


    public static final String  navigateUrl = "navigateUrl";


    public static final String  name = "name";


    public static final String  handlerName = "handlerName";


    public static final String  state = "state";


    public static final String  id = "id";


    public static final String  addDate = "addDate";


    public static final String  lastDate = "lastDate";

}