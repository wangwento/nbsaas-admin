package com.nbsaas.boot.todo.api.domain.simple;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import lombok.Data;

/**
* 列表对象
*/
@Data
public class TodoSimple implements Serializable {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;



            /**
            * 
            **/
                private String dataKey;

            /**
            * 
            **/
                private String note;

            /**
            * 
            **/
                private Long handler;

            /**
            * 
            **/
                private String ownerName;

            /**
            * 
            **/
                private Long dataId;

            /**
            * 
            **/
                private String navigateUrl;

            /**
            * 
            **/
                private String name;

            /**
            * 
            **/
                private String handlerName;

            /**
            * 
            **/
                private Integer state;

            /**
            * 主键id
            **/
                private Long id;

            /**
            * 添加时间
            **/
                private Date addDate;

            /**
            * 最新修改时间
            **/
                private Date lastDate;


}