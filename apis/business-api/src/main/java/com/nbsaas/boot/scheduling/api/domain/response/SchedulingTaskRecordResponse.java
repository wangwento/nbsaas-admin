package com.nbsaas.boot.scheduling.api.domain.response;

import lombok.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
* 响应对象
*/
@Getter
@Setter
@ToString(callSuper = true)
public class SchedulingTaskRecordResponse  implements Serializable {
/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;


        /**
        * 任务执行完返回的结果
        **/
            private String note;

        /**
        * 任务id
        **/
            private Long schedulingTask;

        /**
        * 任务id
        **/
            private String schedulingTaskName;

        /**
        * 执行状态 1成功，2失败
        **/
            private Integer state;

        /**
        * 主键id
        **/
            private Long id;

        /**
        * 添加时间
        **/
            private Date addDate;

        /**
        * 最新修改时间
        **/
            private Date lastDate;

}