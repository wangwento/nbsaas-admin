package com.nbsaas.boot.todo.api.apis;

import com.nbsaas.boot.todo.api.domain.request.TodoDataRequest;
import com.nbsaas.boot.todo.api.domain.simple.TodoSimple;
import com.nbsaas.boot.todo.api.domain.response.TodoResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface TodoApi extends BaseApi<TodoResponse, TodoSimple, TodoDataRequest> {


}
