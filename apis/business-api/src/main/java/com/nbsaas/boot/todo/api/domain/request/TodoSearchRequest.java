package com.nbsaas.boot.todo.api.domain.request;

import com.nbsaas.boot.rest.filter.Operator;
import com.nbsaas.boot.rest.filter.Search;
import com.nbsaas.boot.rest.request.PageRequest;
import lombok.*;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
* 搜索bean
*/
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class TodoSearchRequest   extends PageRequest implements Serializable {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;



            /**
            * 
            **/
            @Search(name = "dataKey",operator = Operator.like)
            private String dataKey;

            /**
            * 
            **/
            @Search(name = "note",operator = Operator.like)
            private String note;

            /**
            * 
            **/
            @Search(name = "handler",operator = Operator.eq)
            private Long handler;

            /**
            * 
            **/
            @Search(name = "ownerName",operator = Operator.like)
            private String ownerName;

            /**
            * 
            **/
            @Search(name = "dataId",operator = Operator.eq)
            private Long dataId;

            /**
            * 
            **/
            @Search(name = "navigateUrl",operator = Operator.like)
            private String navigateUrl;

            /**
            * 
            **/
            @Search(name = "name",operator = Operator.like)
            private String name;

            /**
            * 
            **/
            @Search(name = "handlerName",operator = Operator.like)
            private String handlerName;

            /**
            * 
            **/
            @Search(name = "state",operator = Operator.eq)
            private Integer state;

            /**
            * 主键id
            **/
            @Search(name = "id",operator = Operator.eq)
            private Long id;



}