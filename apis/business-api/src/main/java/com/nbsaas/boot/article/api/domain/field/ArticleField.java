package com.nbsaas.boot.article.api.domain.field;


/**
*   字段映射类
*/
public class ArticleField  {



    public static final String  images = "images";


    public static final String  img = "img";


    public static final String  comments = "comments";


    public static final String  title = "title";


    public static final String  addDate = "addDate";


    public static final String  extData = "extData";


    public static final String  ups = "ups";


    public static final String  storeState = "storeState";


    public static final String  id = "id";


    public static final String  introduction = "introduction";


    public static final String  views = "views";


    public static final String  likes = "likes";


    public static final String  lastDate = "lastDate";

}