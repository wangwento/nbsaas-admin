package com.nbsaas.boot.scheduling.api.domain.request;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import lombok.Data;
import com.nbsaas.boot.rest.request.RequestId;
/**
* 请求对象
*/
@Data
public class SchedulingTaskRecordDataRequest implements Serializable,RequestId {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;



        /**
        * 任务执行完返回的结果
        **/
            private String note;

        /**
        * 任务id
        **/
            private Long schedulingTask;

        /**
        * 任务id
        **/
            //private String schedulingTaskNameName;

        /**
        * 执行状态 1成功，2失败
        **/
            private Integer state;

        /**
        * 主键id
        **/
            private Long id;

        /**
        * 添加时间
        **/
            private Date addDate;

        /**
        * 最新修改时间
        **/
            private Date lastDate;
}