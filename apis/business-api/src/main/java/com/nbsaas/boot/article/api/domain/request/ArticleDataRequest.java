package com.nbsaas.boot.article.api.domain.request;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import lombok.Data;
import com.nbsaas.boot.rest.request.RequestId;
            import com.nbsaas.boot.rest.enums.StoreState;
/**
* 请求对象
*/
@Data
public class ArticleDataRequest implements Serializable,RequestId {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;



        /**
        * 
        **/
            private String images;

        /**
        * 
        **/
            private String img;

        /**
        * 
        **/
            private Integer comments;

        /**
        * 
        **/
            private String title;

        /**
        * 添加时间
        **/
            private Date addDate;

        /**
        * 
        **/
            private String extData;

        /**
        * 
        **/
            private Integer ups;

        /**
        * 
        **/
            private StoreState storeState;

        /**
        * 主键id
        **/
            private Long id;

        /**
        * 
        **/
            private String introduction;

        /**
        * 
        **/
            private Integer views;

        /**
        * 
        **/
            private Integer likes;

        /**
        * 最新修改时间
        **/
            private Date lastDate;
}