package com.nbsaas.boot.scheduling.api.apis;

import com.nbsaas.boot.scheduling.api.domain.request.SchedulingTaskRecordDataRequest;
import com.nbsaas.boot.scheduling.api.domain.simple.SchedulingTaskRecordSimple;
import com.nbsaas.boot.scheduling.api.domain.response.SchedulingTaskRecordResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface SchedulingTaskRecordApi extends BaseApi<SchedulingTaskRecordResponse, SchedulingTaskRecordSimple, SchedulingTaskRecordDataRequest> {


}
