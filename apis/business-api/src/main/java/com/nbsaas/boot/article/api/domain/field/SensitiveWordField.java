package com.nbsaas.boot.article.api.domain.field;


/**
*   字段映射类
*/
public class SensitiveWordField  {



    public static final String  size = "size";


    public static final String  catalog = "catalog";


    public static final String  replace = "replace";


    public static final String  id = "id";


    public static final String  word = "word";


    public static final String  addDate = "addDate";


    public static final String  lastDate = "lastDate";

}