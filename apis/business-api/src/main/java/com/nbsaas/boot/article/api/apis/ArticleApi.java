package com.nbsaas.boot.article.api.apis;

import com.nbsaas.boot.article.api.domain.request.ArticleDataRequest;
import com.nbsaas.boot.article.api.domain.simple.ArticleSimple;
import com.nbsaas.boot.article.api.domain.response.ArticleResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface ArticleApi extends BaseApi<ArticleResponse, ArticleSimple, ArticleDataRequest> {


}
