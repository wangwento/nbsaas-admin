package com.nbsaas.boot.article.api.domain.response;

import lombok.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
* 响应对象
*/
@Getter
@Setter
@ToString(callSuper = true)
public class SensitiveWordResponse  implements Serializable {
/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;


        /**
        * 
        **/
            private Integer size;

        /**
        * 
        **/
            private Integer catalog;

        /**
        * 
        **/
            private String replace;

        /**
        * 主键id
        **/
            private Long id;

        /**
        * 
        **/
            private String word;

        /**
        * 添加时间
        **/
            private Date addDate;

        /**
        * 最新修改时间
        **/
            private Date lastDate;

}