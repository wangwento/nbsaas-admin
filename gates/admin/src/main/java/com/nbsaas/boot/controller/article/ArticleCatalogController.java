package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.article.api.domain.request.ArticleCatalogDataRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleCatalogSearchRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleCatalogResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleCatalogSimple;
import com.nbsaas.boot.article.api.apis.ArticleCatalogApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/articleCatalog")
public class ArticleCatalogController {


    @Resource
    private ArticleCatalogApi articleCatalogApi;


    @RequestMapping("/search")
    public PageResponse <ArticleCatalogSimple> search(ArticleCatalogSearchRequest request) {
        return articleCatalogApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<ArticleCatalogSimple> list(ArticleCatalogSearchRequest request) {
        return articleCatalogApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <ArticleCatalogResponse> create(@Validated(AddOperator.class) ArticleCatalogDataRequest request) {
        return articleCatalogApi.create(request);
    }

   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<ArticleCatalogResponse> update(@Validated(UpdateOperator.class) ArticleCatalogDataRequest request) {
       return articleCatalogApi.update(request);
   }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) ArticleCatalogDataRequest request) {
        return articleCatalogApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject <ArticleCatalogResponse> view(@Validated(ViewOperator.class) ArticleCatalogDataRequest  request) {
        return articleCatalogApi.view(request);
    }
}