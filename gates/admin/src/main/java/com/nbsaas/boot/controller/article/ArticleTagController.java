package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.article.api.domain.request.ArticleTagDataRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleTagSearchRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleTagResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleTagSimple;
import com.nbsaas.boot.article.api.apis.ArticleTagApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/articleTag")
public class ArticleTagController {


    @Resource
    private ArticleTagApi articleTagApi;


    @RequestMapping("/search")
    public PageResponse <ArticleTagSimple> search(ArticleTagSearchRequest request) {
        return articleTagApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<ArticleTagSimple> list(ArticleTagSearchRequest request) {
        return articleTagApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <ArticleTagResponse> create(@Validated(AddOperator.class) ArticleTagDataRequest request) {
        return articleTagApi.create(request);
    }

   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<ArticleTagResponse> update(@Validated(UpdateOperator.class) ArticleTagDataRequest request) {
       return articleTagApi.update(request);
   }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) ArticleTagDataRequest request) {
        return articleTagApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject <ArticleTagResponse> view(@Validated(ViewOperator.class) ArticleTagDataRequest  request) {
        return articleTagApi.view(request);
    }
}