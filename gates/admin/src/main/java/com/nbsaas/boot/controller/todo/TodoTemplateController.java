package com.nbsaas.boot.controller.todo;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.todo.api.domain.request.TodoTemplateDataRequest;
import com.nbsaas.boot.todo.api.domain.request.TodoTemplateSearchRequest;
import com.nbsaas.boot.todo.api.domain.response.TodoTemplateResponse;
import com.nbsaas.boot.todo.api.domain.simple.TodoTemplateSimple;
import com.nbsaas.boot.todo.api.apis.TodoTemplateApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/todoTemplate")
public class TodoTemplateController {


    @Resource
    private TodoTemplateApi todoTemplateApi;


    @RequestMapping("/search")
    public PageResponse <TodoTemplateSimple> search(TodoTemplateSearchRequest request) {
        return todoTemplateApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<TodoTemplateSimple> list(TodoTemplateSearchRequest request) {
        return todoTemplateApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <TodoTemplateResponse> create(@Validated(AddOperator.class) TodoTemplateDataRequest request) {
        return todoTemplateApi.create(request);
    }

   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<TodoTemplateResponse> update(@Validated(UpdateOperator.class) TodoTemplateDataRequest request) {
       return todoTemplateApi.update(request);
   }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) TodoTemplateDataRequest request) {
        return todoTemplateApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject <TodoTemplateResponse> view(@Validated(ViewOperator.class) TodoTemplateDataRequest  request) {
        return todoTemplateApi.view(request);
    }
}