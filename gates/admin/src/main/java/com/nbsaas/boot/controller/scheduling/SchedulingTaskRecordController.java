package com.nbsaas.boot.controller.scheduling;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.scheduling.api.domain.request.SchedulingTaskRecordDataRequest;
import com.nbsaas.boot.scheduling.api.domain.request.SchedulingTaskRecordSearchRequest;
import com.nbsaas.boot.scheduling.api.domain.response.SchedulingTaskRecordResponse;
import com.nbsaas.boot.scheduling.api.domain.simple.SchedulingTaskRecordSimple;
import com.nbsaas.boot.scheduling.api.apis.SchedulingTaskRecordApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/schedulingTaskRecord")
public class SchedulingTaskRecordController {


    @Resource
    private SchedulingTaskRecordApi schedulingTaskRecordApi;


    @RequiresPermissions("schedulingTaskRecord")
    @RequestMapping("/search")
    public PageResponse <SchedulingTaskRecordSimple> search(SchedulingTaskRecordSearchRequest request) {
        return schedulingTaskRecordApi.search(request);
    }

    @RequiresPermissions("schedulingTaskRecord")
    @RequestMapping("/list")
    public ListResponse<SchedulingTaskRecordSimple> list(SchedulingTaskRecordSearchRequest request) {
        return schedulingTaskRecordApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @RequiresPermissions("schedulingTaskRecord")
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <SchedulingTaskRecordResponse> create(@Validated(AddOperator.class) SchedulingTaskRecordDataRequest request) {
        return schedulingTaskRecordApi.create(request);
    }

   @RequiresPermissions("schedulingTaskRecord")
   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<SchedulingTaskRecordResponse> update(@Validated(UpdateOperator.class) SchedulingTaskRecordDataRequest request) {
       return schedulingTaskRecordApi.update(request);
   }

    @RequiresPermissions("schedulingTaskRecord")
    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) SchedulingTaskRecordDataRequest request) {
        return schedulingTaskRecordApi.delete(request);
    }

    @RequiresPermissions("schedulingTaskRecord")
    @RequestMapping("/view")
    public ResponseObject <SchedulingTaskRecordResponse> view(@Validated(ViewOperator.class) SchedulingTaskRecordDataRequest  request) {
        return schedulingTaskRecordApi.view(request);
    }
}