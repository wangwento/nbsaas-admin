package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.article.api.domain.request.SensitiveCategoryDataRequest;
import com.nbsaas.boot.article.api.domain.request.SensitiveCategorySearchRequest;
import com.nbsaas.boot.article.api.domain.response.SensitiveCategoryResponse;
import com.nbsaas.boot.article.api.domain.simple.SensitiveCategorySimple;
import com.nbsaas.boot.article.api.apis.SensitiveCategoryApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/sensitiveCategory")
public class SensitiveCategoryController {


    @Resource
    private SensitiveCategoryApi sensitiveCategoryApi;


    @RequestMapping("/search")
    public PageResponse <SensitiveCategorySimple> search(SensitiveCategorySearchRequest request) {
        return sensitiveCategoryApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<SensitiveCategorySimple> list(SensitiveCategorySearchRequest request) {
        return sensitiveCategoryApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <SensitiveCategoryResponse> create(@Validated(AddOperator.class) SensitiveCategoryDataRequest request) {
        return sensitiveCategoryApi.create(request);
    }

   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<SensitiveCategoryResponse> update(@Validated(UpdateOperator.class) SensitiveCategoryDataRequest request) {
       return sensitiveCategoryApi.update(request);
   }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) SensitiveCategoryDataRequest request) {
        return sensitiveCategoryApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject <SensitiveCategoryResponse> view(@Validated(ViewOperator.class) SensitiveCategoryDataRequest  request) {
        return sensitiveCategoryApi.view(request);
    }
}