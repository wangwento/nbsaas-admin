package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.article.api.domain.request.ArticleDocumentDataRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleDocumentSearchRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleDocumentResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleDocumentSimple;
import com.nbsaas.boot.article.api.apis.ArticleDocumentApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/articleDocument")
public class ArticleDocumentController {


    @Resource
    private ArticleDocumentApi articleDocumentApi;


    @RequestMapping("/search")
    public PageResponse <ArticleDocumentSimple> search(ArticleDocumentSearchRequest request) {
        return articleDocumentApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<ArticleDocumentSimple> list(ArticleDocumentSearchRequest request) {
        return articleDocumentApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <ArticleDocumentResponse> create(@Validated(AddOperator.class) ArticleDocumentDataRequest request) {
        return articleDocumentApi.create(request);
    }

   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<ArticleDocumentResponse> update(@Validated(UpdateOperator.class) ArticleDocumentDataRequest request) {
       return articleDocumentApi.update(request);
   }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) ArticleDocumentDataRequest request) {
        return articleDocumentApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject <ArticleDocumentResponse> view(@Validated(ViewOperator.class) ArticleDocumentDataRequest  request) {
        return articleDocumentApi.view(request);
    }
}