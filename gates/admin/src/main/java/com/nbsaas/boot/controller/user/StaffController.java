package com.nbsaas.boot.controller.user;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.user.api.domain.request.StaffDataRequest;
import com.nbsaas.boot.user.api.domain.request.StaffSearchRequest;
import com.nbsaas.boot.user.api.domain.response.StaffResponse;
import com.nbsaas.boot.user.api.domain.simple.StaffSimple;
import com.nbsaas.boot.user.api.apis.StaffApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/staff")
public class StaffController {


    @Resource
    private StaffApi staffApi;


    @RequestMapping("/search")
    public PageResponse <StaffSimple> search(StaffSearchRequest request) {
        return staffApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<StaffSimple> list(StaffSearchRequest request) {
        return staffApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <StaffResponse> create(@Validated(AddOperator.class) StaffDataRequest request) {
        return staffApi.create(request);
    }

   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<StaffResponse> update(@Validated(UpdateOperator.class) StaffDataRequest request) {
       return staffApi.update(request);
   }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) StaffDataRequest request) {
        return staffApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject <StaffResponse> view(@Validated(ViewOperator.class) StaffDataRequest  request) {
        return staffApi.view(request);
    }
}