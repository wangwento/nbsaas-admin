package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.article.api.domain.request.ArticleDataRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleSearchRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleSimple;
import com.nbsaas.boot.article.api.apis.ArticleApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/article")
public class ArticleController {


    @Resource
    private ArticleApi articleApi;


    @RequestMapping("/search")
    public PageResponse <ArticleSimple> search(ArticleSearchRequest request) {
        return articleApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<ArticleSimple> list(ArticleSearchRequest request) {
        return articleApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <ArticleResponse> create(@Validated(AddOperator.class) ArticleDataRequest request) {
        return articleApi.create(request);
    }

   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<ArticleResponse> update(@Validated(UpdateOperator.class) ArticleDataRequest request) {
       return articleApi.update(request);
   }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) ArticleDataRequest request) {
        return articleApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject <ArticleResponse> view(@Validated(ViewOperator.class) ArticleDataRequest  request) {
        return articleApi.view(request);
    }
}