package com.nbsaas.boot;

import com.nbsaas.boot.event.BootSuccessEvent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.annotation.Resource;

/**
 * 排除jpa注解，使用原生hibernate
 * (exclude= HibernateJpaAutoConfiguration.class)
 */
@EnableTransactionManagement
@EnableCaching
@CrossOrigin
@SpringBootApplication
@EnableDiscoveryClient
public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }
    @Component
    public static class ApplicationStartup implements ApplicationListener<ContextRefreshedEvent> {

        @Resource
        private ApplicationEventPublisher applicationEventPublisher;

        @Override
        public void onApplicationEvent(ContextRefreshedEvent event) {
            // 在应用程序启动成功后执行的代码
            System.out.println("应用程序启动成功！");
            applicationEventPublisher.publishEvent(new BootSuccessEvent());
        }
    }

}
