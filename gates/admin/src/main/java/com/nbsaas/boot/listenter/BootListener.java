package com.nbsaas.boot.listenter;

import com.nbsaas.boot.controller.scheduling.TaskController;
import com.nbsaas.boot.event.BootSuccessEvent;
import org.quartz.SchedulerException;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@EnableAsync
public class BootListener {


    @Resource
    private TaskController taskController;

    @Async
    @EventListener(BootSuccessEvent.class)
    public void notifyAdd(BootSuccessEvent event) {
        try {
            taskController.loadTasks();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

    }
}
