package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.article.api.domain.request.SensitiveWordDataRequest;
import com.nbsaas.boot.article.api.domain.request.SensitiveWordSearchRequest;
import com.nbsaas.boot.article.api.domain.response.SensitiveWordResponse;
import com.nbsaas.boot.article.api.domain.simple.SensitiveWordSimple;
import com.nbsaas.boot.article.api.apis.SensitiveWordApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/sensitiveWord")
public class SensitiveWordController {


    @Resource
    private SensitiveWordApi sensitiveWordApi;


    @RequestMapping("/search")
    public PageResponse <SensitiveWordSimple> search(SensitiveWordSearchRequest request) {
        return sensitiveWordApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<SensitiveWordSimple> list(SensitiveWordSearchRequest request) {
        return sensitiveWordApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <SensitiveWordResponse> create(@Validated(AddOperator.class) SensitiveWordDataRequest request) {
        return sensitiveWordApi.create(request);
    }

   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<SensitiveWordResponse> update(@Validated(UpdateOperator.class) SensitiveWordDataRequest request) {
       return sensitiveWordApi.update(request);
   }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) SensitiveWordDataRequest request) {
        return sensitiveWordApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject <SensitiveWordResponse> view(@Validated(ViewOperator.class) SensitiveWordDataRequest  request) {
        return sensitiveWordApi.view(request);
    }
}