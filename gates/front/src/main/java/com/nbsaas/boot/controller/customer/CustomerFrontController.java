package com.nbsaas.boot.controller.customer;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.customer.api.domain.request.CustomerDataRequest;
import com.nbsaas.boot.customer.api.domain.request.CustomerSearchRequest;
import com.nbsaas.boot.customer.api.domain.response.CustomerResponse;
import com.nbsaas.boot.customer.api.domain.simple.CustomerSimple;
import com.nbsaas.boot.customer.api.apis.CustomerApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  前端控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/customer")
public class CustomerFrontController {


    @Resource
    private CustomerApi customerApi;


    @RequestMapping("/search")
    public PageResponse<CustomerSimple> search(CustomerSearchRequest request) {
        return customerApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<CustomerSimple> list(CustomerSearchRequest request) {
        return customerApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject<CustomerResponse> create(@Validated(AddOperator.class) CustomerDataRequest request) {
        return customerApi.create(request);
    }

    @UpdateData
    @RequestMapping("/update")
    public ResponseObject <CustomerResponse> update(@Validated(UpdateOperator.class) CustomerDataRequest request) {
        return customerApi.update(request);
    }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) CustomerDataRequest request) {
     return customerApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject<CustomerResponse> view(@Validated(ViewOperator.class) CustomerDataRequest request) {
        return customerApi.view(request);
    }
}