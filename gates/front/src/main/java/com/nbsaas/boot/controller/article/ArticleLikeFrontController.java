package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.article.api.domain.request.ArticleLikeDataRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleLikeSearchRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleLikeResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleLikeSimple;
import com.nbsaas.boot.article.api.apis.ArticleLikeApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  前端控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/articleLike")
public class ArticleLikeFrontController {


    @Resource
    private ArticleLikeApi articleLikeApi;


    @RequestMapping("/search")
    public PageResponse<ArticleLikeSimple> search(ArticleLikeSearchRequest request) {
        return articleLikeApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<ArticleLikeSimple> list(ArticleLikeSearchRequest request) {
        return articleLikeApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject<ArticleLikeResponse> create(@Validated(AddOperator.class) ArticleLikeDataRequest request) {
        return articleLikeApi.create(request);
    }

    @UpdateData
    @RequestMapping("/update")
    public ResponseObject <ArticleLikeResponse> update(@Validated(UpdateOperator.class) ArticleLikeDataRequest request) {
        return articleLikeApi.update(request);
    }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) ArticleLikeDataRequest request) {
     return articleLikeApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject<ArticleLikeResponse> view(@Validated(ViewOperator.class) ArticleLikeDataRequest request) {
        return articleLikeApi.view(request);
    }
}