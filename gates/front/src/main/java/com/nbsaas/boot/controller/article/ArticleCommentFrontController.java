package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.article.api.domain.request.ArticleCommentDataRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleCommentSearchRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleCommentResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleCommentSimple;
import com.nbsaas.boot.article.api.apis.ArticleCommentApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  前端控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/articleComment")
public class ArticleCommentFrontController {


    @Resource
    private ArticleCommentApi articleCommentApi;


    @RequestMapping("/search")
    public PageResponse<ArticleCommentSimple> search(ArticleCommentSearchRequest request) {
        return articleCommentApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<ArticleCommentSimple> list(ArticleCommentSearchRequest request) {
        return articleCommentApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject<ArticleCommentResponse> create(@Validated(AddOperator.class) ArticleCommentDataRequest request) {
        return articleCommentApi.create(request);
    }

    @UpdateData
    @RequestMapping("/update")
    public ResponseObject <ArticleCommentResponse> update(@Validated(UpdateOperator.class) ArticleCommentDataRequest request) {
        return articleCommentApi.update(request);
    }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) ArticleCommentDataRequest request) {
     return articleCommentApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject<ArticleCommentResponse> view(@Validated(ViewOperator.class) ArticleCommentDataRequest request) {
        return articleCommentApi.view(request);
    }
}