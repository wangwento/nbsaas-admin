package com.nbsaas.boot.controller.todo;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.todo.api.domain.request.TodoDataRequest;
import com.nbsaas.boot.todo.api.domain.request.TodoSearchRequest;
import com.nbsaas.boot.todo.api.domain.response.TodoResponse;
import com.nbsaas.boot.todo.api.domain.simple.TodoSimple;
import com.nbsaas.boot.todo.api.apis.TodoApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  前端控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/todo")
public class TodoFrontController {


    @Resource
    private TodoApi todoApi;


    @RequestMapping("/search")
    public PageResponse<TodoSimple> search(TodoSearchRequest request) {
        return todoApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<TodoSimple> list(TodoSearchRequest request) {
        return todoApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject<TodoResponse> create(@Validated(AddOperator.class) TodoDataRequest request) {
        return todoApi.create(request);
    }

    @UpdateData
    @RequestMapping("/update")
    public ResponseObject <TodoResponse> update(@Validated(UpdateOperator.class) TodoDataRequest request) {
        return todoApi.update(request);
    }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) TodoDataRequest request) {
     return todoApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject<TodoResponse> view(@Validated(ViewOperator.class) TodoDataRequest request) {
        return todoApi.view(request);
    }
}