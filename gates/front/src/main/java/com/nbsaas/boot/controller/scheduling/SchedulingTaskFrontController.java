package com.nbsaas.boot.controller.scheduling;

import com.nbsaas.boot.rest.annotations.AddOperator;
import com.nbsaas.boot.rest.annotations.DeleteOperator;
import com.nbsaas.boot.rest.annotations.UpdateOperator;
import com.nbsaas.boot.rest.annotations.ViewOperator;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.rest.annotations.CreateData;
import com.nbsaas.boot.rest.annotations.UpdateData;
import com.nbsaas.boot.scheduling.api.domain.request.SchedulingTaskDataRequest;
import com.nbsaas.boot.scheduling.api.domain.request.SchedulingTaskSearchRequest;
import com.nbsaas.boot.scheduling.api.domain.response.SchedulingTaskResponse;
import com.nbsaas.boot.scheduling.api.domain.simple.SchedulingTaskSimple;
import com.nbsaas.boot.scheduling.api.apis.SchedulingTaskApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
*  前端控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/schedulingTask")
public class SchedulingTaskFrontController {


    @Resource
    private SchedulingTaskApi schedulingTaskApi;


    @RequestMapping("/search")
    public PageResponse<SchedulingTaskSimple> search(SchedulingTaskSearchRequest request) {
        return schedulingTaskApi.search(request);
    }

    @RequestMapping("/list")
    public ListResponse<SchedulingTaskSimple> list(SchedulingTaskSearchRequest request) {
        return schedulingTaskApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @CreateData
    @RequestMapping("/create")
    public ResponseObject<SchedulingTaskResponse> create(@Validated(AddOperator.class) SchedulingTaskDataRequest request) {
        return schedulingTaskApi.create(request);
    }

    @UpdateData
    @RequestMapping("/update")
    public ResponseObject <SchedulingTaskResponse> update(@Validated(UpdateOperator.class) SchedulingTaskDataRequest request) {
        return schedulingTaskApi.update(request);
    }

    @RequestMapping("/delete")
    public ResponseObject<?> delete(@Validated(DeleteOperator.class) SchedulingTaskDataRequest request) {
     return schedulingTaskApi.delete(request);
    }

    @RequestMapping("/view")
    public ResponseObject<SchedulingTaskResponse> view(@Validated(ViewOperator.class) SchedulingTaskDataRequest request) {
        return schedulingTaskApi.view(request);
    }
}