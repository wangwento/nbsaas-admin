package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.Article;
import com.nbsaas.boot.article.api.domain.request.ArticleDataRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class ArticleEntityConvert  implements Converter<Article, ArticleDataRequest> {

    @Override
    public Article convert(ArticleDataRequest source) {
        Article result = new Article();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

