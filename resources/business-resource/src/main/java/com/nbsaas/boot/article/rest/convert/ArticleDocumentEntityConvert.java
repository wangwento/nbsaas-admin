package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleDocument;
import com.nbsaas.boot.article.api.domain.request.ArticleDocumentDataRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class ArticleDocumentEntityConvert  implements Converter<ArticleDocument, ArticleDocumentDataRequest> {

    @Override
    public ArticleDocument convert(ArticleDocumentDataRequest source) {
        ArticleDocument result = new ArticleDocument();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

