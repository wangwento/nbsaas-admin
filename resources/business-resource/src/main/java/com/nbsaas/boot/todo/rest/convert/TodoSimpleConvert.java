package com.nbsaas.boot.todo.rest.convert;

import com.nbsaas.boot.todo.data.entity.Todo;
import com.nbsaas.boot.todo.api.domain.simple.TodoSimple;

import com.nbsaas.boot.rest.api.Converter;
/**
* 列表对象转换器
*/

public class TodoSimpleConvert implements Converter<TodoSimple, Todo> {




@Override
public TodoSimple convert(Todo source) {
    TodoSimple result = new TodoSimple();

                result.setDataKey(source.getDataKey());
                result.setNote(source.getNote());
                result.setHandler(source.getHandler());
                result.setOwnerName(source.getOwnerName());
                result.setDataId(source.getDataId());
                result.setNavigateUrl(source.getNavigateUrl());
                result.setName(source.getName());
                result.setHandlerName(source.getHandlerName());
                result.setState(source.getState());
                result.setId(source.getId());
                result.setAddDate(source.getAddDate());
                result.setLastDate(source.getLastDate());


    return result;
}

}