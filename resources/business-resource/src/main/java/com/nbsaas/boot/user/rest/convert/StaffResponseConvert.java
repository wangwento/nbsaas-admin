package com.nbsaas.boot.user.rest.convert;

import com.nbsaas.boot.user.data.entity.Staff;
import com.nbsaas.boot.user.api.domain.response.StaffResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;

/**
* 实体对象转化成响应对象
*/

public class StaffResponseConvert  implements Converter<StaffResponse,Staff> {

    @Override
    public StaffResponse convert(Staff source) {
        StaffResponse  result = new  StaffResponse();
        BeanDataUtils.copyProperties(source, result);
                    if(source.getStructure()!=null){
                        result.setStructureName(source.getStructure().getName());
                    }
                    if(source.getStoreState()!=null){
                        result.setStoreStateName(String.valueOf(source.getStoreState()));
                    }
                    if(source.getStructure()!=null){
                        result.setStructure(source.getStructure().getId());
                    }
        return result;
    }

}

