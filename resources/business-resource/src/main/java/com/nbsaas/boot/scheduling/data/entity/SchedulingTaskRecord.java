package com.nbsaas.boot.scheduling.data.entity;

import com.nbsaas.boot.code.annotation.FieldConvert;
import com.nbsaas.boot.code.annotation.FieldName;
import com.nbsaas.boot.jpa.data.entity.AbstractEntity;
import lombok.Data;
import org.hibernate.annotations.Comment;

import javax.persistence.*;


@org.hibernate.annotations.Table(appliesTo = "scheduling_task_record", comment = "定时任务记录")
@Data
@Entity
@Table(name = "scheduling_task_record")
public class SchedulingTaskRecord extends AbstractEntity {


    @Comment("任务id")
    @JoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private SchedulingTask schedulingTask;


    @Comment("执行状态 1成功，2失败")
    private Integer state;

    @Comment("任务执行完返回的结果")
    private String note;


}