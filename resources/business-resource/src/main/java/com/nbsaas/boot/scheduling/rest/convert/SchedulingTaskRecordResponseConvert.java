package com.nbsaas.boot.scheduling.rest.convert;

import com.nbsaas.boot.scheduling.data.entity.SchedulingTaskRecord;
import com.nbsaas.boot.scheduling.api.domain.response.SchedulingTaskRecordResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;

/**
* 实体对象转化成响应对象
*/

public class SchedulingTaskRecordResponseConvert  implements Converter<SchedulingTaskRecordResponse,SchedulingTaskRecord> {

    @Override
    public SchedulingTaskRecordResponse convert(SchedulingTaskRecord source) {
        SchedulingTaskRecordResponse  result = new  SchedulingTaskRecordResponse();
        BeanDataUtils.copyProperties(source, result);
                    if(source.getSchedulingTask()!=null){
                        result.setSchedulingTask(source.getSchedulingTask().getId());
                    }
                    if(source.getSchedulingTask()!=null){
                        result.setSchedulingTaskName(source.getSchedulingTask().getName());
                    }
        return result;
    }

}

