package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleLike;
import com.nbsaas.boot.article.api.domain.request.ArticleLikeDataRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;
            import com.nbsaas.boot.user.data.entity.Staff;
            import com.nbsaas.boot.article.data.entity.Article;

/**
* 请求对象转换成实体对象
*/

public class ArticleLikeEntityConvert  implements Converter<ArticleLike, ArticleLikeDataRequest> {

    @Override
    public ArticleLike convert(ArticleLikeDataRequest source) {
        ArticleLike result = new ArticleLike();
        BeanDataUtils.copyProperties(source, result);
                    if(source.getUser()!=null){
                    Staff user =new Staff();
                    user.setId(source.getUser());
                    result.setUser(user);
                    }
                    if(source.getArticle()!=null){
                    Article article =new Article();
                    article.setId(source.getArticle());
                    result.setArticle(article);
                    }
        return result;
    }
}

