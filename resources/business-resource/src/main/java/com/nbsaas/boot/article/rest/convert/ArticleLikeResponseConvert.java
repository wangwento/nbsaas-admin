package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleLike;
import com.nbsaas.boot.article.api.domain.response.ArticleLikeResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;

/**
* 实体对象转化成响应对象
*/

public class ArticleLikeResponseConvert  implements Converter<ArticleLikeResponse,ArticleLike> {

    @Override
    public ArticleLikeResponse convert(ArticleLike source) {
        ArticleLikeResponse  result = new  ArticleLikeResponse();
        BeanDataUtils.copyProperties(source, result);
                    if(source.getUser()!=null){
                        result.setUser(source.getUser().getId());
                    }
                    if(source.getArticle()!=null){
                        result.setArticle(source.getArticle().getId());
                    }
        return result;
    }

}

