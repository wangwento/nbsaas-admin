package com.nbsaas.boot.scheduling.rest.convert;

import com.nbsaas.boot.scheduling.data.entity.SchedulingTask;
import com.nbsaas.boot.scheduling.api.domain.request.SchedulingTaskDataRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class SchedulingTaskEntityConvert  implements Converter<SchedulingTask, SchedulingTaskDataRequest> {

    @Override
    public SchedulingTask convert(SchedulingTaskDataRequest source) {
        SchedulingTask result = new SchedulingTask();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

