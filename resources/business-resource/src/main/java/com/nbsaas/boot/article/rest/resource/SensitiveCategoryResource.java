package com.nbsaas.boot.article.rest.resource;

import com.nbsaas.boot.rest.request.PageRequest;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.article.api.apis.SensitiveCategoryApi;
import com.nbsaas.boot.article.data.entity.SensitiveCategory;
import com.nbsaas.boot.article.api.domain.request.SensitiveCategoryDataRequest;
import com.nbsaas.boot.article.api.domain.request.SensitiveCategorySearchRequest;
import com.nbsaas.boot.article.api.domain.response.SensitiveCategoryResponse;
import com.nbsaas.boot.article.api.domain.simple.SensitiveCategorySimple;
import com.nbsaas.boot.article.rest.convert.SensitiveCategorySimpleConvert;
import com.nbsaas.boot.article.rest.convert.SensitiveCategoryEntityConvert;
import com.nbsaas.boot.article.rest.convert.SensitiveCategoryResponseConvert;
import com.nbsaas.boot.article.data.repository.SensitiveCategoryRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
*   业务接口实现
*/
@Transactional
@Service
public class SensitiveCategoryResource extends BaseResource<SensitiveCategory,SensitiveCategoryResponse, SensitiveCategorySimple, SensitiveCategoryDataRequest>  implements SensitiveCategoryApi {

    @Resource
    private SensitiveCategoryRepository sensitiveCategoryRepository;

    @Override
    public JpaRepositoryImplementation<SensitiveCategory, Serializable> getJpaRepository() {
        return sensitiveCategoryRepository;
    }

    @Override
    public Function<SensitiveCategory, SensitiveCategorySimple> getConvertSimple() {
        return new SensitiveCategorySimpleConvert();
    }

    @Override
    public Function<SensitiveCategoryDataRequest, SensitiveCategory> getConvertForm() {
        return new SensitiveCategoryEntityConvert();
    }

    @Override
    public Function<SensitiveCategory, SensitiveCategoryResponse> getConvertResponse() {
    return new SensitiveCategoryResponseConvert();
    }




}


