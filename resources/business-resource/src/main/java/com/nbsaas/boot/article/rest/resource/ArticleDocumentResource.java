package com.nbsaas.boot.article.rest.resource;

import com.nbsaas.boot.rest.request.PageRequest;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.article.api.apis.ArticleDocumentApi;
import com.nbsaas.boot.article.data.entity.ArticleDocument;
import com.nbsaas.boot.article.api.domain.request.ArticleDocumentDataRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleDocumentSearchRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleDocumentResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleDocumentSimple;
import com.nbsaas.boot.article.rest.convert.ArticleDocumentSimpleConvert;
import com.nbsaas.boot.article.rest.convert.ArticleDocumentEntityConvert;
import com.nbsaas.boot.article.rest.convert.ArticleDocumentResponseConvert;
import com.nbsaas.boot.article.data.repository.ArticleDocumentRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
*   业务接口实现
*/
@Transactional
@Service
public class ArticleDocumentResource extends BaseResource<ArticleDocument,ArticleDocumentResponse, ArticleDocumentSimple, ArticleDocumentDataRequest>  implements ArticleDocumentApi {

    @Resource
    private ArticleDocumentRepository articleDocumentRepository;

    @Override
    public JpaRepositoryImplementation<ArticleDocument, Serializable> getJpaRepository() {
        return articleDocumentRepository;
    }

    @Override
    public Function<ArticleDocument, ArticleDocumentSimple> getConvertSimple() {
        return new ArticleDocumentSimpleConvert();
    }

    @Override
    public Function<ArticleDocumentDataRequest, ArticleDocument> getConvertForm() {
        return new ArticleDocumentEntityConvert();
    }

    @Override
    public Function<ArticleDocument, ArticleDocumentResponse> getConvertResponse() {
    return new ArticleDocumentResponseConvert();
    }




}


