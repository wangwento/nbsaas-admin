package com.nbsaas.boot.article.data.repository;

import com.nbsaas.boot.article.data.entity.ArticleLike;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface ArticleLikeRepository  extends  JpaRepositoryImplementation<ArticleLike, Serializable>{

}