package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleComment;
import com.nbsaas.boot.article.api.domain.request.ArticleCommentDataRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class ArticleCommentEntityConvert  implements Converter<ArticleComment, ArticleCommentDataRequest> {

    @Override
    public ArticleComment convert(ArticleCommentDataRequest source) {
        ArticleComment result = new ArticleComment();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

