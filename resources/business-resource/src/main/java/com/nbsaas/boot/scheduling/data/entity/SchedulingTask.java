package com.nbsaas.boot.scheduling.data.entity;

import com.nbsaas.boot.code.annotation.*;
import com.nbsaas.boot.jpa.data.entity.AbstractEntity;
import lombok.Data;
import org.hibernate.annotations.Comment;

import javax.persistence.Entity;
import javax.persistence.Table;


@ComposeView
@FormAnnotation(title = "定时任务",model = "定时任务")
@org.hibernate.annotations.Table(appliesTo = "scheduling_task", comment = "任务计划任务记录表")
@Data
@Entity
@Table(name = "scheduling_task")
public class SchedulingTask extends AbstractEntity {



    @SearchItem(label = "任务名称",name = "name")
    @FormField(title = "任务名称",grid = true,required = true)
    @Comment("任务名称")
    private String name;

    @FormField(title = "任务别名",grid = true)
    @Comment("任务别名")
    private String aliasName;


    @FormField(title = "记录类型",grid = true)
    @Comment("记录类型 1日志记录，2日志不记录")
    private Integer recordType;

    @FormField(title = "cron表达式",grid = true,required = true)
    @Comment("任务运行时间表达式")
    private String cronExpression;

    @FormField(title = "是否异步",grid = true)
    @Comment("是否异步,1:是;2:否")
    private Integer isSync;

    @Comment("任务类型,1:url地址请求,2:执行指定命令操作(复制文件,下载文件）,3:打印文本类型")
    private Integer type;

    @FormField(title = "任务url",grid = true,col = 24,required = true,width = "280")
    @Comment("任务执行url")
    private String url;

    @FormField(title = "任务备注",grid = true,col = 24,type = InputType.textarea,width = "10000")
    @Comment("任务备注")
    private String remark;

    @FormField(title = "任务内容",col = 24,type = InputType.textarea)
    @Comment("任务内容")
    private String content;


}