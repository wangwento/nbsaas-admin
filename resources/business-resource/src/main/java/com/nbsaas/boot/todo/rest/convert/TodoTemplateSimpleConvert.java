package com.nbsaas.boot.todo.rest.convert;

import com.nbsaas.boot.todo.data.entity.TodoTemplate;
import com.nbsaas.boot.todo.api.domain.simple.TodoTemplateSimple;

import com.nbsaas.boot.rest.api.Converter;
/**
* 列表对象转换器
*/

public class TodoTemplateSimpleConvert implements Converter<TodoTemplateSimple, TodoTemplate> {




@Override
public TodoTemplateSimple convert(TodoTemplate source) {
    TodoTemplateSimple result = new TodoTemplateSimple();

                result.setDataKey(source.getDataKey());
                result.setExtData(source.getExtData());
                result.setNavigateUrl(source.getNavigateUrl());
                result.setName(source.getName());
                result.setId(source.getId());
                result.setAddDate(source.getAddDate());
                result.setLastDate(source.getLastDate());


    return result;
}

}