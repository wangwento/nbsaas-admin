package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleComment;
import com.nbsaas.boot.article.api.domain.response.ArticleCommentResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;

/**
* 实体对象转化成响应对象
*/

public class ArticleCommentResponseConvert  implements Converter<ArticleCommentResponse,ArticleComment> {

    @Override
    public ArticleCommentResponse convert(ArticleComment source) {
        ArticleCommentResponse  result = new  ArticleCommentResponse();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }

}

