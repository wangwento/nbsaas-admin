package com.nbsaas.boot.todo.data.repository;

import com.nbsaas.boot.todo.data.entity.TodoTemplate;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface TodoTemplateRepository  extends  JpaRepositoryImplementation<TodoTemplate, Serializable>{

}