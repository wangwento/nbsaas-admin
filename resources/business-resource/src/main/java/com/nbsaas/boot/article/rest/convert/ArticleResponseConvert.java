package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.Article;
import com.nbsaas.boot.article.api.domain.response.ArticleResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;

/**
* 实体对象转化成响应对象
*/

public class ArticleResponseConvert  implements Converter<ArticleResponse,Article> {

    @Override
    public ArticleResponse convert(Article source) {
        ArticleResponse  result = new  ArticleResponse();
        BeanDataUtils.copyProperties(source, result);
                    if(source.getDocument()!=null){
                    result.setNote(source.getDocument().getNote());
                    }
                    if(source.getUser()!=null){
                    result.setUserAvatar(source.getUser().getAvatar());
                    }
                    if(source.getUser()!=null){
                    result.setUserName(source.getUser().getName());
                    }
                    if(source.getStoreState()!=null){
                        result.setStoreStateName(String.valueOf(source.getStoreState()));
                    }
        return result;
    }

}

