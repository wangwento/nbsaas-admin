package com.nbsaas.boot.scheduling.rest.resource;

import com.nbsaas.boot.rest.request.PageRequest;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.scheduling.api.apis.SchedulingTaskRecordApi;
import com.nbsaas.boot.scheduling.data.entity.SchedulingTaskRecord;
import com.nbsaas.boot.scheduling.api.domain.request.SchedulingTaskRecordDataRequest;
import com.nbsaas.boot.scheduling.api.domain.request.SchedulingTaskRecordSearchRequest;
import com.nbsaas.boot.scheduling.api.domain.response.SchedulingTaskRecordResponse;
import com.nbsaas.boot.scheduling.api.domain.simple.SchedulingTaskRecordSimple;
import com.nbsaas.boot.scheduling.rest.convert.SchedulingTaskRecordSimpleConvert;
import com.nbsaas.boot.scheduling.rest.convert.SchedulingTaskRecordEntityConvert;
import com.nbsaas.boot.scheduling.rest.convert.SchedulingTaskRecordResponseConvert;
import com.nbsaas.boot.scheduling.data.repository.SchedulingTaskRecordRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
*   业务接口实现
*/
@Transactional
@Service
public class SchedulingTaskRecordResource extends BaseResource<SchedulingTaskRecord,SchedulingTaskRecordResponse, SchedulingTaskRecordSimple, SchedulingTaskRecordDataRequest>  implements SchedulingTaskRecordApi {

    @Resource
    private SchedulingTaskRecordRepository schedulingTaskRecordRepository;

    @Override
    public JpaRepositoryImplementation<SchedulingTaskRecord, Serializable> getJpaRepository() {
        return schedulingTaskRecordRepository;
    }

    @Override
    public Function<SchedulingTaskRecord, SchedulingTaskRecordSimple> getConvertSimple() {
        return new SchedulingTaskRecordSimpleConvert();
    }

    @Override
    public Function<SchedulingTaskRecordDataRequest, SchedulingTaskRecord> getConvertForm() {
        return new SchedulingTaskRecordEntityConvert();
    }

    @Override
    public Function<SchedulingTaskRecord, SchedulingTaskRecordResponse> getConvertResponse() {
    return new SchedulingTaskRecordResponseConvert();
    }




}


