package com.nbsaas.boot.scheduling.rest.convert;

import com.nbsaas.boot.scheduling.data.entity.SchedulingTask;
import com.nbsaas.boot.scheduling.api.domain.simple.SchedulingTaskSimple;

import com.nbsaas.boot.rest.api.Converter;
/**
* 列表对象转换器
*/

public class SchedulingTaskSimpleConvert implements Converter<SchedulingTaskSimple, SchedulingTask> {




@Override
public SchedulingTaskSimple convert(SchedulingTask source) {
    SchedulingTaskSimple result = new SchedulingTaskSimple();

                result.setCronExpression(source.getCronExpression());
                result.setAliasName(source.getAliasName());
                result.setRecordType(source.getRecordType());
                result.setName(source.getName());
                result.setRemark(source.getRemark());
                result.setId(source.getId());
                result.setType(source.getType());
                result.setIsSync(source.getIsSync());
                result.setAddDate(source.getAddDate());
                result.setUrl(source.getUrl());
                result.setContent(source.getContent());
                result.setLastDate(source.getLastDate());


    return result;
}

}