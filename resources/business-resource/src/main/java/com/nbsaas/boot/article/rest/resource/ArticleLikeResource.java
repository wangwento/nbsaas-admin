package com.nbsaas.boot.article.rest.resource;

import com.nbsaas.boot.rest.request.PageRequest;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.article.api.apis.ArticleLikeApi;
import com.nbsaas.boot.article.data.entity.ArticleLike;
import com.nbsaas.boot.article.api.domain.request.ArticleLikeDataRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleLikeSearchRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleLikeResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleLikeSimple;
import com.nbsaas.boot.article.rest.convert.ArticleLikeSimpleConvert;
import com.nbsaas.boot.article.rest.convert.ArticleLikeEntityConvert;
import com.nbsaas.boot.article.rest.convert.ArticleLikeResponseConvert;
import com.nbsaas.boot.article.data.repository.ArticleLikeRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
*   业务接口实现
*/
@Transactional
@Service
public class ArticleLikeResource extends BaseResource<ArticleLike,ArticleLikeResponse, ArticleLikeSimple, ArticleLikeDataRequest>  implements ArticleLikeApi {

    @Resource
    private ArticleLikeRepository articleLikeRepository;

    @Override
    public JpaRepositoryImplementation<ArticleLike, Serializable> getJpaRepository() {
        return articleLikeRepository;
    }

    @Override
    public Function<ArticleLike, ArticleLikeSimple> getConvertSimple() {
        return new ArticleLikeSimpleConvert();
    }

    @Override
    public Function<ArticleLikeDataRequest, ArticleLike> getConvertForm() {
        return new ArticleLikeEntityConvert();
    }

    @Override
    public Function<ArticleLike, ArticleLikeResponse> getConvertResponse() {
    return new ArticleLikeResponseConvert();
    }




}


