package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleLike;
import com.nbsaas.boot.article.api.domain.simple.ArticleLikeSimple;

import com.nbsaas.boot.rest.api.Converter;
/**
* 列表对象转换器
*/

public class ArticleLikeSimpleConvert implements Converter<ArticleLikeSimple, ArticleLike> {




@Override
public ArticleLikeSimple convert(ArticleLike source) {
    ArticleLikeSimple result = new ArticleLikeSimple();

                result.setId(source.getId());
                if(source.getUser()!=null){
                    result.setUser(source.getUser().getId());
                }
                result.setAddDate(source.getAddDate());
                if(source.getArticle()!=null){
                    result.setArticle(source.getArticle().getId());
                }
                result.setLastDate(source.getLastDate());


    return result;
}

}