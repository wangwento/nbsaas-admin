package com.nbsaas.boot.article.data.repository;

import com.nbsaas.boot.article.data.entity.ArticleComment;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface ArticleCommentRepository  extends  JpaRepositoryImplementation<ArticleComment, Serializable>{

}