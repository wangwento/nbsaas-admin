package com.nbsaas.boot.article.data.entity;

import com.nbsaas.boot.jpa.data.entity.CatalogEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * 文章分类
 * 
 * @author 年高
 *
 */
@Data
@Entity
@Table(name = "article_catalog")
public class ArticleCatalog extends CatalogEntity {

	/**
	 * 父节点
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	private ArticleCatalog parent;

	/**
	 * 数量
	 */
	private Long amount;

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
	private List<ArticleCatalog> children;


	@Override
	public Serializable getParentId() {
		if (parent != null) {
			return parent.getId();
		}
		return null;
	}
}
