package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.SensitiveWord;
import com.nbsaas.boot.article.api.domain.response.SensitiveWordResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;

/**
* 实体对象转化成响应对象
*/

public class SensitiveWordResponseConvert  implements Converter<SensitiveWordResponse,SensitiveWord> {

    @Override
    public SensitiveWordResponse convert(SensitiveWord source) {
        SensitiveWordResponse  result = new  SensitiveWordResponse();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }

}

