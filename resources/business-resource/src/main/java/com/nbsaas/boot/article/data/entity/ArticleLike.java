package com.nbsaas.boot.article.data.entity;

import com.nbsaas.boot.code.annotation.FieldConvert;
import com.nbsaas.boot.jpa.data.entity.AbstractEntity;
import com.nbsaas.boot.user.data.entity.Staff;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by cng19 on 2017/12/2.
 */

@Data
@Entity
@Table(name = "article_like")
public class ArticleLike extends AbstractEntity {

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private Article article;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff user;

}
