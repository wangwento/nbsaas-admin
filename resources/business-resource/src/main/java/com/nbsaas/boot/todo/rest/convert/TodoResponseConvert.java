package com.nbsaas.boot.todo.rest.convert;

import com.nbsaas.boot.todo.data.entity.Todo;
import com.nbsaas.boot.todo.api.domain.response.TodoResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;

/**
* 实体对象转化成响应对象
*/

public class TodoResponseConvert  implements Converter<TodoResponse,Todo> {

    @Override
    public TodoResponse convert(Todo source) {
        TodoResponse  result = new  TodoResponse();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }

}

