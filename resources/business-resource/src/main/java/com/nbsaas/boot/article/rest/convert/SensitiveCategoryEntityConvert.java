package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.SensitiveCategory;
import com.nbsaas.boot.article.api.domain.request.SensitiveCategoryDataRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class SensitiveCategoryEntityConvert  implements Converter<SensitiveCategory, SensitiveCategoryDataRequest> {

    @Override
    public SensitiveCategory convert(SensitiveCategoryDataRequest source) {
        SensitiveCategory result = new SensitiveCategory();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

