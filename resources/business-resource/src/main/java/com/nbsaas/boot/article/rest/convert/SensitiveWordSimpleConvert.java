package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.SensitiveWord;
import com.nbsaas.boot.article.api.domain.simple.SensitiveWordSimple;

import com.nbsaas.boot.rest.api.Converter;
/**
* 列表对象转换器
*/

public class SensitiveWordSimpleConvert implements Converter<SensitiveWordSimple, SensitiveWord> {




@Override
public SensitiveWordSimple convert(SensitiveWord source) {
    SensitiveWordSimple result = new SensitiveWordSimple();

                result.setSize(source.getSize());
                result.setCatalog(source.getCatalog());
                result.setReplace(source.getReplace());
                result.setId(source.getId());
                result.setWord(source.getWord());
                result.setAddDate(source.getAddDate());
                result.setLastDate(source.getLastDate());


    return result;
}

}