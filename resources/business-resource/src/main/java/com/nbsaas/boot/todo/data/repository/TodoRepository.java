package com.nbsaas.boot.todo.data.repository;

import com.nbsaas.boot.todo.data.entity.Todo;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface TodoRepository  extends  JpaRepositoryImplementation<Todo, Serializable>{

}