package com.nbsaas.boot.customer.rest.convert;

import com.nbsaas.boot.customer.data.entity.Customer;
import com.nbsaas.boot.customer.api.domain.simple.CustomerSimple;

import com.nbsaas.boot.rest.api.Converter;
/**
* 列表对象转换器
*/

public class CustomerSimpleConvert implements Converter<CustomerSimple, Customer> {




@Override
public CustomerSimple convert(Customer source) {
    CustomerSimple result = new CustomerSimple();

                result.setBeginDate(source.getBeginDate());
                result.setScore(source.getScore());
                result.setNote(source.getNote());
                result.setIntroducer(source.getIntroducer());
                result.setPhone(source.getPhone());
                result.setName(source.getName());
                result.setId(source.getId());
                result.setAddDate(source.getAddDate());
                result.setLastDate(source.getLastDate());


    return result;
}

}