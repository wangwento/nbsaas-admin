package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleTag;
import com.nbsaas.boot.article.api.domain.response.ArticleTagResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;

/**
* 实体对象转化成响应对象
*/

public class ArticleTagResponseConvert  implements Converter<ArticleTagResponse,ArticleTag> {

    @Override
    public ArticleTagResponse convert(ArticleTag source) {
        ArticleTagResponse  result = new  ArticleTagResponse();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }

}

