package com.nbsaas.boot.scheduling.data.repository;

import com.nbsaas.boot.scheduling.data.entity.SchedulingTaskRecord;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface SchedulingTaskRecordRepository  extends  JpaRepositoryImplementation<SchedulingTaskRecord, Serializable>{

}