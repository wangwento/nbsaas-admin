package com.nbsaas.boot.scheduling.rest.convert;

import com.nbsaas.boot.scheduling.data.entity.SchedulingTaskRecord;
import com.nbsaas.boot.scheduling.api.domain.simple.SchedulingTaskRecordSimple;

import com.nbsaas.boot.rest.api.Converter;
/**
* 列表对象转换器
*/

public class SchedulingTaskRecordSimpleConvert implements Converter<SchedulingTaskRecordSimple, SchedulingTaskRecord> {




@Override
public SchedulingTaskRecordSimple convert(SchedulingTaskRecord source) {
    SchedulingTaskRecordSimple result = new SchedulingTaskRecordSimple();

                result.setNote(source.getNote());
                if(source.getSchedulingTask()!=null){
                    result.setSchedulingTask(source.getSchedulingTask().getId());
                }
                if(source.getSchedulingTask()!=null){
                    result.setSchedulingTaskName(source.getSchedulingTask().getName());
                }
                result.setState(source.getState());
                result.setId(source.getId());
                result.setAddDate(source.getAddDate());
                result.setLastDate(source.getLastDate());


    return result;
}

}