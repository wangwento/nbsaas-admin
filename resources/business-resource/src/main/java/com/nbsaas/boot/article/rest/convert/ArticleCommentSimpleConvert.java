package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleComment;
import com.nbsaas.boot.article.api.domain.simple.ArticleCommentSimple;

import com.nbsaas.boot.rest.api.Converter;
/**
* 列表对象转换器
*/

public class ArticleCommentSimpleConvert implements Converter<ArticleCommentSimple, ArticleComment> {




@Override
public ArticleCommentSimple convert(ArticleComment source) {
    ArticleCommentSimple result = new ArticleCommentSimple();

                result.setContents(source.getContents());
                result.setId(source.getId());
                result.setTitle(source.getTitle());
                result.setAddDate(source.getAddDate());
                result.setLastDate(source.getLastDate());


    return result;
}

}