package com.nbsaas.boot.todo.rest.resource;

import com.nbsaas.boot.rest.request.PageRequest;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.todo.api.apis.TodoTemplateApi;
import com.nbsaas.boot.todo.data.entity.TodoTemplate;
import com.nbsaas.boot.todo.api.domain.request.TodoTemplateDataRequest;
import com.nbsaas.boot.todo.api.domain.request.TodoTemplateSearchRequest;
import com.nbsaas.boot.todo.api.domain.response.TodoTemplateResponse;
import com.nbsaas.boot.todo.api.domain.simple.TodoTemplateSimple;
import com.nbsaas.boot.todo.rest.convert.TodoTemplateSimpleConvert;
import com.nbsaas.boot.todo.rest.convert.TodoTemplateEntityConvert;
import com.nbsaas.boot.todo.rest.convert.TodoTemplateResponseConvert;
import com.nbsaas.boot.todo.data.repository.TodoTemplateRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
*   业务接口实现
*/
@Transactional
@Service
public class TodoTemplateResource extends BaseResource<TodoTemplate,TodoTemplateResponse, TodoTemplateSimple, TodoTemplateDataRequest>  implements TodoTemplateApi {

    @Resource
    private TodoTemplateRepository todoTemplateRepository;

    @Override
    public JpaRepositoryImplementation<TodoTemplate, Serializable> getJpaRepository() {
        return todoTemplateRepository;
    }

    @Override
    public Function<TodoTemplate, TodoTemplateSimple> getConvertSimple() {
        return new TodoTemplateSimpleConvert();
    }

    @Override
    public Function<TodoTemplateDataRequest, TodoTemplate> getConvertForm() {
        return new TodoTemplateEntityConvert();
    }

    @Override
    public Function<TodoTemplate, TodoTemplateResponse> getConvertResponse() {
    return new TodoTemplateResponseConvert();
    }




}


