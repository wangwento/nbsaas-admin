package com.nbsaas.boot.todo.rest.convert;

import com.nbsaas.boot.todo.data.entity.Todo;
import com.nbsaas.boot.todo.api.domain.request.TodoDataRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class TodoEntityConvert  implements Converter<Todo, TodoDataRequest> {

    @Override
    public Todo convert(TodoDataRequest source) {
        Todo result = new Todo();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

