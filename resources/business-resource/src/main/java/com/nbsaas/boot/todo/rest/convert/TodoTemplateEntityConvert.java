package com.nbsaas.boot.todo.rest.convert;

import com.nbsaas.boot.todo.data.entity.TodoTemplate;
import com.nbsaas.boot.todo.api.domain.request.TodoTemplateDataRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class TodoTemplateEntityConvert  implements Converter<TodoTemplate, TodoTemplateDataRequest> {

    @Override
    public TodoTemplate convert(TodoTemplateDataRequest source) {
        TodoTemplate result = new TodoTemplate();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

