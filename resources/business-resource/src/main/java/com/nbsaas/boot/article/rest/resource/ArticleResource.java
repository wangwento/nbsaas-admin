package com.nbsaas.boot.article.rest.resource;

import com.nbsaas.boot.rest.request.PageRequest;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.article.api.apis.ArticleApi;
import com.nbsaas.boot.article.data.entity.Article;
import com.nbsaas.boot.article.api.domain.request.ArticleDataRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleSearchRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleSimple;
import com.nbsaas.boot.article.rest.convert.ArticleSimpleConvert;
import com.nbsaas.boot.article.rest.convert.ArticleEntityConvert;
import com.nbsaas.boot.article.rest.convert.ArticleResponseConvert;
import com.nbsaas.boot.article.data.repository.ArticleRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
*   业务接口实现
*/
@Transactional
@Service
public class ArticleResource extends BaseResource<Article,ArticleResponse, ArticleSimple, ArticleDataRequest>  implements ArticleApi {

    @Resource
    private ArticleRepository articleRepository;

    @Override
    public JpaRepositoryImplementation<Article, Serializable> getJpaRepository() {
        return articleRepository;
    }

    @Override
    public Function<Article, ArticleSimple> getConvertSimple() {
        return new ArticleSimpleConvert();
    }

    @Override
    public Function<ArticleDataRequest, Article> getConvertForm() {
        return new ArticleEntityConvert();
    }

    @Override
    public Function<Article, ArticleResponse> getConvertResponse() {
    return new ArticleResponseConvert();
    }




}


