package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.Article;
import com.nbsaas.boot.article.api.domain.simple.ArticleSimple;

import com.nbsaas.boot.rest.api.Converter;
/**
* 列表对象转换器
*/

public class ArticleSimpleConvert implements Converter<ArticleSimple, Article> {




@Override
public ArticleSimple convert(Article source) {
    ArticleSimple result = new ArticleSimple();

                result.setImages(source.getImages());
                result.setImg(source.getImg());
                result.setComments(source.getComments());
                if(source.getUser()!=null){
                    result.setUserAvatar(source.getUser().getAvatar());
                }
                result.setTitle(source.getTitle());
                if(source.getUser()!=null){
                    result.setUserName(source.getUser().getName());
                }
                result.setAddDate(source.getAddDate());
                result.setExtData(source.getExtData());
                result.setUps(source.getUps());
                if(source.getStoreState()!=null){
                    result.setStoreStateName(String.valueOf(source.getStoreState()));
                }
                result.setStoreState(source.getStoreState());
                result.setId(source.getId());
                result.setIntroduction(source.getIntroduction());
                result.setViews(source.getViews());
                result.setLikes(source.getLikes());
                result.setLastDate(source.getLastDate());


    return result;
}

}