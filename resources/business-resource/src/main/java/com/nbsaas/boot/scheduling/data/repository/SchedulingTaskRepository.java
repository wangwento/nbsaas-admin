package com.nbsaas.boot.scheduling.data.repository;

import com.nbsaas.boot.scheduling.data.entity.SchedulingTask;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface SchedulingTaskRepository  extends  JpaRepositoryImplementation<SchedulingTask, Serializable>{

}