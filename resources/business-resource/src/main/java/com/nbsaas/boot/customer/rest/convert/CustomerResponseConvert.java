package com.nbsaas.boot.customer.rest.convert;

import com.nbsaas.boot.customer.data.entity.Customer;
import com.nbsaas.boot.customer.api.domain.response.CustomerResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;

/**
* 实体对象转化成响应对象
*/

public class CustomerResponseConvert  implements Converter<CustomerResponse,Customer> {

    @Override
    public CustomerResponse convert(Customer source) {
        CustomerResponse  result = new  CustomerResponse();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }

}

