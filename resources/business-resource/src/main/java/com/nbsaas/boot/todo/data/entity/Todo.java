package com.nbsaas.boot.todo.data.entity;

import com.nbsaas.boot.jpa.data.entity.AbstractEntity;
import com.nbsaas.boot.user.data.entity.Staff;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * description: todo 代办 实体类
 */
@Data
@Entity
@Table(name = "todo")
public class Todo extends AbstractEntity {


    /**
     * name 待办名称
     */
    private String name;

    /**
     * data_key 标识
     */
    private String dataKey;

    /**
     * note 待办内容
     */
    private String note;

    /**
     * navigate_url 跳转url
     */
    private String navigateUrl;

    /**
     * state 状态 1代办 2已处理
     */
    private Integer state;


    /**
     * owner 待办人id
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff owner;

    /**
     * owner_name 待办人名称
     */
    private String ownerName;

    /**
     * handler 处理人id
     */
    private Long handler;

    /**
     * handler_name 处理人姓名
     */
    private String handlerName;

    /**
     * data_id 业务数据id
     */
    private Long dataId;


}