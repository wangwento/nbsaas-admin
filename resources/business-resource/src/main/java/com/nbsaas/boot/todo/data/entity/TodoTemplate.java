package com.nbsaas.boot.todo.data.entity;

import com.nbsaas.boot.jpa.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* description: todo_template 代办模板配置 实体类
*/
@Data
@Entity
@Table(name = "todo_template")
public class TodoTemplate extends AbstractEntity {





    /**
    * name 模板名称
    */
    private String name;

    /**
    * data_key 标识
    */
    private String dataKey;

    /**
    * navigate_url 跳转url
    */
    private String navigateUrl;

    /**
    * ext_data 用户数据
    */
    private String extData;


}