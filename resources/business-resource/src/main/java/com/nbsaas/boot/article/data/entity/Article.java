package com.nbsaas.boot.article.data.entity;

import com.nbsaas.boot.code.annotation.*;
import com.nbsaas.boot.jpa.data.entity.AbstractEntity;
import com.nbsaas.boot.rest.enums.StoreState;
import com.nbsaas.boot.rest.filter.Operator;
import com.nbsaas.boot.user.data.entity.Staff;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

/**
 * 文章
 *
 * @author 年高
 */
@BeanExt(items = {
        @FormExtField(title = "用户名称",parent = "user",parentField = "name", fieldName = "userName", fieldClass = String.class),
        @FormExtField(title = "用户头像",parent = "user",parentField = "avatar", fieldName = "userAvatar", fieldClass = String.class),
        @FormExtField(title = "文章内容",parent = "document",parentField = "note", fieldName = "note", fieldClass = String.class,simple = false)

})
@SearchBean(
        items = {
                @SearchItem(label = "文章分类名称",name = "catalogName",key = "catalog.name")
        }
)
@Data
@Entity
@Table(name = "article")
public class Article extends AbstractEntity {

    public static Article fromId(Long id){
        Article result=new Article();
        result.setId(id);
        return result;
    }

    /**
     * 文章分类
     */
    @SearchItem(label = "文章分类",name = "catalog",key = "catalog.id",classType = Long.class,operator = Operator.eq)
    @ManyToOne(fetch = FetchType.LAZY)
    private ArticleCatalog catalog;
    /**
     * 文章评论数量
     */
    private Integer comments;

    /**
     * 喜欢的数量
     */
    private Integer likes;


    /**
     * 文章内容
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private ArticleDocument  document;

    /**
     * 文章扩展信息
     */
    private String extData;


    /**
     * 文章图片集
     */
    private String images;

    /**
     * 封面
     */
    private String img;

    /**
     * 文章介绍
     */
    private String introduction;
    @JoinTable(name = "article_link_tag")
    @ManyToMany
    private Set<ArticleTag> tags;

    /**
     * 文章标题
     */
    private String title;

    /**
     * 点赞数量
     */
    private Integer ups;

    /**
     * 文章作者
     */
    @SearchItem(label = "用户名称",name = "userName",key = "user.name",show = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff user;
    /**
     * 文章查看数量
     */
    private Integer views;

    private StoreState storeState;

}
