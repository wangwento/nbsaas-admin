package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.SensitiveWord;
import com.nbsaas.boot.article.api.domain.request.SensitiveWordDataRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class SensitiveWordEntityConvert  implements Converter<SensitiveWord, SensitiveWordDataRequest> {

    @Override
    public SensitiveWord convert(SensitiveWordDataRequest source) {
        SensitiveWord result = new SensitiveWord();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

