package com.nbsaas.boot.todo.rest.convert;

import com.nbsaas.boot.todo.data.entity.TodoTemplate;
import com.nbsaas.boot.todo.api.domain.response.TodoTemplateResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;

/**
* 实体对象转化成响应对象
*/

public class TodoTemplateResponseConvert  implements Converter<TodoTemplateResponse,TodoTemplate> {

    @Override
    public TodoTemplateResponse convert(TodoTemplate source) {
        TodoTemplateResponse  result = new  TodoTemplateResponse();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }

}

