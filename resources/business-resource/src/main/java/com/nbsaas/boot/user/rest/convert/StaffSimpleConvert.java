package com.nbsaas.boot.user.rest.convert;

import com.nbsaas.boot.user.data.entity.Staff;
import com.nbsaas.boot.user.api.domain.simple.StaffSimple;

import com.nbsaas.boot.rest.api.Converter;
/**
* 列表对象转换器
*/

public class StaffSimpleConvert implements Converter<StaffSimple, Staff> {




@Override
public StaffSimple convert(Staff source) {
    StaffSimple result = new StaffSimple();

                result.setPhone(source.getPhone());
                result.setCatalog(source.getCatalog());
                if(source.getStructure()!=null){
                    result.setStructureName(source.getStructure().getName());
                }
                result.setName(source.getName());
                if(source.getStoreState()!=null){
                    result.setStoreStateName(String.valueOf(source.getStoreState()));
                }
                result.setStoreState(source.getStoreState());
                result.setAvatar(source.getAvatar());
                result.setId(source.getId());
                result.setAddDate(source.getAddDate());
                if(source.getStructure()!=null){
                    result.setStructure(source.getStructure().getId());
                }
                result.setLoginSize(source.getLoginSize());
                result.setLastDate(source.getLastDate());


    return result;
}

}