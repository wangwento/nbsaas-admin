package com.nbsaas.boot.todo.rest.resource;

import com.nbsaas.boot.rest.request.PageRequest;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.todo.api.apis.TodoApi;
import com.nbsaas.boot.todo.data.entity.Todo;
import com.nbsaas.boot.todo.api.domain.request.TodoDataRequest;
import com.nbsaas.boot.todo.api.domain.request.TodoSearchRequest;
import com.nbsaas.boot.todo.api.domain.response.TodoResponse;
import com.nbsaas.boot.todo.api.domain.simple.TodoSimple;
import com.nbsaas.boot.todo.rest.convert.TodoSimpleConvert;
import com.nbsaas.boot.todo.rest.convert.TodoEntityConvert;
import com.nbsaas.boot.todo.rest.convert.TodoResponseConvert;
import com.nbsaas.boot.todo.data.repository.TodoRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
*   业务接口实现
*/
@Transactional
@Service
public class TodoResource extends BaseResource<Todo,TodoResponse, TodoSimple, TodoDataRequest>  implements TodoApi {

    @Resource
    private TodoRepository todoRepository;

    @Override
    public JpaRepositoryImplementation<Todo, Serializable> getJpaRepository() {
        return todoRepository;
    }

    @Override
    public Function<Todo, TodoSimple> getConvertSimple() {
        return new TodoSimpleConvert();
    }

    @Override
    public Function<TodoDataRequest, Todo> getConvertForm() {
        return new TodoEntityConvert();
    }

    @Override
    public Function<Todo, TodoResponse> getConvertResponse() {
    return new TodoResponseConvert();
    }




}


