package com.nbsaas.boot.article.data.repository;

import com.nbsaas.boot.article.data.entity.Article;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface ArticleRepository  extends  JpaRepositoryImplementation<Article, Serializable>{

}