package com.nbsaas.boot.scheduling.rest.convert;

import com.nbsaas.boot.scheduling.data.entity.SchedulingTaskRecord;
import com.nbsaas.boot.scheduling.api.domain.request.SchedulingTaskRecordDataRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;
            import com.nbsaas.boot.scheduling.data.entity.SchedulingTask;

/**
* 请求对象转换成实体对象
*/

public class SchedulingTaskRecordEntityConvert  implements Converter<SchedulingTaskRecord, SchedulingTaskRecordDataRequest> {

    @Override
    public SchedulingTaskRecord convert(SchedulingTaskRecordDataRequest source) {
        SchedulingTaskRecord result = new SchedulingTaskRecord();
        BeanDataUtils.copyProperties(source, result);
                    if(source.getSchedulingTask()!=null){
                    SchedulingTask schedulingTask =new SchedulingTask();
                    schedulingTask.setId(source.getSchedulingTask());
                    result.setSchedulingTask(schedulingTask);
                    }
        return result;
    }
}

