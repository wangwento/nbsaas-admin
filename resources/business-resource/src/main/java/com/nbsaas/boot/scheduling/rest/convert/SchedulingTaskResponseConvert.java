package com.nbsaas.boot.scheduling.rest.convert;

import com.nbsaas.boot.scheduling.data.entity.SchedulingTask;
import com.nbsaas.boot.scheduling.api.domain.response.SchedulingTaskResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;

/**
* 实体对象转化成响应对象
*/

public class SchedulingTaskResponseConvert  implements Converter<SchedulingTaskResponse,SchedulingTask> {

    @Override
    public SchedulingTaskResponse convert(SchedulingTask source) {
        SchedulingTaskResponse  result = new  SchedulingTaskResponse();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }

}

