package com.nbsaas.boot.article.data.entity;

import com.nbsaas.boot.code.annotation.SearchBean;
import com.nbsaas.boot.code.annotation.SearchItem;
import com.nbsaas.boot.jpa.data.entity.AbstractEntity;
import com.nbsaas.boot.rest.filter.Operator;
import com.nbsaas.boot.user.data.entity.Staff;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 文章评论
 * 
 * @author 年高
 *
 */

@SearchBean(items = {@SearchItem(label = "文章分类", name = "categoryName", key = "article.catalog.name", operator = Operator.like)})
@Data
@Entity
@Table(name = "article_comment")
public class ArticleComment extends AbstractEntity {



	private String contents;


	private String title;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Article article;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Staff user;



}
