package com.nbsaas.boot.article.data.repository;

import com.nbsaas.boot.article.data.entity.SensitiveWord;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface SensitiveWordRepository  extends  JpaRepositoryImplementation<SensitiveWord, Serializable>{

}