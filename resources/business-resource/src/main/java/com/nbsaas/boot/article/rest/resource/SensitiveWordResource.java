package com.nbsaas.boot.article.rest.resource;

import com.nbsaas.boot.rest.request.PageRequest;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.article.api.apis.SensitiveWordApi;
import com.nbsaas.boot.article.data.entity.SensitiveWord;
import com.nbsaas.boot.article.api.domain.request.SensitiveWordDataRequest;
import com.nbsaas.boot.article.api.domain.request.SensitiveWordSearchRequest;
import com.nbsaas.boot.article.api.domain.response.SensitiveWordResponse;
import com.nbsaas.boot.article.api.domain.simple.SensitiveWordSimple;
import com.nbsaas.boot.article.rest.convert.SensitiveWordSimpleConvert;
import com.nbsaas.boot.article.rest.convert.SensitiveWordEntityConvert;
import com.nbsaas.boot.article.rest.convert.SensitiveWordResponseConvert;
import com.nbsaas.boot.article.data.repository.SensitiveWordRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
*   业务接口实现
*/
@Transactional
@Service
public class SensitiveWordResource extends BaseResource<SensitiveWord,SensitiveWordResponse, SensitiveWordSimple, SensitiveWordDataRequest>  implements SensitiveWordApi {

    @Resource
    private SensitiveWordRepository sensitiveWordRepository;

    @Override
    public JpaRepositoryImplementation<SensitiveWord, Serializable> getJpaRepository() {
        return sensitiveWordRepository;
    }

    @Override
    public Function<SensitiveWord, SensitiveWordSimple> getConvertSimple() {
        return new SensitiveWordSimpleConvert();
    }

    @Override
    public Function<SensitiveWordDataRequest, SensitiveWord> getConvertForm() {
        return new SensitiveWordEntityConvert();
    }

    @Override
    public Function<SensitiveWord, SensitiveWordResponse> getConvertResponse() {
    return new SensitiveWordResponseConvert();
    }




}


