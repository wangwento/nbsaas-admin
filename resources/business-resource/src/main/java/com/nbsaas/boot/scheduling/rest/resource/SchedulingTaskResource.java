package com.nbsaas.boot.scheduling.rest.resource;

import com.nbsaas.boot.rest.request.PageRequest;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.scheduling.api.apis.SchedulingTaskApi;
import com.nbsaas.boot.scheduling.data.entity.SchedulingTask;
import com.nbsaas.boot.scheduling.api.domain.request.SchedulingTaskDataRequest;
import com.nbsaas.boot.scheduling.api.domain.request.SchedulingTaskSearchRequest;
import com.nbsaas.boot.scheduling.api.domain.response.SchedulingTaskResponse;
import com.nbsaas.boot.scheduling.api.domain.simple.SchedulingTaskSimple;
import com.nbsaas.boot.scheduling.rest.convert.SchedulingTaskSimpleConvert;
import com.nbsaas.boot.scheduling.rest.convert.SchedulingTaskEntityConvert;
import com.nbsaas.boot.scheduling.rest.convert.SchedulingTaskResponseConvert;
import com.nbsaas.boot.scheduling.data.repository.SchedulingTaskRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
*   业务接口实现
*/
@Transactional
@Service
public class SchedulingTaskResource extends BaseResource<SchedulingTask,SchedulingTaskResponse, SchedulingTaskSimple, SchedulingTaskDataRequest>  implements SchedulingTaskApi {

    @Resource
    private SchedulingTaskRepository schedulingTaskRepository;

    @Override
    public JpaRepositoryImplementation<SchedulingTask, Serializable> getJpaRepository() {
        return schedulingTaskRepository;
    }

    @Override
    public Function<SchedulingTask, SchedulingTaskSimple> getConvertSimple() {
        return new SchedulingTaskSimpleConvert();
    }

    @Override
    public Function<SchedulingTaskDataRequest, SchedulingTask> getConvertForm() {
        return new SchedulingTaskEntityConvert();
    }

    @Override
    public Function<SchedulingTask, SchedulingTaskResponse> getConvertResponse() {
    return new SchedulingTaskResponseConvert();
    }




}


