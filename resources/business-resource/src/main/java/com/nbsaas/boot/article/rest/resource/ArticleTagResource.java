package com.nbsaas.boot.article.rest.resource;

import com.nbsaas.boot.rest.request.PageRequest;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.article.api.apis.ArticleTagApi;
import com.nbsaas.boot.article.data.entity.ArticleTag;
import com.nbsaas.boot.article.api.domain.request.ArticleTagDataRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleTagSearchRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleTagResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleTagSimple;
import com.nbsaas.boot.article.rest.convert.ArticleTagSimpleConvert;
import com.nbsaas.boot.article.rest.convert.ArticleTagEntityConvert;
import com.nbsaas.boot.article.rest.convert.ArticleTagResponseConvert;
import com.nbsaas.boot.article.data.repository.ArticleTagRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
*   业务接口实现
*/
@Transactional
@Service
public class ArticleTagResource extends BaseResource<ArticleTag,ArticleTagResponse, ArticleTagSimple, ArticleTagDataRequest>  implements ArticleTagApi {

    @Resource
    private ArticleTagRepository articleTagRepository;

    @Override
    public JpaRepositoryImplementation<ArticleTag, Serializable> getJpaRepository() {
        return articleTagRepository;
    }

    @Override
    public Function<ArticleTag, ArticleTagSimple> getConvertSimple() {
        return new ArticleTagSimpleConvert();
    }

    @Override
    public Function<ArticleTagDataRequest, ArticleTag> getConvertForm() {
        return new ArticleTagEntityConvert();
    }

    @Override
    public Function<ArticleTag, ArticleTagResponse> getConvertResponse() {
    return new ArticleTagResponseConvert();
    }




}


